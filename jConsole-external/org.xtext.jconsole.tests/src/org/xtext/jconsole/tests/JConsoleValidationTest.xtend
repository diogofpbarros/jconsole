package org.xtext.jconsole.tests

import com.google.inject.Inject
import com.jconsole.CLIApp
import com.jconsole.JconsolePackage
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import org.xtext.jconsole.validation.JConsoleValidator

@ExtendWith(InjectionExtension)
@InjectWith(JConsoleInjectorProvider)
class JConsoleValidationTest {
	
	@Inject
	ParseHelper<CLIApp> parseHelper
	
	@Inject
	ValidationTestHelper validator
	
	
	@Test
	def void duplicateCommands() {
		val result = parseHelper.parse('''
			command exit = "exit"
			command exit = ""
			
			screen Single {
				do {{
					print("I'm single :(");
				}}
			}
			
			main is Single
		''')
		
		// Parsed
		Assertions.assertNotNull(result)
		
		// Error is raised
		validator.assertError(result,
							  JconsolePackage::eINSTANCE.command,
							  JConsoleValidator::DUPLICATE_COMMAND,
			                  "Duplicate declaration for command 'exit'")
	}
	
	@Test
	def void duplicateGuards() {
		val result = parseHelper.parse('''
			variables {{
				static int i = 0;
			}}
			
			guard once = "i != 0"
			guard once = "i > 0"
			
			screen Single {
				do {{
					print("I'm single :(");
				}}
			}
			
			main is Single
		''')
		
		// Parsed
		Assertions.assertNotNull(result)
		
		// Error is raised
		validator.assertError(result,
							  JconsolePackage::eINSTANCE.guard,
							  JConsoleValidator::DUPLICATE_GUARD,
			                  "Duplicate declaration for guard 'once'")
	}
	
	@Test
	def void duplicateScreens() {
		val result = parseHelper.parse('''
			screen Single {
				do {{
					print("I'm single :(");
				}}
			}
			
			screen Single {
				do {{
					print("I'm NOT single :)");
				}}
			}
			
			main is Single
		''')
		
		// Parsed
		Assertions.assertNotNull(result)
		
		// Error is raised
		validator.assertError(result,
							  JconsolePackage::eINSTANCE.screen,
							  JConsoleValidator::DUPLICATE_SCREEN,
			                  "Duplicate declaration for screen 'Single'")
	}
	
	@Test
	def void duplicateProcedure() {
		val result = parseHelper.parse('''
			screen Single {
				do {{
					print("I'm single :(");
				}}
			}
			
			procedure DoIt {{
				print("Do it!");
			}}
			
			procedure DoIt {{
				print("Do it again!");			
			}}
			
			main is Single
		''')
		
		// Parsed
		Assertions.assertNotNull(result)
		
		// Error is raised
		validator.assertError(result,
							  JconsolePackage::eINSTANCE.procedure,
							  JConsoleValidator::DUPLICATE_PROCEDURE,
			                  "Duplicate declaration for procedure 'DoIt'")
	}
}