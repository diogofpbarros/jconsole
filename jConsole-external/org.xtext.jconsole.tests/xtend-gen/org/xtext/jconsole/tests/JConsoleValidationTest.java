package org.xtext.jconsole.tests;

import com.google.inject.Inject;
import com.jconsole.CLIApp;
import com.jconsole.JconsolePackage;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.xtext.jconsole.validation.JConsoleValidator;

@ExtendWith(InjectionExtension.class)
@InjectWith(JConsoleInjectorProvider.class)
@SuppressWarnings("all")
public class JConsoleValidationTest {
  @Inject
  private ParseHelper<CLIApp> parseHelper;
  
  @Inject
  private ValidationTestHelper validator;
  
  @Test
  public void duplicateCommands() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("command exit = \"exit\"");
      _builder.newLine();
      _builder.append("command exit = \"\"");
      _builder.newLine();
      _builder.newLine();
      _builder.append("screen Single {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("do {{");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("print(\"I\'m single :(\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("main is Single");
      _builder.newLine();
      final CLIApp result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      this.validator.assertError(result, 
        JconsolePackage.eINSTANCE.getCommand(), 
        JConsoleValidator.DUPLICATE_COMMAND, 
        "Duplicate declaration for command \'exit\'");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void duplicateGuards() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("variables {{");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("static int i = 0;");
      _builder.newLine();
      _builder.append("}}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("guard once = \"i != 0\"");
      _builder.newLine();
      _builder.append("guard once = \"i > 0\"");
      _builder.newLine();
      _builder.newLine();
      _builder.append("screen Single {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("do {{");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("print(\"I\'m single :(\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("main is Single");
      _builder.newLine();
      final CLIApp result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      this.validator.assertError(result, 
        JconsolePackage.eINSTANCE.getGuard(), 
        JConsoleValidator.DUPLICATE_GUARD, 
        "Duplicate declaration for guard \'once\'");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void duplicateScreens() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("screen Single {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("do {{");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("print(\"I\'m single :(\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("screen Single {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("do {{");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("print(\"I\'m NOT single :)\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("main is Single");
      _builder.newLine();
      final CLIApp result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      this.validator.assertError(result, 
        JconsolePackage.eINSTANCE.getScreen(), 
        JConsoleValidator.DUPLICATE_SCREEN, 
        "Duplicate declaration for screen \'Single\'");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void duplicateProcedure() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("screen Single {");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("do {{");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("print(\"I\'m single :(\");");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("}}");
      _builder.newLine();
      _builder.append("}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("procedure DoIt {{");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("print(\"Do it!\");");
      _builder.newLine();
      _builder.append("}}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("procedure DoIt {{");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("print(\"Do it again!\");\t\t\t");
      _builder.newLine();
      _builder.append("}}");
      _builder.newLine();
      _builder.newLine();
      _builder.append("main is Single");
      _builder.newLine();
      final CLIApp result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      this.validator.assertError(result, 
        JconsolePackage.eINSTANCE.getProcedure(), 
        JConsoleValidator.DUPLICATE_PROCEDURE, 
        "Duplicate declaration for procedure \'DoIt\'");
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
