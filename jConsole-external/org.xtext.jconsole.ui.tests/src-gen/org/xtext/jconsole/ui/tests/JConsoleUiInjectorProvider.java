/*
 * generated by Xtext 2.25.0
 */
package org.xtext.jconsole.ui.tests;

import com.google.inject.Injector;
import org.eclipse.xtext.testing.IInjectorProvider;
import org.xtext.jconsole.ui.internal.JconsoleActivator;

public class JConsoleUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return JconsoleActivator.getInstance().getInjector("org.xtext.jconsole.JConsole");
	}

}
