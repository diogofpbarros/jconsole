// ---------- DEFAULT IMPORTS ----------
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// ---------- DEFAULT IMPORTS ----------

// ---------- USER IMPORTS ----------

	import java.util.Random;
// ---------- USER IMPORTS ----------

public class GuessGame {
	
	// ---------- VARIABLES ----------
	 
		// Java code
	  	static int n, attempts, guess; 
	  	static int maxAttempts = 10;
	// ---------- VARIABLES ----------
	
	
	private static final HashMap<Class<?>, Function<String, ?>> PARSERS = getParsers();
	private static final HashMap<Class<?>, Function<String, ?>> getParsers()
	{
		HashMap<Class<?>, Function<String, ?>> parsers = new HashMap<>();
	
		parsers.put(Integer.class, Integer::parseInt);
		parsers.put(String.class, (s) -> s);
		parsers.put(Boolean.class, Boolean::parseBoolean);
	
		return parsers;
	}
	
	
	public static void main(String[] args) 
	{
		// ---------- COMMANDS ----------
		HashMap<String, Command> commands = new HashMap<>();
		commands.put("Guess", (input) -> {
			Matcher m = Pattern.compile("^(-?\\d+)$").matcher(input);
			if (!m.find()) 
				return false;
				
			int i = 1;
			
			GuessGame.guess = (Integer) PARSERS.get(Integer.class).apply(m.group(i));
			i++;
			
			return true;
		});
		// ---------- COMMANDS ----------
		
		
		// ---------- PROCEDURES ----------
		//   User defined procedures have their own name
		HashMap<String, Procedure> procedures = new HashMap<>();
		
		//   Screen internal procedures have the name of the screen
		procedures.put("MainScreen", () -> {
			  
					// Java code 
			    	Random r = new Random(1);
			    	n = r.nextInt(1000) + 1;
			    	attempts = 1;
			    	 
			    	print("Guess a number between 1 and 1000!");  
		});
		procedures.put("RetryGuess", () -> {
			
					if (n < guess)
			      		print("Lower!");
			    	else
			      		print("Higher!");
			    	attempts++;
		});
		procedures.put("Win", () -> {
			
					// Java code
			    	print("You won!");
		});
		procedures.put("Loss", () -> {
			
					// Java code
			    	print("You lost! The answer was " + n);
		});
		// ---------- PROCEDURES ----------
		
		
		String currentScreen = "MainScreen";
		
		// Trigger first Screen's procedure
		procedures.get(currentScreen).doit();
		
		// App loop
		String input;
		Scanner sc = new Scanner(System.in);
		while (true)
		{
			
			input = sc.nextLine();
			
			if (currentScreen.equals("MainScreen"))
			{
				if (input.matches("^(-?\\d+)$") &&
				    commands.get("Guess").parse(input)) {
					if ((guess == n))
					{
						currentScreen = "Win";
						
						procedures.get(currentScreen).doit();
						break;
					}
					else {
						currentScreen = "RetryGuess";
				
					}
				}
			}
			else if (currentScreen.equals("RetryGuess"))
			{
				if (input.matches("^(-?\\d+)$") &&
				    commands.get("Guess").parse(input)) {
					if ( (guess == n) && (attempts < maxAttempts))
					{
						currentScreen = "Win";
						
						procedures.get(currentScreen).doit();
						break;
					}
					else if (!(guess == n) && (attempts < maxAttempts))
					{
						currentScreen = "RetryGuess";
						
					}
					else {
						currentScreen = "Loss";
				
						procedures.get(currentScreen).doit();
						break;
					}
				}
			}
			else if (currentScreen.equals("Win"))
			{
				// Terminal screen
				break;
			}
			else if (currentScreen.equals("Loss"))
			{
				// Terminal screen
				break;
			}
			
			procedures.get(currentScreen).doit();
		}
		
		sc.close();
	}
	
	// AUX METHODS
	
	private static void print(Object o) {
		System.out.println(o);
	}
	
	// STRUCTS
	
	// COMMAND
	@FunctionalInterface
	private interface Command {
		boolean parse(String input);
	}
	
	// PROCEDURE
	@FunctionalInterface
	private interface Procedure {
		void doit();
	}
}
