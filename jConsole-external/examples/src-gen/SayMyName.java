// ---------- DEFAULT IMPORTS ----------
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// ---------- DEFAULT IMPORTS ----------

// ---------- USER IMPORTS ----------
// ---------- USER IMPORTS ----------

public class SayMyName {
	
	// ---------- VARIABLES ----------
	
		static String name;
	// ---------- VARIABLES ----------
	
	
	private static final HashMap<Class<?>, Function<String, ?>> PARSERS = getParsers();
	private static final HashMap<Class<?>, Function<String, ?>> getParsers()
	{
		HashMap<Class<?>, Function<String, ?>> parsers = new HashMap<>();
	
		parsers.put(Integer.class, Integer::parseInt);
		parsers.put(String.class, (s) -> s);
		parsers.put(Boolean.class, Boolean::parseBoolean);
	
		return parsers;
	}
	
	
	public static void main(String[] args) 
	{
		// ---------- COMMANDS ----------
		HashMap<String, Command> commands = new HashMap<>();
		commands.put("Name", (input) -> {
			Matcher m = Pattern.compile("^<name : String>$").matcher(input);
			if (!m.find()) 
				return false;
				
			int i = 1;
			
			SayMyName.name = (String) PARSERS.get(String.class).apply(m.group(i));
			i++;
			
			return true;
		});
		// ---------- COMMANDS ----------
		
		
		// ---------- PROCEDURES ----------
		//   User defined procedures have their own name
		HashMap<String, Procedure> procedures = new HashMap<>();
		
		//   Screen internal procedures have the name of the screen
		procedures.put("Main", () -> {
			
					System.out.println("What is your name?");
		});
		procedures.put("SayMyName", () -> {
			
					System.out.println("Hello, " + name + "!");
		});
		// ---------- PROCEDURES ----------
		
		
		String currentScreen = "Main";
		
		// Trigger first Screen's procedure
		procedures.get(currentScreen).doit();
		
		// App loop
		String input;
		Scanner sc = new Scanner(System.in);
		while (true)
		{
			
			input = sc.nextLine();
			
			if (currentScreen.equals("Main"))
			{
				if(commands.get("Name").parse(input)) {
					currentScreen = "SayMyName";
					procedures.get(currentScreen).doit();
					break;
				}
			}
			else if (currentScreen.equals("SayMyName"))
			{
				// Terminal screen
				break;
			}
			
			procedures.get(currentScreen).doit();
		}
		
		sc.close();
	}
	
	// AUX METHODS
	
	private static void print(Object o) {
		System.out.println(o);
	}
	
	// STRUCTS
	
	// COMMAND
	@FunctionalInterface
	private interface Command {
		boolean parse(String input);
	}
	
	// PROCEDURE
	@FunctionalInterface
	private interface Procedure {
		void doit();
	}
}
