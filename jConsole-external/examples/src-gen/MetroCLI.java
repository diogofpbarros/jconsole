// ---------- DEFAULT IMPORTS ----------
import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
// ---------- DEFAULT IMPORTS ----------

// ---------- USER IMPORTS ----------

	import pt.diogofpbarros.metroapi.client.*;
	import pt.diogofpbarros.metroapi.client.data_structures.*;

	import java.util.List;
// ---------- USER IMPORTS ----------

public class MetroCLI {
	
	// ---------- VARIABLES ----------
	
		static MetroAPIAdapter api = MetroAPIAdapter.getInstance();
	
		static String linha, estacao;
	// ---------- VARIABLES ----------
	
	
	private static final HashMap<Class<?>, Function<String, ?>> PARSERS = getParsers();
	private static final HashMap<Class<?>, Function<String, ?>> getParsers()
	{
		HashMap<Class<?>, Function<String, ?>> parsers = new HashMap<>();
	
		parsers.put(Integer.class, Integer::parseInt);
		parsers.put(String.class, (s) -> s);
		parsers.put(Boolean.class, Boolean::parseBoolean);
	
		return parsers;
	}
	
	
	public static void main(String[] args) 
	{
		// ---------- COMMANDS ----------
		HashMap<String, Command> commands = new HashMap<>();
		commands.put("menuEstadoLinha", (input) -> {
			Matcher m = Pattern.compile("^estadoLinha$").matcher(input);
			if (!m.find()) 
				return false;
				
			
			return true;
		});
		commands.put("menuTempoEspera", (input) -> {
			Matcher m = Pattern.compile("^tempoEspera$").matcher(input);
			if (!m.find()) 
				return false;
				
			
			return true;
		});
		commands.put("showLinhas", (input) -> {
			Matcher m = Pattern.compile("^linhas$").matcher(input);
			if (!m.find()) 
				return false;
				
			
			return true;
		});
		commands.put("getEstadoLinha", (input) -> {
			Matcher m = Pattern.compile("^\"(\\w*)\"$").matcher(input);
			if (!m.find()) 
				return false;
				
			int i = 1;
			
			MetroCLI.linha = (String) PARSERS.get(String.class).apply(m.group(i));
			i++;
			
			return true;
		});
		commands.put("getEstadoLinhaTodos", (input) -> {
			Matcher m = Pattern.compile("^todas$").matcher(input);
			if (!m.find()) 
				return false;
				
			
			return true;
		});
		commands.put("exit", (input) -> {
			Matcher m = Pattern.compile("^exit$").matcher(input);
			if (!m.find()) 
				return false;
				
			
			return true;
		});
		// ---------- COMMANDS ----------
		
		
		// ---------- PROCEDURES ----------
		//   User defined procedures have their own name
		HashMap<String, Procedure> procedures = new HashMap<>();
		procedures.put("ShowLinhas", () -> {
			
				print("Linhas:");
				print("> \"Azul\"");
				print("> \"Amarela\"");
				print("> \"Vermelha\"");
				print("> \"Verde\"");
				print("");
		});
		procedures.put("GetEstadoLinha", () -> {
			
				MetroLineState state = api.getEstadoLinha(MetroLine.parseLinha(linha));
			
				if (state == null)
					print("Indisponivel");
				else
					print(state);
		});
		procedures.put("GetEstadoLinhaTodos", () -> {
			
					// TODO: Insert your Java code here
		});
		
		//   Screen internal procedures have the name of the screen
		procedures.put("MainMenu", () -> {
			
					print("");
					print("==== Menu Principal ====");
			    	print("Available commands:");
			    	print("> estadoLinha - Menu Estado Linha");
			    	print("> tempoEspera - Menu Tempos Espera");
			    	print("> exit    - Fechar o menu");
		});
		procedures.put("MenuEstadoLinha", () -> {
			
					print("");
					print("==== Menu Estado Linha ====");
					print("Available commands:");
					print("> linhas  - Mostra todas as linhas ");
					print("> <linha> - Mostra o estado para a linha dada");
					print("> todas   - Mostra o estado de todas as linhas");
					print("> back    - Regressar ao menu principal");
		});
		procedures.put("MenuTempoEspera", () -> {
			
					// TODO: Insert your Java code here
		});
		procedures.put("Exit", () -> {
			
			 		print("Exiting..");
			 		print("==== Metro ====");
		});
		// ---------- PROCEDURES ----------
		
		
		String currentScreen = "MainMenu";
		
		// Trigger first Screen's procedure
		procedures.get(currentScreen).doit();
		
		// App loop
		String input;
		Scanner sc = new Scanner(System.in);
		while (true)
		{
			
			input = sc.nextLine();
			
			if (currentScreen.equals("MainMenu"))
			{
				if(commands.get("menuEstadoLinha").parse(input)) {
					currentScreen = "MenuEstadoLinha";
				}
				if(commands.get("menuTempoEspera").parse(input)) {
					currentScreen = "MenuTempoEspera";
					procedures.get(currentScreen).doit();
					break;
				}
				if(commands.get("exit").parse(input)) {
					currentScreen = "Exit";
					procedures.get(currentScreen).doit();
					break;
				}
			}
			else if (currentScreen.equals("MenuEstadoLinha"))
			{
				if(commands.get("showLinhas").parse(input)) {
					procedures.get("ShowLinhas").doit();
					continue;
				}
				if(commands.get("getEstadoLinhaTodos").parse(input)) {
					procedures.get("GetEstadoLinhaTodos").doit();
					continue;
				}
				if(commands.get("getEstadoLinha").parse(input)) {
					procedures.get("GetEstadoLinha").doit();
					continue;
				}
			}
			else if (currentScreen.equals("MenuTempoEspera"))
			{
				// Terminal screen
				break;
			}
			else if (currentScreen.equals("Exit"))
			{
				// Terminal screen
				break;
			}
			
			procedures.get(currentScreen).doit();
		}
		
		sc.close();
	}
	
	// AUX METHODS
	
	private static void print(Object o) {
		System.out.println(o);
	}
	
	// STRUCTS
	
	// COMMAND
	@FunctionalInterface
	private interface Command {
		boolean parse(String input);
	}
	
	// PROCEDURE
	@FunctionalInterface
	private interface Procedure {
		void doit();
	}
}
