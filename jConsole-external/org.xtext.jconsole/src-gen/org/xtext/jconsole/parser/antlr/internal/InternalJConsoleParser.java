package org.xtext.jconsole.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.jconsole.services.JConsoleGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJConsoleParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_JAVABLOCK", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'main'", "'is'", "'imports'", "'variables'", "'command'", "'='", "'guard'", "'procedure'", "'screen'", "'{'", "'}'", "'do'", "'when'", "'if'", "'then'", "'else'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_JAVABLOCK=5;
    public static final int RULE_ID=4;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=7;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalJConsoleParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJConsoleParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJConsoleParser.tokenNames; }
    public String getGrammarFileName() { return "InternalJConsole.g"; }



     	private JConsoleGrammarAccess grammarAccess;

        public InternalJConsoleParser(TokenStream input, JConsoleGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "CLIApp";
       	}

       	@Override
       	protected JConsoleGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleCLIApp"
    // InternalJConsole.g:64:1: entryRuleCLIApp returns [EObject current=null] : iv_ruleCLIApp= ruleCLIApp EOF ;
    public final EObject entryRuleCLIApp() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCLIApp = null;


        try {
            // InternalJConsole.g:64:47: (iv_ruleCLIApp= ruleCLIApp EOF )
            // InternalJConsole.g:65:2: iv_ruleCLIApp= ruleCLIApp EOF
            {
             newCompositeNode(grammarAccess.getCLIAppRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCLIApp=ruleCLIApp();

            state._fsp--;

             current =iv_ruleCLIApp; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCLIApp"


    // $ANTLR start "ruleCLIApp"
    // InternalJConsole.g:71:1: ruleCLIApp returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImportBlock ) )? ( (lv_variables_1_0= ruleVariableBlock ) )? ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )* ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )* (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) ) ) ;
    public final EObject ruleCLIApp() throws RecognitionException {
        EObject current = null;

        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_imports_0_0 = null;

        EObject lv_variables_1_0 = null;

        EObject lv_commands_2_0 = null;

        EObject lv_guards_3_0 = null;

        EObject lv_screens_4_0 = null;

        EObject lv_procedures_5_0 = null;



        	enterRule();

        try {
            // InternalJConsole.g:77:2: ( ( ( (lv_imports_0_0= ruleImportBlock ) )? ( (lv_variables_1_0= ruleVariableBlock ) )? ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )* ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )* (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) ) ) )
            // InternalJConsole.g:78:2: ( ( (lv_imports_0_0= ruleImportBlock ) )? ( (lv_variables_1_0= ruleVariableBlock ) )? ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )* ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )* (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) ) )
            {
            // InternalJConsole.g:78:2: ( ( (lv_imports_0_0= ruleImportBlock ) )? ( (lv_variables_1_0= ruleVariableBlock ) )? ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )* ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )* (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) ) )
            // InternalJConsole.g:79:3: ( (lv_imports_0_0= ruleImportBlock ) )? ( (lv_variables_1_0= ruleVariableBlock ) )? ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )* ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )* (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) )
            {
            // InternalJConsole.g:79:3: ( (lv_imports_0_0= ruleImportBlock ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalJConsole.g:80:4: (lv_imports_0_0= ruleImportBlock )
                    {
                    // InternalJConsole.g:80:4: (lv_imports_0_0= ruleImportBlock )
                    // InternalJConsole.g:81:5: lv_imports_0_0= ruleImportBlock
                    {

                    					newCompositeNode(grammarAccess.getCLIAppAccess().getImportsImportBlockParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_imports_0_0=ruleImportBlock();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCLIAppRule());
                    					}
                    					set(
                    						current,
                    						"imports",
                    						lv_imports_0_0,
                    						"org.xtext.jconsole.JConsole.ImportBlock");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalJConsole.g:98:3: ( (lv_variables_1_0= ruleVariableBlock ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==15) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalJConsole.g:99:4: (lv_variables_1_0= ruleVariableBlock )
                    {
                    // InternalJConsole.g:99:4: (lv_variables_1_0= ruleVariableBlock )
                    // InternalJConsole.g:100:5: lv_variables_1_0= ruleVariableBlock
                    {

                    					newCompositeNode(grammarAccess.getCLIAppAccess().getVariablesVariableBlockParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_4);
                    lv_variables_1_0=ruleVariableBlock();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCLIAppRule());
                    					}
                    					set(
                    						current,
                    						"variables",
                    						lv_variables_1_0,
                    						"org.xtext.jconsole.JConsole.VariableBlock");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalJConsole.g:117:3: ( ( (lv_commands_2_0= ruleCommand ) ) | ( (lv_guards_3_0= ruleGuard ) ) )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }
                else if ( (LA3_0==18) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalJConsole.g:118:4: ( (lv_commands_2_0= ruleCommand ) )
            	    {
            	    // InternalJConsole.g:118:4: ( (lv_commands_2_0= ruleCommand ) )
            	    // InternalJConsole.g:119:5: (lv_commands_2_0= ruleCommand )
            	    {
            	    // InternalJConsole.g:119:5: (lv_commands_2_0= ruleCommand )
            	    // InternalJConsole.g:120:6: lv_commands_2_0= ruleCommand
            	    {

            	    						newCompositeNode(grammarAccess.getCLIAppAccess().getCommandsCommandParserRuleCall_2_0_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_commands_2_0=ruleCommand();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCLIAppRule());
            	    						}
            	    						add(
            	    							current,
            	    							"commands",
            	    							lv_commands_2_0,
            	    							"org.xtext.jconsole.JConsole.Command");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalJConsole.g:138:4: ( (lv_guards_3_0= ruleGuard ) )
            	    {
            	    // InternalJConsole.g:138:4: ( (lv_guards_3_0= ruleGuard ) )
            	    // InternalJConsole.g:139:5: (lv_guards_3_0= ruleGuard )
            	    {
            	    // InternalJConsole.g:139:5: (lv_guards_3_0= ruleGuard )
            	    // InternalJConsole.g:140:6: lv_guards_3_0= ruleGuard
            	    {

            	    						newCompositeNode(grammarAccess.getCLIAppAccess().getGuardsGuardParserRuleCall_2_1_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_guards_3_0=ruleGuard();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCLIAppRule());
            	    						}
            	    						add(
            	    							current,
            	    							"guards",
            	    							lv_guards_3_0,
            	    							"org.xtext.jconsole.JConsole.Guard");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // InternalJConsole.g:158:3: ( ( (lv_screens_4_0= ruleScreen ) ) | ( (lv_procedures_5_0= ruleProcedure ) ) )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==20) ) {
                    alt4=1;
                }
                else if ( (LA4_0==19) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalJConsole.g:159:4: ( (lv_screens_4_0= ruleScreen ) )
            	    {
            	    // InternalJConsole.g:159:4: ( (lv_screens_4_0= ruleScreen ) )
            	    // InternalJConsole.g:160:5: (lv_screens_4_0= ruleScreen )
            	    {
            	    // InternalJConsole.g:160:5: (lv_screens_4_0= ruleScreen )
            	    // InternalJConsole.g:161:6: lv_screens_4_0= ruleScreen
            	    {

            	    						newCompositeNode(grammarAccess.getCLIAppAccess().getScreensScreenParserRuleCall_3_0_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_screens_4_0=ruleScreen();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCLIAppRule());
            	    						}
            	    						add(
            	    							current,
            	    							"screens",
            	    							lv_screens_4_0,
            	    							"org.xtext.jconsole.JConsole.Screen");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalJConsole.g:179:4: ( (lv_procedures_5_0= ruleProcedure ) )
            	    {
            	    // InternalJConsole.g:179:4: ( (lv_procedures_5_0= ruleProcedure ) )
            	    // InternalJConsole.g:180:5: (lv_procedures_5_0= ruleProcedure )
            	    {
            	    // InternalJConsole.g:180:5: (lv_procedures_5_0= ruleProcedure )
            	    // InternalJConsole.g:181:6: lv_procedures_5_0= ruleProcedure
            	    {

            	    						newCompositeNode(grammarAccess.getCLIAppAccess().getProceduresProcedureParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_procedures_5_0=ruleProcedure();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getCLIAppRule());
            	    						}
            	    						add(
            	    							current,
            	    							"procedures",
            	    							lv_procedures_5_0,
            	    							"org.xtext.jconsole.JConsole.Procedure");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalJConsole.g:199:3: (otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) ) )
            // InternalJConsole.g:200:4: otherlv_6= 'main' otherlv_7= 'is' ( (otherlv_8= RULE_ID ) )
            {
            otherlv_6=(Token)match(input,12,FOLLOW_6); 

            				newLeafNode(otherlv_6, grammarAccess.getCLIAppAccess().getMainKeyword_4_0());
            			
            otherlv_7=(Token)match(input,13,FOLLOW_7); 

            				newLeafNode(otherlv_7, grammarAccess.getCLIAppAccess().getIsKeyword_4_1());
            			
            // InternalJConsole.g:208:4: ( (otherlv_8= RULE_ID ) )
            // InternalJConsole.g:209:5: (otherlv_8= RULE_ID )
            {
            // InternalJConsole.g:209:5: (otherlv_8= RULE_ID )
            // InternalJConsole.g:210:6: otherlv_8= RULE_ID
            {

            						if (current==null) {
            							current = createModelElement(grammarAccess.getCLIAppRule());
            						}
            					
            otherlv_8=(Token)match(input,RULE_ID,FOLLOW_2); 

            						newLeafNode(otherlv_8, grammarAccess.getCLIAppAccess().getInitialScreenCrossReference_4_2_0());
            					

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCLIApp"


    // $ANTLR start "entryRuleImportBlock"
    // InternalJConsole.g:226:1: entryRuleImportBlock returns [EObject current=null] : iv_ruleImportBlock= ruleImportBlock EOF ;
    public final EObject entryRuleImportBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportBlock = null;


        try {
            // InternalJConsole.g:226:52: (iv_ruleImportBlock= ruleImportBlock EOF )
            // InternalJConsole.g:227:2: iv_ruleImportBlock= ruleImportBlock EOF
            {
             newCompositeNode(grammarAccess.getImportBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportBlock=ruleImportBlock();

            state._fsp--;

             current =iv_ruleImportBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportBlock"


    // $ANTLR start "ruleImportBlock"
    // InternalJConsole.g:233:1: ruleImportBlock returns [EObject current=null] : (otherlv_0= 'imports' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) ;
    public final EObject ruleImportBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_lines_1_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:239:2: ( (otherlv_0= 'imports' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) )
            // InternalJConsole.g:240:2: (otherlv_0= 'imports' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            {
            // InternalJConsole.g:240:2: (otherlv_0= 'imports' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            // InternalJConsole.g:241:3: otherlv_0= 'imports' ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getImportBlockAccess().getImportsKeyword_0());
            		
            // InternalJConsole.g:245:3: ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            // InternalJConsole.g:246:4: (lv_lines_1_0= RULE_JAVABLOCK )
            {
            // InternalJConsole.g:246:4: (lv_lines_1_0= RULE_JAVABLOCK )
            // InternalJConsole.g:247:5: lv_lines_1_0= RULE_JAVABLOCK
            {
            lv_lines_1_0=(Token)match(input,RULE_JAVABLOCK,FOLLOW_2); 

            					newLeafNode(lv_lines_1_0, grammarAccess.getImportBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportBlockRule());
            					}
            					addWithLastConsumed(
            						current,
            						"lines",
            						lv_lines_1_0,
            						"org.xtext.jconsole.JConsole.JAVABLOCK");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportBlock"


    // $ANTLR start "entryRuleVariableBlock"
    // InternalJConsole.g:267:1: entryRuleVariableBlock returns [EObject current=null] : iv_ruleVariableBlock= ruleVariableBlock EOF ;
    public final EObject entryRuleVariableBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableBlock = null;


        try {
            // InternalJConsole.g:267:54: (iv_ruleVariableBlock= ruleVariableBlock EOF )
            // InternalJConsole.g:268:2: iv_ruleVariableBlock= ruleVariableBlock EOF
            {
             newCompositeNode(grammarAccess.getVariableBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariableBlock=ruleVariableBlock();

            state._fsp--;

             current =iv_ruleVariableBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableBlock"


    // $ANTLR start "ruleVariableBlock"
    // InternalJConsole.g:274:1: ruleVariableBlock returns [EObject current=null] : (otherlv_0= 'variables' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) ;
    public final EObject ruleVariableBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_lines_1_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:280:2: ( (otherlv_0= 'variables' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) )
            // InternalJConsole.g:281:2: (otherlv_0= 'variables' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            {
            // InternalJConsole.g:281:2: (otherlv_0= 'variables' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            // InternalJConsole.g:282:3: otherlv_0= 'variables' ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getVariableBlockAccess().getVariablesKeyword_0());
            		
            // InternalJConsole.g:286:3: ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            // InternalJConsole.g:287:4: (lv_lines_1_0= RULE_JAVABLOCK )
            {
            // InternalJConsole.g:287:4: (lv_lines_1_0= RULE_JAVABLOCK )
            // InternalJConsole.g:288:5: lv_lines_1_0= RULE_JAVABLOCK
            {
            lv_lines_1_0=(Token)match(input,RULE_JAVABLOCK,FOLLOW_2); 

            					newLeafNode(lv_lines_1_0, grammarAccess.getVariableBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVariableBlockRule());
            					}
            					addWithLastConsumed(
            						current,
            						"lines",
            						lv_lines_1_0,
            						"org.xtext.jconsole.JConsole.JAVABLOCK");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableBlock"


    // $ANTLR start "entryRuleCommand"
    // InternalJConsole.g:308:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // InternalJConsole.g:308:48: (iv_ruleCommand= ruleCommand EOF )
            // InternalJConsole.g:309:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalJConsole.g:315:1: ruleCommand returns [EObject current=null] : (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_command_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_command_3_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:321:2: ( (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_command_3_0= RULE_STRING ) ) ) )
            // InternalJConsole.g:322:2: (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_command_3_0= RULE_STRING ) ) )
            {
            // InternalJConsole.g:322:2: (otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_command_3_0= RULE_STRING ) ) )
            // InternalJConsole.g:323:3: otherlv_0= 'command' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_command_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getCommandAccess().getCommandKeyword_0());
            		
            // InternalJConsole.g:327:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalJConsole.g:328:4: (lv_name_1_0= RULE_ID )
            {
            // InternalJConsole.g:328:4: (lv_name_1_0= RULE_ID )
            // InternalJConsole.g:329:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getCommandAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommandRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getCommandAccess().getEqualsSignKeyword_2());
            		
            // InternalJConsole.g:349:3: ( (lv_command_3_0= RULE_STRING ) )
            // InternalJConsole.g:350:4: (lv_command_3_0= RULE_STRING )
            {
            // InternalJConsole.g:350:4: (lv_command_3_0= RULE_STRING )
            // InternalJConsole.g:351:5: lv_command_3_0= RULE_STRING
            {
            lv_command_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_command_3_0, grammarAccess.getCommandAccess().getCommandSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommandRule());
            					}
            					setWithLastConsumed(
            						current,
            						"command",
            						lv_command_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleGuard"
    // InternalJConsole.g:371:1: entryRuleGuard returns [EObject current=null] : iv_ruleGuard= ruleGuard EOF ;
    public final EObject entryRuleGuard() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuard = null;


        try {
            // InternalJConsole.g:371:46: (iv_ruleGuard= ruleGuard EOF )
            // InternalJConsole.g:372:2: iv_ruleGuard= ruleGuard EOF
            {
             newCompositeNode(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuard=ruleGuard();

            state._fsp--;

             current =iv_ruleGuard; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // InternalJConsole.g:378:1: ruleGuard returns [EObject current=null] : (otherlv_0= 'guard' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_guard_3_0= RULE_STRING ) ) ) ;
    public final EObject ruleGuard() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_guard_3_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:384:2: ( (otherlv_0= 'guard' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_guard_3_0= RULE_STRING ) ) ) )
            // InternalJConsole.g:385:2: (otherlv_0= 'guard' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_guard_3_0= RULE_STRING ) ) )
            {
            // InternalJConsole.g:385:2: (otherlv_0= 'guard' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_guard_3_0= RULE_STRING ) ) )
            // InternalJConsole.g:386:3: otherlv_0= 'guard' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_guard_3_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getGuardAccess().getGuardKeyword_0());
            		
            // InternalJConsole.g:390:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalJConsole.g:391:4: (lv_name_1_0= RULE_ID )
            {
            // InternalJConsole.g:391:4: (lv_name_1_0= RULE_ID )
            // InternalJConsole.g:392:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getGuardAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getGuardAccess().getEqualsSignKeyword_2());
            		
            // InternalJConsole.g:412:3: ( (lv_guard_3_0= RULE_STRING ) )
            // InternalJConsole.g:413:4: (lv_guard_3_0= RULE_STRING )
            {
            // InternalJConsole.g:413:4: (lv_guard_3_0= RULE_STRING )
            // InternalJConsole.g:414:5: lv_guard_3_0= RULE_STRING
            {
            lv_guard_3_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_guard_3_0, grammarAccess.getGuardAccess().getGuardSTRINGTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGuardRule());
            					}
            					setWithLastConsumed(
            						current,
            						"guard",
            						lv_guard_3_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleProcedure"
    // InternalJConsole.g:434:1: entryRuleProcedure returns [EObject current=null] : iv_ruleProcedure= ruleProcedure EOF ;
    public final EObject entryRuleProcedure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcedure = null;


        try {
            // InternalJConsole.g:434:50: (iv_ruleProcedure= ruleProcedure EOF )
            // InternalJConsole.g:435:2: iv_ruleProcedure= ruleProcedure EOF
            {
             newCompositeNode(grammarAccess.getProcedureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcedure=ruleProcedure();

            state._fsp--;

             current =iv_ruleProcedure; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcedure"


    // $ANTLR start "ruleProcedure"
    // InternalJConsole.g:441:1: ruleProcedure returns [EObject current=null] : (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) ( (lv_lines_2_0= RULE_JAVABLOCK ) ) ) ;
    public final EObject ruleProcedure() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_lines_2_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:447:2: ( (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) ( (lv_lines_2_0= RULE_JAVABLOCK ) ) ) )
            // InternalJConsole.g:448:2: (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) ( (lv_lines_2_0= RULE_JAVABLOCK ) ) )
            {
            // InternalJConsole.g:448:2: (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) ( (lv_lines_2_0= RULE_JAVABLOCK ) ) )
            // InternalJConsole.g:449:3: otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) ( (lv_lines_2_0= RULE_JAVABLOCK ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getProcedureAccess().getProcedureKeyword_0());
            		
            // InternalJConsole.g:453:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalJConsole.g:454:4: (lv_name_1_0= RULE_ID )
            {
            // InternalJConsole.g:454:4: (lv_name_1_0= RULE_ID )
            // InternalJConsole.g:455:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_1_0, grammarAccess.getProcedureAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcedureRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalJConsole.g:471:3: ( (lv_lines_2_0= RULE_JAVABLOCK ) )
            // InternalJConsole.g:472:4: (lv_lines_2_0= RULE_JAVABLOCK )
            {
            // InternalJConsole.g:472:4: (lv_lines_2_0= RULE_JAVABLOCK )
            // InternalJConsole.g:473:5: lv_lines_2_0= RULE_JAVABLOCK
            {
            lv_lines_2_0=(Token)match(input,RULE_JAVABLOCK,FOLLOW_2); 

            					newLeafNode(lv_lines_2_0, grammarAccess.getProcedureAccess().getLinesJAVABLOCKTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcedureRule());
            					}
            					addWithLastConsumed(
            						current,
            						"lines",
            						lv_lines_2_0,
            						"org.xtext.jconsole.JConsole.JAVABLOCK");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcedure"


    // $ANTLR start "entryRuleScreen"
    // InternalJConsole.g:493:1: entryRuleScreen returns [EObject current=null] : iv_ruleScreen= ruleScreen EOF ;
    public final EObject entryRuleScreen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScreen = null;


        try {
            // InternalJConsole.g:493:47: (iv_ruleScreen= ruleScreen EOF )
            // InternalJConsole.g:494:2: iv_ruleScreen= ruleScreen EOF
            {
             newCompositeNode(grammarAccess.getScreenRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScreen=ruleScreen();

            state._fsp--;

             current =iv_ruleScreen; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScreen"


    // $ANTLR start "ruleScreen"
    // InternalJConsole.g:500:1: ruleScreen returns [EObject current=null] : (otherlv_0= 'screen' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_internalProcedure_3_0= ruleDoBlock ) ) ( (lv_transitionSets_4_0= ruleWhenBlock ) )* otherlv_5= '}' ) ;
    public final EObject ruleScreen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        EObject lv_internalProcedure_3_0 = null;

        EObject lv_transitionSets_4_0 = null;



        	enterRule();

        try {
            // InternalJConsole.g:506:2: ( (otherlv_0= 'screen' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_internalProcedure_3_0= ruleDoBlock ) ) ( (lv_transitionSets_4_0= ruleWhenBlock ) )* otherlv_5= '}' ) )
            // InternalJConsole.g:507:2: (otherlv_0= 'screen' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_internalProcedure_3_0= ruleDoBlock ) ) ( (lv_transitionSets_4_0= ruleWhenBlock ) )* otherlv_5= '}' )
            {
            // InternalJConsole.g:507:2: (otherlv_0= 'screen' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_internalProcedure_3_0= ruleDoBlock ) ) ( (lv_transitionSets_4_0= ruleWhenBlock ) )* otherlv_5= '}' )
            // InternalJConsole.g:508:3: otherlv_0= 'screen' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_internalProcedure_3_0= ruleDoBlock ) ) ( (lv_transitionSets_4_0= ruleWhenBlock ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getScreenAccess().getScreenKeyword_0());
            		
            // InternalJConsole.g:512:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalJConsole.g:513:4: (lv_name_1_0= RULE_ID )
            {
            // InternalJConsole.g:513:4: (lv_name_1_0= RULE_ID )
            // InternalJConsole.g:514:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_1_0, grammarAccess.getScreenAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getScreenRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getScreenAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalJConsole.g:534:3: ( (lv_internalProcedure_3_0= ruleDoBlock ) )
            // InternalJConsole.g:535:4: (lv_internalProcedure_3_0= ruleDoBlock )
            {
            // InternalJConsole.g:535:4: (lv_internalProcedure_3_0= ruleDoBlock )
            // InternalJConsole.g:536:5: lv_internalProcedure_3_0= ruleDoBlock
            {

            					newCompositeNode(grammarAccess.getScreenAccess().getInternalProcedureDoBlockParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_13);
            lv_internalProcedure_3_0=ruleDoBlock();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScreenRule());
            					}
            					set(
            						current,
            						"internalProcedure",
            						lv_internalProcedure_3_0,
            						"org.xtext.jconsole.JConsole.DoBlock");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalJConsole.g:553:3: ( (lv_transitionSets_4_0= ruleWhenBlock ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==24) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalJConsole.g:554:4: (lv_transitionSets_4_0= ruleWhenBlock )
            	    {
            	    // InternalJConsole.g:554:4: (lv_transitionSets_4_0= ruleWhenBlock )
            	    // InternalJConsole.g:555:5: lv_transitionSets_4_0= ruleWhenBlock
            	    {

            	    					newCompositeNode(grammarAccess.getScreenAccess().getTransitionSetsWhenBlockParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_13);
            	    lv_transitionSets_4_0=ruleWhenBlock();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getScreenRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitionSets",
            	    						lv_transitionSets_4_0,
            	    						"org.xtext.jconsole.JConsole.WhenBlock");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_5=(Token)match(input,22,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getScreenAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScreen"


    // $ANTLR start "entryRuleDoBlock"
    // InternalJConsole.g:580:1: entryRuleDoBlock returns [EObject current=null] : iv_ruleDoBlock= ruleDoBlock EOF ;
    public final EObject entryRuleDoBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoBlock = null;


        try {
            // InternalJConsole.g:580:48: (iv_ruleDoBlock= ruleDoBlock EOF )
            // InternalJConsole.g:581:2: iv_ruleDoBlock= ruleDoBlock EOF
            {
             newCompositeNode(grammarAccess.getDoBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoBlock=ruleDoBlock();

            state._fsp--;

             current =iv_ruleDoBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoBlock"


    // $ANTLR start "ruleDoBlock"
    // InternalJConsole.g:587:1: ruleDoBlock returns [EObject current=null] : (otherlv_0= 'do' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) ;
    public final EObject ruleDoBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_lines_1_0=null;


        	enterRule();

        try {
            // InternalJConsole.g:593:2: ( (otherlv_0= 'do' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) ) )
            // InternalJConsole.g:594:2: (otherlv_0= 'do' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            {
            // InternalJConsole.g:594:2: (otherlv_0= 'do' ( (lv_lines_1_0= RULE_JAVABLOCK ) ) )
            // InternalJConsole.g:595:3: otherlv_0= 'do' ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_8); 

            			newLeafNode(otherlv_0, grammarAccess.getDoBlockAccess().getDoKeyword_0());
            		
            // InternalJConsole.g:599:3: ( (lv_lines_1_0= RULE_JAVABLOCK ) )
            // InternalJConsole.g:600:4: (lv_lines_1_0= RULE_JAVABLOCK )
            {
            // InternalJConsole.g:600:4: (lv_lines_1_0= RULE_JAVABLOCK )
            // InternalJConsole.g:601:5: lv_lines_1_0= RULE_JAVABLOCK
            {
            lv_lines_1_0=(Token)match(input,RULE_JAVABLOCK,FOLLOW_2); 

            					newLeafNode(lv_lines_1_0, grammarAccess.getDoBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDoBlockRule());
            					}
            					addWithLastConsumed(
            						current,
            						"lines",
            						lv_lines_1_0,
            						"org.xtext.jconsole.JConsole.JAVABLOCK");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoBlock"


    // $ANTLR start "entryRuleWhenBlock"
    // InternalJConsole.g:621:1: entryRuleWhenBlock returns [EObject current=null] : iv_ruleWhenBlock= ruleWhenBlock EOF ;
    public final EObject entryRuleWhenBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhenBlock = null;


        try {
            // InternalJConsole.g:621:50: (iv_ruleWhenBlock= ruleWhenBlock EOF )
            // InternalJConsole.g:622:2: iv_ruleWhenBlock= ruleWhenBlock EOF
            {
             newCompositeNode(grammarAccess.getWhenBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWhenBlock=ruleWhenBlock();

            state._fsp--;

             current =iv_ruleWhenBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhenBlock"


    // $ANTLR start "ruleWhenBlock"
    // InternalJConsole.g:628:1: ruleWhenBlock returns [EObject current=null] : (this_ProcedureWhen_0= ruleProcedureWhen | this_ScreenWhenBlock_1= ruleScreenWhenBlock ) ;
    public final EObject ruleWhenBlock() throws RecognitionException {
        EObject current = null;

        EObject this_ProcedureWhen_0 = null;

        EObject this_ScreenWhenBlock_1 = null;



        	enterRule();

        try {
            // InternalJConsole.g:634:2: ( (this_ProcedureWhen_0= ruleProcedureWhen | this_ScreenWhenBlock_1= ruleScreenWhenBlock ) )
            // InternalJConsole.g:635:2: (this_ProcedureWhen_0= ruleProcedureWhen | this_ScreenWhenBlock_1= ruleScreenWhenBlock )
            {
            // InternalJConsole.g:635:2: (this_ProcedureWhen_0= ruleProcedureWhen | this_ScreenWhenBlock_1= ruleScreenWhenBlock )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==24) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==RULE_ID) ) {
                    int LA6_2 = input.LA(3);

                    if ( (LA6_2==21) ) {
                        alt6=2;
                    }
                    else if ( (LA6_2==23) ) {
                        alt6=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalJConsole.g:636:3: this_ProcedureWhen_0= ruleProcedureWhen
                    {

                    			newCompositeNode(grammarAccess.getWhenBlockAccess().getProcedureWhenParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ProcedureWhen_0=ruleProcedureWhen();

                    state._fsp--;


                    			current = this_ProcedureWhen_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalJConsole.g:645:3: this_ScreenWhenBlock_1= ruleScreenWhenBlock
                    {

                    			newCompositeNode(grammarAccess.getWhenBlockAccess().getScreenWhenBlockParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ScreenWhenBlock_1=ruleScreenWhenBlock();

                    state._fsp--;


                    			current = this_ScreenWhenBlock_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhenBlock"


    // $ANTLR start "entryRuleProcedureWhen"
    // InternalJConsole.g:657:1: entryRuleProcedureWhen returns [EObject current=null] : iv_ruleProcedureWhen= ruleProcedureWhen EOF ;
    public final EObject entryRuleProcedureWhen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProcedureWhen = null;


        try {
            // InternalJConsole.g:657:54: (iv_ruleProcedureWhen= ruleProcedureWhen EOF )
            // InternalJConsole.g:658:2: iv_ruleProcedureWhen= ruleProcedureWhen EOF
            {
             newCompositeNode(grammarAccess.getProcedureWhenRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProcedureWhen=ruleProcedureWhen();

            state._fsp--;

             current =iv_ruleProcedureWhen; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProcedureWhen"


    // $ANTLR start "ruleProcedureWhen"
    // InternalJConsole.g:664:1: ruleProcedureWhen returns [EObject current=null] : (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'do' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleProcedureWhen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalJConsole.g:670:2: ( (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'do' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalJConsole.g:671:2: (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'do' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalJConsole.g:671:2: (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'do' ( (otherlv_3= RULE_ID ) ) )
            // InternalJConsole.g:672:3: otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= 'do' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getProcedureWhenAccess().getWhenKeyword_0());
            		
            // InternalJConsole.g:676:3: ( (otherlv_1= RULE_ID ) )
            // InternalJConsole.g:677:4: (otherlv_1= RULE_ID )
            {
            // InternalJConsole.g:677:4: (otherlv_1= RULE_ID )
            // InternalJConsole.g:678:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcedureWhenRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(otherlv_1, grammarAccess.getProcedureWhenAccess().getCommandCommandCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getProcedureWhenAccess().getDoKeyword_2());
            		
            // InternalJConsole.g:693:3: ( (otherlv_3= RULE_ID ) )
            // InternalJConsole.g:694:4: (otherlv_3= RULE_ID )
            {
            // InternalJConsole.g:694:4: (otherlv_3= RULE_ID )
            // InternalJConsole.g:695:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getProcedureWhenRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_3, grammarAccess.getProcedureWhenAccess().getProcedureProcedureCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProcedureWhen"


    // $ANTLR start "entryRuleScreenWhenBlock"
    // InternalJConsole.g:710:1: entryRuleScreenWhenBlock returns [EObject current=null] : iv_ruleScreenWhenBlock= ruleScreenWhenBlock EOF ;
    public final EObject entryRuleScreenWhenBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScreenWhenBlock = null;


        try {
            // InternalJConsole.g:710:56: (iv_ruleScreenWhenBlock= ruleScreenWhenBlock EOF )
            // InternalJConsole.g:711:2: iv_ruleScreenWhenBlock= ruleScreenWhenBlock EOF
            {
             newCompositeNode(grammarAccess.getScreenWhenBlockRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScreenWhenBlock=ruleScreenWhenBlock();

            state._fsp--;

             current =iv_ruleScreenWhenBlock; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScreenWhenBlock"


    // $ANTLR start "ruleScreenWhenBlock"
    // InternalJConsole.g:717:1: ruleScreenWhenBlock returns [EObject current=null] : (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) ) otherlv_6= '}' ) ;
    public final EObject ruleScreenWhenBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        EObject lv_transitions_4_0 = null;

        EObject lv_transitionElse_5_0 = null;



        	enterRule();

        try {
            // InternalJConsole.g:723:2: ( (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) ) otherlv_6= '}' ) )
            // InternalJConsole.g:724:2: (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) ) otherlv_6= '}' )
            {
            // InternalJConsole.g:724:2: (otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) ) otherlv_6= '}' )
            // InternalJConsole.g:725:3: otherlv_0= 'when' ( (otherlv_1= RULE_ID ) ) otherlv_2= '{' ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getScreenWhenBlockAccess().getWhenKeyword_0());
            		
            // InternalJConsole.g:729:3: ( (otherlv_1= RULE_ID ) )
            // InternalJConsole.g:730:4: (otherlv_1= RULE_ID )
            {
            // InternalJConsole.g:730:4: (otherlv_1= RULE_ID )
            // InternalJConsole.g:731:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getScreenWhenBlockRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(otherlv_1, grammarAccess.getScreenWhenBlockAccess().getCommandCommandCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getScreenWhenBlockAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalJConsole.g:746:3: ( ( (otherlv_3= RULE_ID ) ) | ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? ) )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_ID) ) {
                alt9=1;
            }
            else if ( (LA9_0==25) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalJConsole.g:747:4: ( (otherlv_3= RULE_ID ) )
                    {
                    // InternalJConsole.g:747:4: ( (otherlv_3= RULE_ID ) )
                    // InternalJConsole.g:748:5: (otherlv_3= RULE_ID )
                    {
                    // InternalJConsole.g:748:5: (otherlv_3= RULE_ID )
                    // InternalJConsole.g:749:6: otherlv_3= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getScreenWhenBlockRule());
                    						}
                    					
                    otherlv_3=(Token)match(input,RULE_ID,FOLLOW_15); 

                    						newLeafNode(otherlv_3, grammarAccess.getScreenWhenBlockAccess().getTransitionDirectScreenCrossReference_3_0_0());
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalJConsole.g:761:4: ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? )
                    {
                    // InternalJConsole.g:761:4: ( ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )? )
                    // InternalJConsole.g:762:5: ( (lv_transitions_4_0= ruleIfThen ) )+ ( (lv_transitionElse_5_0= ruleElse ) )?
                    {
                    // InternalJConsole.g:762:5: ( (lv_transitions_4_0= ruleIfThen ) )+
                    int cnt7=0;
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==25) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalJConsole.g:763:6: (lv_transitions_4_0= ruleIfThen )
                    	    {
                    	    // InternalJConsole.g:763:6: (lv_transitions_4_0= ruleIfThen )
                    	    // InternalJConsole.g:764:7: lv_transitions_4_0= ruleIfThen
                    	    {

                    	    							newCompositeNode(grammarAccess.getScreenWhenBlockAccess().getTransitionsIfThenParserRuleCall_3_1_0_0());
                    	    						
                    	    pushFollow(FOLLOW_16);
                    	    lv_transitions_4_0=ruleIfThen();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getScreenWhenBlockRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"transitions",
                    	    								lv_transitions_4_0,
                    	    								"org.xtext.jconsole.JConsole.IfThen");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt7 >= 1 ) break loop7;
                                EarlyExitException eee =
                                    new EarlyExitException(7, input);
                                throw eee;
                        }
                        cnt7++;
                    } while (true);

                    // InternalJConsole.g:781:5: ( (lv_transitionElse_5_0= ruleElse ) )?
                    int alt8=2;
                    int LA8_0 = input.LA(1);

                    if ( (LA8_0==27) ) {
                        alt8=1;
                    }
                    switch (alt8) {
                        case 1 :
                            // InternalJConsole.g:782:6: (lv_transitionElse_5_0= ruleElse )
                            {
                            // InternalJConsole.g:782:6: (lv_transitionElse_5_0= ruleElse )
                            // InternalJConsole.g:783:7: lv_transitionElse_5_0= ruleElse
                            {

                            							newCompositeNode(grammarAccess.getScreenWhenBlockAccess().getTransitionElseElseParserRuleCall_3_1_1_0());
                            						
                            pushFollow(FOLLOW_15);
                            lv_transitionElse_5_0=ruleElse();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getScreenWhenBlockRule());
                            							}
                            							set(
                            								current,
                            								"transitionElse",
                            								lv_transitionElse_5_0,
                            								"org.xtext.jconsole.JConsole.Else");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,22,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getScreenWhenBlockAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScreenWhenBlock"


    // $ANTLR start "entryRuleIfThen"
    // InternalJConsole.g:810:1: entryRuleIfThen returns [EObject current=null] : iv_ruleIfThen= ruleIfThen EOF ;
    public final EObject entryRuleIfThen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfThen = null;


        try {
            // InternalJConsole.g:810:47: (iv_ruleIfThen= ruleIfThen EOF )
            // InternalJConsole.g:811:2: iv_ruleIfThen= ruleIfThen EOF
            {
             newCompositeNode(grammarAccess.getIfThenRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIfThen=ruleIfThen();

            state._fsp--;

             current =iv_ruleIfThen; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfThen"


    // $ANTLR start "ruleIfThen"
    // InternalJConsole.g:817:1: ruleIfThen returns [EObject current=null] : (otherlv_0= 'if' ( (lv_condition_1_0= RULE_STRING ) ) otherlv_2= 'then' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleIfThen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_condition_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalJConsole.g:823:2: ( (otherlv_0= 'if' ( (lv_condition_1_0= RULE_STRING ) ) otherlv_2= 'then' ( (otherlv_3= RULE_ID ) ) ) )
            // InternalJConsole.g:824:2: (otherlv_0= 'if' ( (lv_condition_1_0= RULE_STRING ) ) otherlv_2= 'then' ( (otherlv_3= RULE_ID ) ) )
            {
            // InternalJConsole.g:824:2: (otherlv_0= 'if' ( (lv_condition_1_0= RULE_STRING ) ) otherlv_2= 'then' ( (otherlv_3= RULE_ID ) ) )
            // InternalJConsole.g:825:3: otherlv_0= 'if' ( (lv_condition_1_0= RULE_STRING ) ) otherlv_2= 'then' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getIfThenAccess().getIfKeyword_0());
            		
            // InternalJConsole.g:829:3: ( (lv_condition_1_0= RULE_STRING ) )
            // InternalJConsole.g:830:4: (lv_condition_1_0= RULE_STRING )
            {
            // InternalJConsole.g:830:4: (lv_condition_1_0= RULE_STRING )
            // InternalJConsole.g:831:5: lv_condition_1_0= RULE_STRING
            {
            lv_condition_1_0=(Token)match(input,RULE_STRING,FOLLOW_17); 

            					newLeafNode(lv_condition_1_0, grammarAccess.getIfThenAccess().getConditionSTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIfThenRule());
            					}
            					setWithLastConsumed(
            						current,
            						"condition",
            						lv_condition_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_2=(Token)match(input,26,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getIfThenAccess().getThenKeyword_2());
            		
            // InternalJConsole.g:851:3: ( (otherlv_3= RULE_ID ) )
            // InternalJConsole.g:852:4: (otherlv_3= RULE_ID )
            {
            // InternalJConsole.g:852:4: (otherlv_3= RULE_ID )
            // InternalJConsole.g:853:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIfThenRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_3, grammarAccess.getIfThenAccess().getScreenScreenCrossReference_3_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfThen"


    // $ANTLR start "entryRuleElse"
    // InternalJConsole.g:868:1: entryRuleElse returns [EObject current=null] : iv_ruleElse= ruleElse EOF ;
    public final EObject entryRuleElse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleElse = null;


        try {
            // InternalJConsole.g:868:45: (iv_ruleElse= ruleElse EOF )
            // InternalJConsole.g:869:2: iv_ruleElse= ruleElse EOF
            {
             newCompositeNode(grammarAccess.getElseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleElse=ruleElse();

            state._fsp--;

             current =iv_ruleElse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleElse"


    // $ANTLR start "ruleElse"
    // InternalJConsole.g:875:1: ruleElse returns [EObject current=null] : (otherlv_0= 'else' ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleElse() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalJConsole.g:881:2: ( (otherlv_0= 'else' ( (otherlv_1= RULE_ID ) ) ) )
            // InternalJConsole.g:882:2: (otherlv_0= 'else' ( (otherlv_1= RULE_ID ) ) )
            {
            // InternalJConsole.g:882:2: (otherlv_0= 'else' ( (otherlv_1= RULE_ID ) ) )
            // InternalJConsole.g:883:3: otherlv_0= 'else' ( (otherlv_1= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,27,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getElseAccess().getElseKeyword_0());
            		
            // InternalJConsole.g:887:3: ( (otherlv_1= RULE_ID ) )
            // InternalJConsole.g:888:4: (otherlv_1= RULE_ID )
            {
            // InternalJConsole.g:888:4: (otherlv_1= RULE_ID )
            // InternalJConsole.g:889:5: otherlv_1= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getElseRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_1, grammarAccess.getElseAccess().getScreenScreenCrossReference_1_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleElse"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000001D9000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00000000001D1000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000181000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000A400010L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000004000000L});

}