package org.xtext.jconsole.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.jconsole.services.JConsoleGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJConsoleParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_JAVABLOCK", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'main'", "'is'", "'imports'", "'variables'", "'command'", "'='", "'guard'", "'procedure'", "'screen'", "'{'", "'}'", "'do'", "'when'", "'if'", "'then'", "'else'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_JAVABLOCK=5;
    public static final int RULE_ID=4;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_INT=7;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalJConsoleParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJConsoleParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJConsoleParser.tokenNames; }
    public String getGrammarFileName() { return "InternalJConsole.g"; }


    	private JConsoleGrammarAccess grammarAccess;

    	public void setGrammarAccess(JConsoleGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleCLIApp"
    // InternalJConsole.g:53:1: entryRuleCLIApp : ruleCLIApp EOF ;
    public final void entryRuleCLIApp() throws RecognitionException {
        try {
            // InternalJConsole.g:54:1: ( ruleCLIApp EOF )
            // InternalJConsole.g:55:1: ruleCLIApp EOF
            {
             before(grammarAccess.getCLIAppRule()); 
            pushFollow(FOLLOW_1);
            ruleCLIApp();

            state._fsp--;

             after(grammarAccess.getCLIAppRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCLIApp"


    // $ANTLR start "ruleCLIApp"
    // InternalJConsole.g:62:1: ruleCLIApp : ( ( rule__CLIApp__Group__0 ) ) ;
    public final void ruleCLIApp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:66:2: ( ( ( rule__CLIApp__Group__0 ) ) )
            // InternalJConsole.g:67:2: ( ( rule__CLIApp__Group__0 ) )
            {
            // InternalJConsole.g:67:2: ( ( rule__CLIApp__Group__0 ) )
            // InternalJConsole.g:68:3: ( rule__CLIApp__Group__0 )
            {
             before(grammarAccess.getCLIAppAccess().getGroup()); 
            // InternalJConsole.g:69:3: ( rule__CLIApp__Group__0 )
            // InternalJConsole.g:69:4: rule__CLIApp__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCLIAppAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCLIApp"


    // $ANTLR start "entryRuleImportBlock"
    // InternalJConsole.g:78:1: entryRuleImportBlock : ruleImportBlock EOF ;
    public final void entryRuleImportBlock() throws RecognitionException {
        try {
            // InternalJConsole.g:79:1: ( ruleImportBlock EOF )
            // InternalJConsole.g:80:1: ruleImportBlock EOF
            {
             before(grammarAccess.getImportBlockRule()); 
            pushFollow(FOLLOW_1);
            ruleImportBlock();

            state._fsp--;

             after(grammarAccess.getImportBlockRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportBlock"


    // $ANTLR start "ruleImportBlock"
    // InternalJConsole.g:87:1: ruleImportBlock : ( ( rule__ImportBlock__Group__0 ) ) ;
    public final void ruleImportBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:91:2: ( ( ( rule__ImportBlock__Group__0 ) ) )
            // InternalJConsole.g:92:2: ( ( rule__ImportBlock__Group__0 ) )
            {
            // InternalJConsole.g:92:2: ( ( rule__ImportBlock__Group__0 ) )
            // InternalJConsole.g:93:3: ( rule__ImportBlock__Group__0 )
            {
             before(grammarAccess.getImportBlockAccess().getGroup()); 
            // InternalJConsole.g:94:3: ( rule__ImportBlock__Group__0 )
            // InternalJConsole.g:94:4: rule__ImportBlock__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportBlock__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportBlockAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportBlock"


    // $ANTLR start "entryRuleVariableBlock"
    // InternalJConsole.g:103:1: entryRuleVariableBlock : ruleVariableBlock EOF ;
    public final void entryRuleVariableBlock() throws RecognitionException {
        try {
            // InternalJConsole.g:104:1: ( ruleVariableBlock EOF )
            // InternalJConsole.g:105:1: ruleVariableBlock EOF
            {
             before(grammarAccess.getVariableBlockRule()); 
            pushFollow(FOLLOW_1);
            ruleVariableBlock();

            state._fsp--;

             after(grammarAccess.getVariableBlockRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableBlock"


    // $ANTLR start "ruleVariableBlock"
    // InternalJConsole.g:112:1: ruleVariableBlock : ( ( rule__VariableBlock__Group__0 ) ) ;
    public final void ruleVariableBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:116:2: ( ( ( rule__VariableBlock__Group__0 ) ) )
            // InternalJConsole.g:117:2: ( ( rule__VariableBlock__Group__0 ) )
            {
            // InternalJConsole.g:117:2: ( ( rule__VariableBlock__Group__0 ) )
            // InternalJConsole.g:118:3: ( rule__VariableBlock__Group__0 )
            {
             before(grammarAccess.getVariableBlockAccess().getGroup()); 
            // InternalJConsole.g:119:3: ( rule__VariableBlock__Group__0 )
            // InternalJConsole.g:119:4: rule__VariableBlock__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__VariableBlock__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariableBlockAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableBlock"


    // $ANTLR start "entryRuleCommand"
    // InternalJConsole.g:128:1: entryRuleCommand : ruleCommand EOF ;
    public final void entryRuleCommand() throws RecognitionException {
        try {
            // InternalJConsole.g:129:1: ( ruleCommand EOF )
            // InternalJConsole.g:130:1: ruleCommand EOF
            {
             before(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleCommand();

            state._fsp--;

             after(grammarAccess.getCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalJConsole.g:137:1: ruleCommand : ( ( rule__Command__Group__0 ) ) ;
    public final void ruleCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:141:2: ( ( ( rule__Command__Group__0 ) ) )
            // InternalJConsole.g:142:2: ( ( rule__Command__Group__0 ) )
            {
            // InternalJConsole.g:142:2: ( ( rule__Command__Group__0 ) )
            // InternalJConsole.g:143:3: ( rule__Command__Group__0 )
            {
             before(grammarAccess.getCommandAccess().getGroup()); 
            // InternalJConsole.g:144:3: ( rule__Command__Group__0 )
            // InternalJConsole.g:144:4: rule__Command__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Command__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleGuard"
    // InternalJConsole.g:153:1: entryRuleGuard : ruleGuard EOF ;
    public final void entryRuleGuard() throws RecognitionException {
        try {
            // InternalJConsole.g:154:1: ( ruleGuard EOF )
            // InternalJConsole.g:155:1: ruleGuard EOF
            {
             before(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_1);
            ruleGuard();

            state._fsp--;

             after(grammarAccess.getGuardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // InternalJConsole.g:162:1: ruleGuard : ( ( rule__Guard__Group__0 ) ) ;
    public final void ruleGuard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:166:2: ( ( ( rule__Guard__Group__0 ) ) )
            // InternalJConsole.g:167:2: ( ( rule__Guard__Group__0 ) )
            {
            // InternalJConsole.g:167:2: ( ( rule__Guard__Group__0 ) )
            // InternalJConsole.g:168:3: ( rule__Guard__Group__0 )
            {
             before(grammarAccess.getGuardAccess().getGroup()); 
            // InternalJConsole.g:169:3: ( rule__Guard__Group__0 )
            // InternalJConsole.g:169:4: rule__Guard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Guard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGuardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleProcedure"
    // InternalJConsole.g:178:1: entryRuleProcedure : ruleProcedure EOF ;
    public final void entryRuleProcedure() throws RecognitionException {
        try {
            // InternalJConsole.g:179:1: ( ruleProcedure EOF )
            // InternalJConsole.g:180:1: ruleProcedure EOF
            {
             before(grammarAccess.getProcedureRule()); 
            pushFollow(FOLLOW_1);
            ruleProcedure();

            state._fsp--;

             after(grammarAccess.getProcedureRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcedure"


    // $ANTLR start "ruleProcedure"
    // InternalJConsole.g:187:1: ruleProcedure : ( ( rule__Procedure__Group__0 ) ) ;
    public final void ruleProcedure() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:191:2: ( ( ( rule__Procedure__Group__0 ) ) )
            // InternalJConsole.g:192:2: ( ( rule__Procedure__Group__0 ) )
            {
            // InternalJConsole.g:192:2: ( ( rule__Procedure__Group__0 ) )
            // InternalJConsole.g:193:3: ( rule__Procedure__Group__0 )
            {
             before(grammarAccess.getProcedureAccess().getGroup()); 
            // InternalJConsole.g:194:3: ( rule__Procedure__Group__0 )
            // InternalJConsole.g:194:4: rule__Procedure__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Procedure__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProcedureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcedure"


    // $ANTLR start "entryRuleScreen"
    // InternalJConsole.g:203:1: entryRuleScreen : ruleScreen EOF ;
    public final void entryRuleScreen() throws RecognitionException {
        try {
            // InternalJConsole.g:204:1: ( ruleScreen EOF )
            // InternalJConsole.g:205:1: ruleScreen EOF
            {
             before(grammarAccess.getScreenRule()); 
            pushFollow(FOLLOW_1);
            ruleScreen();

            state._fsp--;

             after(grammarAccess.getScreenRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScreen"


    // $ANTLR start "ruleScreen"
    // InternalJConsole.g:212:1: ruleScreen : ( ( rule__Screen__Group__0 ) ) ;
    public final void ruleScreen() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:216:2: ( ( ( rule__Screen__Group__0 ) ) )
            // InternalJConsole.g:217:2: ( ( rule__Screen__Group__0 ) )
            {
            // InternalJConsole.g:217:2: ( ( rule__Screen__Group__0 ) )
            // InternalJConsole.g:218:3: ( rule__Screen__Group__0 )
            {
             before(grammarAccess.getScreenAccess().getGroup()); 
            // InternalJConsole.g:219:3: ( rule__Screen__Group__0 )
            // InternalJConsole.g:219:4: rule__Screen__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Screen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getScreenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScreen"


    // $ANTLR start "entryRuleDoBlock"
    // InternalJConsole.g:228:1: entryRuleDoBlock : ruleDoBlock EOF ;
    public final void entryRuleDoBlock() throws RecognitionException {
        try {
            // InternalJConsole.g:229:1: ( ruleDoBlock EOF )
            // InternalJConsole.g:230:1: ruleDoBlock EOF
            {
             before(grammarAccess.getDoBlockRule()); 
            pushFollow(FOLLOW_1);
            ruleDoBlock();

            state._fsp--;

             after(grammarAccess.getDoBlockRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDoBlock"


    // $ANTLR start "ruleDoBlock"
    // InternalJConsole.g:237:1: ruleDoBlock : ( ( rule__DoBlock__Group__0 ) ) ;
    public final void ruleDoBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:241:2: ( ( ( rule__DoBlock__Group__0 ) ) )
            // InternalJConsole.g:242:2: ( ( rule__DoBlock__Group__0 ) )
            {
            // InternalJConsole.g:242:2: ( ( rule__DoBlock__Group__0 ) )
            // InternalJConsole.g:243:3: ( rule__DoBlock__Group__0 )
            {
             before(grammarAccess.getDoBlockAccess().getGroup()); 
            // InternalJConsole.g:244:3: ( rule__DoBlock__Group__0 )
            // InternalJConsole.g:244:4: rule__DoBlock__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DoBlock__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDoBlockAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDoBlock"


    // $ANTLR start "entryRuleWhenBlock"
    // InternalJConsole.g:253:1: entryRuleWhenBlock : ruleWhenBlock EOF ;
    public final void entryRuleWhenBlock() throws RecognitionException {
        try {
            // InternalJConsole.g:254:1: ( ruleWhenBlock EOF )
            // InternalJConsole.g:255:1: ruleWhenBlock EOF
            {
             before(grammarAccess.getWhenBlockRule()); 
            pushFollow(FOLLOW_1);
            ruleWhenBlock();

            state._fsp--;

             after(grammarAccess.getWhenBlockRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhenBlock"


    // $ANTLR start "ruleWhenBlock"
    // InternalJConsole.g:262:1: ruleWhenBlock : ( ( rule__WhenBlock__Alternatives ) ) ;
    public final void ruleWhenBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:266:2: ( ( ( rule__WhenBlock__Alternatives ) ) )
            // InternalJConsole.g:267:2: ( ( rule__WhenBlock__Alternatives ) )
            {
            // InternalJConsole.g:267:2: ( ( rule__WhenBlock__Alternatives ) )
            // InternalJConsole.g:268:3: ( rule__WhenBlock__Alternatives )
            {
             before(grammarAccess.getWhenBlockAccess().getAlternatives()); 
            // InternalJConsole.g:269:3: ( rule__WhenBlock__Alternatives )
            // InternalJConsole.g:269:4: rule__WhenBlock__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__WhenBlock__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWhenBlockAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhenBlock"


    // $ANTLR start "entryRuleProcedureWhen"
    // InternalJConsole.g:278:1: entryRuleProcedureWhen : ruleProcedureWhen EOF ;
    public final void entryRuleProcedureWhen() throws RecognitionException {
        try {
            // InternalJConsole.g:279:1: ( ruleProcedureWhen EOF )
            // InternalJConsole.g:280:1: ruleProcedureWhen EOF
            {
             before(grammarAccess.getProcedureWhenRule()); 
            pushFollow(FOLLOW_1);
            ruleProcedureWhen();

            state._fsp--;

             after(grammarAccess.getProcedureWhenRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProcedureWhen"


    // $ANTLR start "ruleProcedureWhen"
    // InternalJConsole.g:287:1: ruleProcedureWhen : ( ( rule__ProcedureWhen__Group__0 ) ) ;
    public final void ruleProcedureWhen() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:291:2: ( ( ( rule__ProcedureWhen__Group__0 ) ) )
            // InternalJConsole.g:292:2: ( ( rule__ProcedureWhen__Group__0 ) )
            {
            // InternalJConsole.g:292:2: ( ( rule__ProcedureWhen__Group__0 ) )
            // InternalJConsole.g:293:3: ( rule__ProcedureWhen__Group__0 )
            {
             before(grammarAccess.getProcedureWhenAccess().getGroup()); 
            // InternalJConsole.g:294:3: ( rule__ProcedureWhen__Group__0 )
            // InternalJConsole.g:294:4: rule__ProcedureWhen__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getProcedureWhenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProcedureWhen"


    // $ANTLR start "entryRuleScreenWhenBlock"
    // InternalJConsole.g:303:1: entryRuleScreenWhenBlock : ruleScreenWhenBlock EOF ;
    public final void entryRuleScreenWhenBlock() throws RecognitionException {
        try {
            // InternalJConsole.g:304:1: ( ruleScreenWhenBlock EOF )
            // InternalJConsole.g:305:1: ruleScreenWhenBlock EOF
            {
             before(grammarAccess.getScreenWhenBlockRule()); 
            pushFollow(FOLLOW_1);
            ruleScreenWhenBlock();

            state._fsp--;

             after(grammarAccess.getScreenWhenBlockRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScreenWhenBlock"


    // $ANTLR start "ruleScreenWhenBlock"
    // InternalJConsole.g:312:1: ruleScreenWhenBlock : ( ( rule__ScreenWhenBlock__Group__0 ) ) ;
    public final void ruleScreenWhenBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:316:2: ( ( ( rule__ScreenWhenBlock__Group__0 ) ) )
            // InternalJConsole.g:317:2: ( ( rule__ScreenWhenBlock__Group__0 ) )
            {
            // InternalJConsole.g:317:2: ( ( rule__ScreenWhenBlock__Group__0 ) )
            // InternalJConsole.g:318:3: ( rule__ScreenWhenBlock__Group__0 )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getGroup()); 
            // InternalJConsole.g:319:3: ( rule__ScreenWhenBlock__Group__0 )
            // InternalJConsole.g:319:4: rule__ScreenWhenBlock__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getScreenWhenBlockAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScreenWhenBlock"


    // $ANTLR start "entryRuleIfThen"
    // InternalJConsole.g:328:1: entryRuleIfThen : ruleIfThen EOF ;
    public final void entryRuleIfThen() throws RecognitionException {
        try {
            // InternalJConsole.g:329:1: ( ruleIfThen EOF )
            // InternalJConsole.g:330:1: ruleIfThen EOF
            {
             before(grammarAccess.getIfThenRule()); 
            pushFollow(FOLLOW_1);
            ruleIfThen();

            state._fsp--;

             after(grammarAccess.getIfThenRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIfThen"


    // $ANTLR start "ruleIfThen"
    // InternalJConsole.g:337:1: ruleIfThen : ( ( rule__IfThen__Group__0 ) ) ;
    public final void ruleIfThen() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:341:2: ( ( ( rule__IfThen__Group__0 ) ) )
            // InternalJConsole.g:342:2: ( ( rule__IfThen__Group__0 ) )
            {
            // InternalJConsole.g:342:2: ( ( rule__IfThen__Group__0 ) )
            // InternalJConsole.g:343:3: ( rule__IfThen__Group__0 )
            {
             before(grammarAccess.getIfThenAccess().getGroup()); 
            // InternalJConsole.g:344:3: ( rule__IfThen__Group__0 )
            // InternalJConsole.g:344:4: rule__IfThen__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IfThen__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIfThenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIfThen"


    // $ANTLR start "entryRuleElse"
    // InternalJConsole.g:353:1: entryRuleElse : ruleElse EOF ;
    public final void entryRuleElse() throws RecognitionException {
        try {
            // InternalJConsole.g:354:1: ( ruleElse EOF )
            // InternalJConsole.g:355:1: ruleElse EOF
            {
             before(grammarAccess.getElseRule()); 
            pushFollow(FOLLOW_1);
            ruleElse();

            state._fsp--;

             after(grammarAccess.getElseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElse"


    // $ANTLR start "ruleElse"
    // InternalJConsole.g:362:1: ruleElse : ( ( rule__Else__Group__0 ) ) ;
    public final void ruleElse() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:366:2: ( ( ( rule__Else__Group__0 ) ) )
            // InternalJConsole.g:367:2: ( ( rule__Else__Group__0 ) )
            {
            // InternalJConsole.g:367:2: ( ( rule__Else__Group__0 ) )
            // InternalJConsole.g:368:3: ( rule__Else__Group__0 )
            {
             before(grammarAccess.getElseAccess().getGroup()); 
            // InternalJConsole.g:369:3: ( rule__Else__Group__0 )
            // InternalJConsole.g:369:4: rule__Else__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Else__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getElseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElse"


    // $ANTLR start "rule__CLIApp__Alternatives_2"
    // InternalJConsole.g:377:1: rule__CLIApp__Alternatives_2 : ( ( ( rule__CLIApp__CommandsAssignment_2_0 ) ) | ( ( rule__CLIApp__GuardsAssignment_2_1 ) ) );
    public final void rule__CLIApp__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:381:1: ( ( ( rule__CLIApp__CommandsAssignment_2_0 ) ) | ( ( rule__CLIApp__GuardsAssignment_2_1 ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==16) ) {
                alt1=1;
            }
            else if ( (LA1_0==18) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalJConsole.g:382:2: ( ( rule__CLIApp__CommandsAssignment_2_0 ) )
                    {
                    // InternalJConsole.g:382:2: ( ( rule__CLIApp__CommandsAssignment_2_0 ) )
                    // InternalJConsole.g:383:3: ( rule__CLIApp__CommandsAssignment_2_0 )
                    {
                     before(grammarAccess.getCLIAppAccess().getCommandsAssignment_2_0()); 
                    // InternalJConsole.g:384:3: ( rule__CLIApp__CommandsAssignment_2_0 )
                    // InternalJConsole.g:384:4: rule__CLIApp__CommandsAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__CommandsAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getCLIAppAccess().getCommandsAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJConsole.g:388:2: ( ( rule__CLIApp__GuardsAssignment_2_1 ) )
                    {
                    // InternalJConsole.g:388:2: ( ( rule__CLIApp__GuardsAssignment_2_1 ) )
                    // InternalJConsole.g:389:3: ( rule__CLIApp__GuardsAssignment_2_1 )
                    {
                     before(grammarAccess.getCLIAppAccess().getGuardsAssignment_2_1()); 
                    // InternalJConsole.g:390:3: ( rule__CLIApp__GuardsAssignment_2_1 )
                    // InternalJConsole.g:390:4: rule__CLIApp__GuardsAssignment_2_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__GuardsAssignment_2_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getCLIAppAccess().getGuardsAssignment_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Alternatives_2"


    // $ANTLR start "rule__CLIApp__Alternatives_3"
    // InternalJConsole.g:398:1: rule__CLIApp__Alternatives_3 : ( ( ( rule__CLIApp__ScreensAssignment_3_0 ) ) | ( ( rule__CLIApp__ProceduresAssignment_3_1 ) ) );
    public final void rule__CLIApp__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:402:1: ( ( ( rule__CLIApp__ScreensAssignment_3_0 ) ) | ( ( rule__CLIApp__ProceduresAssignment_3_1 ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==20) ) {
                alt2=1;
            }
            else if ( (LA2_0==19) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalJConsole.g:403:2: ( ( rule__CLIApp__ScreensAssignment_3_0 ) )
                    {
                    // InternalJConsole.g:403:2: ( ( rule__CLIApp__ScreensAssignment_3_0 ) )
                    // InternalJConsole.g:404:3: ( rule__CLIApp__ScreensAssignment_3_0 )
                    {
                     before(grammarAccess.getCLIAppAccess().getScreensAssignment_3_0()); 
                    // InternalJConsole.g:405:3: ( rule__CLIApp__ScreensAssignment_3_0 )
                    // InternalJConsole.g:405:4: rule__CLIApp__ScreensAssignment_3_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__ScreensAssignment_3_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getCLIAppAccess().getScreensAssignment_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJConsole.g:409:2: ( ( rule__CLIApp__ProceduresAssignment_3_1 ) )
                    {
                    // InternalJConsole.g:409:2: ( ( rule__CLIApp__ProceduresAssignment_3_1 ) )
                    // InternalJConsole.g:410:3: ( rule__CLIApp__ProceduresAssignment_3_1 )
                    {
                     before(grammarAccess.getCLIAppAccess().getProceduresAssignment_3_1()); 
                    // InternalJConsole.g:411:3: ( rule__CLIApp__ProceduresAssignment_3_1 )
                    // InternalJConsole.g:411:4: rule__CLIApp__ProceduresAssignment_3_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__ProceduresAssignment_3_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getCLIAppAccess().getProceduresAssignment_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Alternatives_3"


    // $ANTLR start "rule__WhenBlock__Alternatives"
    // InternalJConsole.g:419:1: rule__WhenBlock__Alternatives : ( ( ruleProcedureWhen ) | ( ruleScreenWhenBlock ) );
    public final void rule__WhenBlock__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:423:1: ( ( ruleProcedureWhen ) | ( ruleScreenWhenBlock ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==24) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==RULE_ID) ) {
                    int LA3_2 = input.LA(3);

                    if ( (LA3_2==23) ) {
                        alt3=1;
                    }
                    else if ( (LA3_2==21) ) {
                        alt3=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 2, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalJConsole.g:424:2: ( ruleProcedureWhen )
                    {
                    // InternalJConsole.g:424:2: ( ruleProcedureWhen )
                    // InternalJConsole.g:425:3: ruleProcedureWhen
                    {
                     before(grammarAccess.getWhenBlockAccess().getProcedureWhenParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleProcedureWhen();

                    state._fsp--;

                     after(grammarAccess.getWhenBlockAccess().getProcedureWhenParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJConsole.g:430:2: ( ruleScreenWhenBlock )
                    {
                    // InternalJConsole.g:430:2: ( ruleScreenWhenBlock )
                    // InternalJConsole.g:431:3: ruleScreenWhenBlock
                    {
                     before(grammarAccess.getWhenBlockAccess().getScreenWhenBlockParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleScreenWhenBlock();

                    state._fsp--;

                     after(grammarAccess.getWhenBlockAccess().getScreenWhenBlockParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenBlock__Alternatives"


    // $ANTLR start "rule__ScreenWhenBlock__Alternatives_3"
    // InternalJConsole.g:440:1: rule__ScreenWhenBlock__Alternatives_3 : ( ( ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 ) ) | ( ( rule__ScreenWhenBlock__Group_3_1__0 ) ) );
    public final void rule__ScreenWhenBlock__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:444:1: ( ( ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 ) ) | ( ( rule__ScreenWhenBlock__Group_3_1__0 ) ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID) ) {
                alt4=1;
            }
            else if ( (LA4_0==25) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalJConsole.g:445:2: ( ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 ) )
                    {
                    // InternalJConsole.g:445:2: ( ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 ) )
                    // InternalJConsole.g:446:3: ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 )
                    {
                     before(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectAssignment_3_0()); 
                    // InternalJConsole.g:447:3: ( rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 )
                    // InternalJConsole.g:447:4: rule__ScreenWhenBlock__TransitionDirectAssignment_3_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ScreenWhenBlock__TransitionDirectAssignment_3_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectAssignment_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalJConsole.g:451:2: ( ( rule__ScreenWhenBlock__Group_3_1__0 ) )
                    {
                    // InternalJConsole.g:451:2: ( ( rule__ScreenWhenBlock__Group_3_1__0 ) )
                    // InternalJConsole.g:452:3: ( rule__ScreenWhenBlock__Group_3_1__0 )
                    {
                     before(grammarAccess.getScreenWhenBlockAccess().getGroup_3_1()); 
                    // InternalJConsole.g:453:3: ( rule__ScreenWhenBlock__Group_3_1__0 )
                    // InternalJConsole.g:453:4: rule__ScreenWhenBlock__Group_3_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ScreenWhenBlock__Group_3_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getScreenWhenBlockAccess().getGroup_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Alternatives_3"


    // $ANTLR start "rule__CLIApp__Group__0"
    // InternalJConsole.g:461:1: rule__CLIApp__Group__0 : rule__CLIApp__Group__0__Impl rule__CLIApp__Group__1 ;
    public final void rule__CLIApp__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:465:1: ( rule__CLIApp__Group__0__Impl rule__CLIApp__Group__1 )
            // InternalJConsole.g:466:2: rule__CLIApp__Group__0__Impl rule__CLIApp__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__CLIApp__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__0"


    // $ANTLR start "rule__CLIApp__Group__0__Impl"
    // InternalJConsole.g:473:1: rule__CLIApp__Group__0__Impl : ( ( rule__CLIApp__ImportsAssignment_0 )? ) ;
    public final void rule__CLIApp__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:477:1: ( ( ( rule__CLIApp__ImportsAssignment_0 )? ) )
            // InternalJConsole.g:478:1: ( ( rule__CLIApp__ImportsAssignment_0 )? )
            {
            // InternalJConsole.g:478:1: ( ( rule__CLIApp__ImportsAssignment_0 )? )
            // InternalJConsole.g:479:2: ( rule__CLIApp__ImportsAssignment_0 )?
            {
             before(grammarAccess.getCLIAppAccess().getImportsAssignment_0()); 
            // InternalJConsole.g:480:2: ( rule__CLIApp__ImportsAssignment_0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==14) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalJConsole.g:480:3: rule__CLIApp__ImportsAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__ImportsAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCLIAppAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__0__Impl"


    // $ANTLR start "rule__CLIApp__Group__1"
    // InternalJConsole.g:488:1: rule__CLIApp__Group__1 : rule__CLIApp__Group__1__Impl rule__CLIApp__Group__2 ;
    public final void rule__CLIApp__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:492:1: ( rule__CLIApp__Group__1__Impl rule__CLIApp__Group__2 )
            // InternalJConsole.g:493:2: rule__CLIApp__Group__1__Impl rule__CLIApp__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__CLIApp__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__1"


    // $ANTLR start "rule__CLIApp__Group__1__Impl"
    // InternalJConsole.g:500:1: rule__CLIApp__Group__1__Impl : ( ( rule__CLIApp__VariablesAssignment_1 )? ) ;
    public final void rule__CLIApp__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:504:1: ( ( ( rule__CLIApp__VariablesAssignment_1 )? ) )
            // InternalJConsole.g:505:1: ( ( rule__CLIApp__VariablesAssignment_1 )? )
            {
            // InternalJConsole.g:505:1: ( ( rule__CLIApp__VariablesAssignment_1 )? )
            // InternalJConsole.g:506:2: ( rule__CLIApp__VariablesAssignment_1 )?
            {
             before(grammarAccess.getCLIAppAccess().getVariablesAssignment_1()); 
            // InternalJConsole.g:507:2: ( rule__CLIApp__VariablesAssignment_1 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalJConsole.g:507:3: rule__CLIApp__VariablesAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__CLIApp__VariablesAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCLIAppAccess().getVariablesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__1__Impl"


    // $ANTLR start "rule__CLIApp__Group__2"
    // InternalJConsole.g:515:1: rule__CLIApp__Group__2 : rule__CLIApp__Group__2__Impl rule__CLIApp__Group__3 ;
    public final void rule__CLIApp__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:519:1: ( rule__CLIApp__Group__2__Impl rule__CLIApp__Group__3 )
            // InternalJConsole.g:520:2: rule__CLIApp__Group__2__Impl rule__CLIApp__Group__3
            {
            pushFollow(FOLLOW_3);
            rule__CLIApp__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__2"


    // $ANTLR start "rule__CLIApp__Group__2__Impl"
    // InternalJConsole.g:527:1: rule__CLIApp__Group__2__Impl : ( ( rule__CLIApp__Alternatives_2 )* ) ;
    public final void rule__CLIApp__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:531:1: ( ( ( rule__CLIApp__Alternatives_2 )* ) )
            // InternalJConsole.g:532:1: ( ( rule__CLIApp__Alternatives_2 )* )
            {
            // InternalJConsole.g:532:1: ( ( rule__CLIApp__Alternatives_2 )* )
            // InternalJConsole.g:533:2: ( rule__CLIApp__Alternatives_2 )*
            {
             before(grammarAccess.getCLIAppAccess().getAlternatives_2()); 
            // InternalJConsole.g:534:2: ( rule__CLIApp__Alternatives_2 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==16||LA7_0==18) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalJConsole.g:534:3: rule__CLIApp__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__CLIApp__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getCLIAppAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__2__Impl"


    // $ANTLR start "rule__CLIApp__Group__3"
    // InternalJConsole.g:542:1: rule__CLIApp__Group__3 : rule__CLIApp__Group__3__Impl rule__CLIApp__Group__4 ;
    public final void rule__CLIApp__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:546:1: ( rule__CLIApp__Group__3__Impl rule__CLIApp__Group__4 )
            // InternalJConsole.g:547:2: rule__CLIApp__Group__3__Impl rule__CLIApp__Group__4
            {
            pushFollow(FOLLOW_3);
            rule__CLIApp__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__3"


    // $ANTLR start "rule__CLIApp__Group__3__Impl"
    // InternalJConsole.g:554:1: rule__CLIApp__Group__3__Impl : ( ( rule__CLIApp__Alternatives_3 )* ) ;
    public final void rule__CLIApp__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:558:1: ( ( ( rule__CLIApp__Alternatives_3 )* ) )
            // InternalJConsole.g:559:1: ( ( rule__CLIApp__Alternatives_3 )* )
            {
            // InternalJConsole.g:559:1: ( ( rule__CLIApp__Alternatives_3 )* )
            // InternalJConsole.g:560:2: ( rule__CLIApp__Alternatives_3 )*
            {
             before(grammarAccess.getCLIAppAccess().getAlternatives_3()); 
            // InternalJConsole.g:561:2: ( rule__CLIApp__Alternatives_3 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=19 && LA8_0<=20)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalJConsole.g:561:3: rule__CLIApp__Alternatives_3
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__CLIApp__Alternatives_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getCLIAppAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__3__Impl"


    // $ANTLR start "rule__CLIApp__Group__4"
    // InternalJConsole.g:569:1: rule__CLIApp__Group__4 : rule__CLIApp__Group__4__Impl ;
    public final void rule__CLIApp__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:573:1: ( rule__CLIApp__Group__4__Impl )
            // InternalJConsole.g:574:2: rule__CLIApp__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CLIApp__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__4"


    // $ANTLR start "rule__CLIApp__Group__4__Impl"
    // InternalJConsole.g:580:1: rule__CLIApp__Group__4__Impl : ( ( rule__CLIApp__Group_4__0 ) ) ;
    public final void rule__CLIApp__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:584:1: ( ( ( rule__CLIApp__Group_4__0 ) ) )
            // InternalJConsole.g:585:1: ( ( rule__CLIApp__Group_4__0 ) )
            {
            // InternalJConsole.g:585:1: ( ( rule__CLIApp__Group_4__0 ) )
            // InternalJConsole.g:586:2: ( rule__CLIApp__Group_4__0 )
            {
             before(grammarAccess.getCLIAppAccess().getGroup_4()); 
            // InternalJConsole.g:587:2: ( rule__CLIApp__Group_4__0 )
            // InternalJConsole.g:587:3: rule__CLIApp__Group_4__0
            {
            pushFollow(FOLLOW_2);
            rule__CLIApp__Group_4__0();

            state._fsp--;


            }

             after(grammarAccess.getCLIAppAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group__4__Impl"


    // $ANTLR start "rule__CLIApp__Group_4__0"
    // InternalJConsole.g:596:1: rule__CLIApp__Group_4__0 : rule__CLIApp__Group_4__0__Impl rule__CLIApp__Group_4__1 ;
    public final void rule__CLIApp__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:600:1: ( rule__CLIApp__Group_4__0__Impl rule__CLIApp__Group_4__1 )
            // InternalJConsole.g:601:2: rule__CLIApp__Group_4__0__Impl rule__CLIApp__Group_4__1
            {
            pushFollow(FOLLOW_6);
            rule__CLIApp__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__0"


    // $ANTLR start "rule__CLIApp__Group_4__0__Impl"
    // InternalJConsole.g:608:1: rule__CLIApp__Group_4__0__Impl : ( 'main' ) ;
    public final void rule__CLIApp__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:612:1: ( ( 'main' ) )
            // InternalJConsole.g:613:1: ( 'main' )
            {
            // InternalJConsole.g:613:1: ( 'main' )
            // InternalJConsole.g:614:2: 'main'
            {
             before(grammarAccess.getCLIAppAccess().getMainKeyword_4_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getCLIAppAccess().getMainKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__0__Impl"


    // $ANTLR start "rule__CLIApp__Group_4__1"
    // InternalJConsole.g:623:1: rule__CLIApp__Group_4__1 : rule__CLIApp__Group_4__1__Impl rule__CLIApp__Group_4__2 ;
    public final void rule__CLIApp__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:627:1: ( rule__CLIApp__Group_4__1__Impl rule__CLIApp__Group_4__2 )
            // InternalJConsole.g:628:2: rule__CLIApp__Group_4__1__Impl rule__CLIApp__Group_4__2
            {
            pushFollow(FOLLOW_7);
            rule__CLIApp__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CLIApp__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__1"


    // $ANTLR start "rule__CLIApp__Group_4__1__Impl"
    // InternalJConsole.g:635:1: rule__CLIApp__Group_4__1__Impl : ( 'is' ) ;
    public final void rule__CLIApp__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:639:1: ( ( 'is' ) )
            // InternalJConsole.g:640:1: ( 'is' )
            {
            // InternalJConsole.g:640:1: ( 'is' )
            // InternalJConsole.g:641:2: 'is'
            {
             before(grammarAccess.getCLIAppAccess().getIsKeyword_4_1()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getCLIAppAccess().getIsKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__1__Impl"


    // $ANTLR start "rule__CLIApp__Group_4__2"
    // InternalJConsole.g:650:1: rule__CLIApp__Group_4__2 : rule__CLIApp__Group_4__2__Impl ;
    public final void rule__CLIApp__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:654:1: ( rule__CLIApp__Group_4__2__Impl )
            // InternalJConsole.g:655:2: rule__CLIApp__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CLIApp__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__2"


    // $ANTLR start "rule__CLIApp__Group_4__2__Impl"
    // InternalJConsole.g:661:1: rule__CLIApp__Group_4__2__Impl : ( ( rule__CLIApp__InitialAssignment_4_2 ) ) ;
    public final void rule__CLIApp__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:665:1: ( ( ( rule__CLIApp__InitialAssignment_4_2 ) ) )
            // InternalJConsole.g:666:1: ( ( rule__CLIApp__InitialAssignment_4_2 ) )
            {
            // InternalJConsole.g:666:1: ( ( rule__CLIApp__InitialAssignment_4_2 ) )
            // InternalJConsole.g:667:2: ( rule__CLIApp__InitialAssignment_4_2 )
            {
             before(grammarAccess.getCLIAppAccess().getInitialAssignment_4_2()); 
            // InternalJConsole.g:668:2: ( rule__CLIApp__InitialAssignment_4_2 )
            // InternalJConsole.g:668:3: rule__CLIApp__InitialAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__CLIApp__InitialAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getCLIAppAccess().getInitialAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__Group_4__2__Impl"


    // $ANTLR start "rule__ImportBlock__Group__0"
    // InternalJConsole.g:677:1: rule__ImportBlock__Group__0 : rule__ImportBlock__Group__0__Impl rule__ImportBlock__Group__1 ;
    public final void rule__ImportBlock__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:681:1: ( rule__ImportBlock__Group__0__Impl rule__ImportBlock__Group__1 )
            // InternalJConsole.g:682:2: rule__ImportBlock__Group__0__Impl rule__ImportBlock__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__ImportBlock__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportBlock__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportBlock__Group__0"


    // $ANTLR start "rule__ImportBlock__Group__0__Impl"
    // InternalJConsole.g:689:1: rule__ImportBlock__Group__0__Impl : ( 'imports' ) ;
    public final void rule__ImportBlock__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:693:1: ( ( 'imports' ) )
            // InternalJConsole.g:694:1: ( 'imports' )
            {
            // InternalJConsole.g:694:1: ( 'imports' )
            // InternalJConsole.g:695:2: 'imports'
            {
             before(grammarAccess.getImportBlockAccess().getImportsKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getImportBlockAccess().getImportsKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportBlock__Group__0__Impl"


    // $ANTLR start "rule__ImportBlock__Group__1"
    // InternalJConsole.g:704:1: rule__ImportBlock__Group__1 : rule__ImportBlock__Group__1__Impl ;
    public final void rule__ImportBlock__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:708:1: ( rule__ImportBlock__Group__1__Impl )
            // InternalJConsole.g:709:2: rule__ImportBlock__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportBlock__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportBlock__Group__1"


    // $ANTLR start "rule__ImportBlock__Group__1__Impl"
    // InternalJConsole.g:715:1: rule__ImportBlock__Group__1__Impl : ( ( rule__ImportBlock__LinesAssignment_1 ) ) ;
    public final void rule__ImportBlock__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:719:1: ( ( ( rule__ImportBlock__LinesAssignment_1 ) ) )
            // InternalJConsole.g:720:1: ( ( rule__ImportBlock__LinesAssignment_1 ) )
            {
            // InternalJConsole.g:720:1: ( ( rule__ImportBlock__LinesAssignment_1 ) )
            // InternalJConsole.g:721:2: ( rule__ImportBlock__LinesAssignment_1 )
            {
             before(grammarAccess.getImportBlockAccess().getLinesAssignment_1()); 
            // InternalJConsole.g:722:2: ( rule__ImportBlock__LinesAssignment_1 )
            // InternalJConsole.g:722:3: rule__ImportBlock__LinesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ImportBlock__LinesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportBlockAccess().getLinesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportBlock__Group__1__Impl"


    // $ANTLR start "rule__VariableBlock__Group__0"
    // InternalJConsole.g:731:1: rule__VariableBlock__Group__0 : rule__VariableBlock__Group__0__Impl rule__VariableBlock__Group__1 ;
    public final void rule__VariableBlock__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:735:1: ( rule__VariableBlock__Group__0__Impl rule__VariableBlock__Group__1 )
            // InternalJConsole.g:736:2: rule__VariableBlock__Group__0__Impl rule__VariableBlock__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__VariableBlock__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VariableBlock__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBlock__Group__0"


    // $ANTLR start "rule__VariableBlock__Group__0__Impl"
    // InternalJConsole.g:743:1: rule__VariableBlock__Group__0__Impl : ( 'variables' ) ;
    public final void rule__VariableBlock__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:747:1: ( ( 'variables' ) )
            // InternalJConsole.g:748:1: ( 'variables' )
            {
            // InternalJConsole.g:748:1: ( 'variables' )
            // InternalJConsole.g:749:2: 'variables'
            {
             before(grammarAccess.getVariableBlockAccess().getVariablesKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getVariableBlockAccess().getVariablesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBlock__Group__0__Impl"


    // $ANTLR start "rule__VariableBlock__Group__1"
    // InternalJConsole.g:758:1: rule__VariableBlock__Group__1 : rule__VariableBlock__Group__1__Impl ;
    public final void rule__VariableBlock__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:762:1: ( rule__VariableBlock__Group__1__Impl )
            // InternalJConsole.g:763:2: rule__VariableBlock__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VariableBlock__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBlock__Group__1"


    // $ANTLR start "rule__VariableBlock__Group__1__Impl"
    // InternalJConsole.g:769:1: rule__VariableBlock__Group__1__Impl : ( ( rule__VariableBlock__LinesAssignment_1 ) ) ;
    public final void rule__VariableBlock__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:773:1: ( ( ( rule__VariableBlock__LinesAssignment_1 ) ) )
            // InternalJConsole.g:774:1: ( ( rule__VariableBlock__LinesAssignment_1 ) )
            {
            // InternalJConsole.g:774:1: ( ( rule__VariableBlock__LinesAssignment_1 ) )
            // InternalJConsole.g:775:2: ( rule__VariableBlock__LinesAssignment_1 )
            {
             before(grammarAccess.getVariableBlockAccess().getLinesAssignment_1()); 
            // InternalJConsole.g:776:2: ( rule__VariableBlock__LinesAssignment_1 )
            // InternalJConsole.g:776:3: rule__VariableBlock__LinesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__VariableBlock__LinesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVariableBlockAccess().getLinesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBlock__Group__1__Impl"


    // $ANTLR start "rule__Command__Group__0"
    // InternalJConsole.g:785:1: rule__Command__Group__0 : rule__Command__Group__0__Impl rule__Command__Group__1 ;
    public final void rule__Command__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:789:1: ( rule__Command__Group__0__Impl rule__Command__Group__1 )
            // InternalJConsole.g:790:2: rule__Command__Group__0__Impl rule__Command__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Command__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Command__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__0"


    // $ANTLR start "rule__Command__Group__0__Impl"
    // InternalJConsole.g:797:1: rule__Command__Group__0__Impl : ( 'command' ) ;
    public final void rule__Command__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:801:1: ( ( 'command' ) )
            // InternalJConsole.g:802:1: ( 'command' )
            {
            // InternalJConsole.g:802:1: ( 'command' )
            // InternalJConsole.g:803:2: 'command'
            {
             before(grammarAccess.getCommandAccess().getCommandKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCommandAccess().getCommandKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__0__Impl"


    // $ANTLR start "rule__Command__Group__1"
    // InternalJConsole.g:812:1: rule__Command__Group__1 : rule__Command__Group__1__Impl rule__Command__Group__2 ;
    public final void rule__Command__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:816:1: ( rule__Command__Group__1__Impl rule__Command__Group__2 )
            // InternalJConsole.g:817:2: rule__Command__Group__1__Impl rule__Command__Group__2
            {
            pushFollow(FOLLOW_9);
            rule__Command__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Command__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__1"


    // $ANTLR start "rule__Command__Group__1__Impl"
    // InternalJConsole.g:824:1: rule__Command__Group__1__Impl : ( ( rule__Command__NameAssignment_1 ) ) ;
    public final void rule__Command__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:828:1: ( ( ( rule__Command__NameAssignment_1 ) ) )
            // InternalJConsole.g:829:1: ( ( rule__Command__NameAssignment_1 ) )
            {
            // InternalJConsole.g:829:1: ( ( rule__Command__NameAssignment_1 ) )
            // InternalJConsole.g:830:2: ( rule__Command__NameAssignment_1 )
            {
             before(grammarAccess.getCommandAccess().getNameAssignment_1()); 
            // InternalJConsole.g:831:2: ( rule__Command__NameAssignment_1 )
            // InternalJConsole.g:831:3: rule__Command__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Command__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCommandAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__1__Impl"


    // $ANTLR start "rule__Command__Group__2"
    // InternalJConsole.g:839:1: rule__Command__Group__2 : rule__Command__Group__2__Impl rule__Command__Group__3 ;
    public final void rule__Command__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:843:1: ( rule__Command__Group__2__Impl rule__Command__Group__3 )
            // InternalJConsole.g:844:2: rule__Command__Group__2__Impl rule__Command__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Command__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Command__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__2"


    // $ANTLR start "rule__Command__Group__2__Impl"
    // InternalJConsole.g:851:1: rule__Command__Group__2__Impl : ( '=' ) ;
    public final void rule__Command__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:855:1: ( ( '=' ) )
            // InternalJConsole.g:856:1: ( '=' )
            {
            // InternalJConsole.g:856:1: ( '=' )
            // InternalJConsole.g:857:2: '='
            {
             before(grammarAccess.getCommandAccess().getEqualsSignKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCommandAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__2__Impl"


    // $ANTLR start "rule__Command__Group__3"
    // InternalJConsole.g:866:1: rule__Command__Group__3 : rule__Command__Group__3__Impl ;
    public final void rule__Command__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:870:1: ( rule__Command__Group__3__Impl )
            // InternalJConsole.g:871:2: rule__Command__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Command__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__3"


    // $ANTLR start "rule__Command__Group__3__Impl"
    // InternalJConsole.g:877:1: rule__Command__Group__3__Impl : ( ( rule__Command__CommandAssignment_3 ) ) ;
    public final void rule__Command__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:881:1: ( ( ( rule__Command__CommandAssignment_3 ) ) )
            // InternalJConsole.g:882:1: ( ( rule__Command__CommandAssignment_3 ) )
            {
            // InternalJConsole.g:882:1: ( ( rule__Command__CommandAssignment_3 ) )
            // InternalJConsole.g:883:2: ( rule__Command__CommandAssignment_3 )
            {
             before(grammarAccess.getCommandAccess().getCommandAssignment_3()); 
            // InternalJConsole.g:884:2: ( rule__Command__CommandAssignment_3 )
            // InternalJConsole.g:884:3: rule__Command__CommandAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Command__CommandAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCommandAccess().getCommandAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__Group__3__Impl"


    // $ANTLR start "rule__Guard__Group__0"
    // InternalJConsole.g:893:1: rule__Guard__Group__0 : rule__Guard__Group__0__Impl rule__Guard__Group__1 ;
    public final void rule__Guard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:897:1: ( rule__Guard__Group__0__Impl rule__Guard__Group__1 )
            // InternalJConsole.g:898:2: rule__Guard__Group__0__Impl rule__Guard__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Guard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__0"


    // $ANTLR start "rule__Guard__Group__0__Impl"
    // InternalJConsole.g:905:1: rule__Guard__Group__0__Impl : ( 'guard' ) ;
    public final void rule__Guard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:909:1: ( ( 'guard' ) )
            // InternalJConsole.g:910:1: ( 'guard' )
            {
            // InternalJConsole.g:910:1: ( 'guard' )
            // InternalJConsole.g:911:2: 'guard'
            {
             before(grammarAccess.getGuardAccess().getGuardKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getGuardAccess().getGuardKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__0__Impl"


    // $ANTLR start "rule__Guard__Group__1"
    // InternalJConsole.g:920:1: rule__Guard__Group__1 : rule__Guard__Group__1__Impl rule__Guard__Group__2 ;
    public final void rule__Guard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:924:1: ( rule__Guard__Group__1__Impl rule__Guard__Group__2 )
            // InternalJConsole.g:925:2: rule__Guard__Group__1__Impl rule__Guard__Group__2
            {
            pushFollow(FOLLOW_9);
            rule__Guard__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guard__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__1"


    // $ANTLR start "rule__Guard__Group__1__Impl"
    // InternalJConsole.g:932:1: rule__Guard__Group__1__Impl : ( ( rule__Guard__NameAssignment_1 ) ) ;
    public final void rule__Guard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:936:1: ( ( ( rule__Guard__NameAssignment_1 ) ) )
            // InternalJConsole.g:937:1: ( ( rule__Guard__NameAssignment_1 ) )
            {
            // InternalJConsole.g:937:1: ( ( rule__Guard__NameAssignment_1 ) )
            // InternalJConsole.g:938:2: ( rule__Guard__NameAssignment_1 )
            {
             before(grammarAccess.getGuardAccess().getNameAssignment_1()); 
            // InternalJConsole.g:939:2: ( rule__Guard__NameAssignment_1 )
            // InternalJConsole.g:939:3: rule__Guard__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Guard__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGuardAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__1__Impl"


    // $ANTLR start "rule__Guard__Group__2"
    // InternalJConsole.g:947:1: rule__Guard__Group__2 : rule__Guard__Group__2__Impl rule__Guard__Group__3 ;
    public final void rule__Guard__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:951:1: ( rule__Guard__Group__2__Impl rule__Guard__Group__3 )
            // InternalJConsole.g:952:2: rule__Guard__Group__2__Impl rule__Guard__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Guard__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Guard__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__2"


    // $ANTLR start "rule__Guard__Group__2__Impl"
    // InternalJConsole.g:959:1: rule__Guard__Group__2__Impl : ( '=' ) ;
    public final void rule__Guard__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:963:1: ( ( '=' ) )
            // InternalJConsole.g:964:1: ( '=' )
            {
            // InternalJConsole.g:964:1: ( '=' )
            // InternalJConsole.g:965:2: '='
            {
             before(grammarAccess.getGuardAccess().getEqualsSignKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getGuardAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__2__Impl"


    // $ANTLR start "rule__Guard__Group__3"
    // InternalJConsole.g:974:1: rule__Guard__Group__3 : rule__Guard__Group__3__Impl ;
    public final void rule__Guard__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:978:1: ( rule__Guard__Group__3__Impl )
            // InternalJConsole.g:979:2: rule__Guard__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Guard__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__3"


    // $ANTLR start "rule__Guard__Group__3__Impl"
    // InternalJConsole.g:985:1: rule__Guard__Group__3__Impl : ( ( rule__Guard__GuardAssignment_3 ) ) ;
    public final void rule__Guard__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:989:1: ( ( ( rule__Guard__GuardAssignment_3 ) ) )
            // InternalJConsole.g:990:1: ( ( rule__Guard__GuardAssignment_3 ) )
            {
            // InternalJConsole.g:990:1: ( ( rule__Guard__GuardAssignment_3 ) )
            // InternalJConsole.g:991:2: ( rule__Guard__GuardAssignment_3 )
            {
             before(grammarAccess.getGuardAccess().getGuardAssignment_3()); 
            // InternalJConsole.g:992:2: ( rule__Guard__GuardAssignment_3 )
            // InternalJConsole.g:992:3: rule__Guard__GuardAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Guard__GuardAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getGuardAccess().getGuardAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__Group__3__Impl"


    // $ANTLR start "rule__Procedure__Group__0"
    // InternalJConsole.g:1001:1: rule__Procedure__Group__0 : rule__Procedure__Group__0__Impl rule__Procedure__Group__1 ;
    public final void rule__Procedure__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1005:1: ( rule__Procedure__Group__0__Impl rule__Procedure__Group__1 )
            // InternalJConsole.g:1006:2: rule__Procedure__Group__0__Impl rule__Procedure__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Procedure__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Procedure__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__0"


    // $ANTLR start "rule__Procedure__Group__0__Impl"
    // InternalJConsole.g:1013:1: rule__Procedure__Group__0__Impl : ( 'procedure' ) ;
    public final void rule__Procedure__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1017:1: ( ( 'procedure' ) )
            // InternalJConsole.g:1018:1: ( 'procedure' )
            {
            // InternalJConsole.g:1018:1: ( 'procedure' )
            // InternalJConsole.g:1019:2: 'procedure'
            {
             before(grammarAccess.getProcedureAccess().getProcedureKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getProcedureAccess().getProcedureKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__0__Impl"


    // $ANTLR start "rule__Procedure__Group__1"
    // InternalJConsole.g:1028:1: rule__Procedure__Group__1 : rule__Procedure__Group__1__Impl rule__Procedure__Group__2 ;
    public final void rule__Procedure__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1032:1: ( rule__Procedure__Group__1__Impl rule__Procedure__Group__2 )
            // InternalJConsole.g:1033:2: rule__Procedure__Group__1__Impl rule__Procedure__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__Procedure__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Procedure__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__1"


    // $ANTLR start "rule__Procedure__Group__1__Impl"
    // InternalJConsole.g:1040:1: rule__Procedure__Group__1__Impl : ( ( rule__Procedure__NameAssignment_1 ) ) ;
    public final void rule__Procedure__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1044:1: ( ( ( rule__Procedure__NameAssignment_1 ) ) )
            // InternalJConsole.g:1045:1: ( ( rule__Procedure__NameAssignment_1 ) )
            {
            // InternalJConsole.g:1045:1: ( ( rule__Procedure__NameAssignment_1 ) )
            // InternalJConsole.g:1046:2: ( rule__Procedure__NameAssignment_1 )
            {
             before(grammarAccess.getProcedureAccess().getNameAssignment_1()); 
            // InternalJConsole.g:1047:2: ( rule__Procedure__NameAssignment_1 )
            // InternalJConsole.g:1047:3: rule__Procedure__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Procedure__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getProcedureAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__1__Impl"


    // $ANTLR start "rule__Procedure__Group__2"
    // InternalJConsole.g:1055:1: rule__Procedure__Group__2 : rule__Procedure__Group__2__Impl ;
    public final void rule__Procedure__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1059:1: ( rule__Procedure__Group__2__Impl )
            // InternalJConsole.g:1060:2: rule__Procedure__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Procedure__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__2"


    // $ANTLR start "rule__Procedure__Group__2__Impl"
    // InternalJConsole.g:1066:1: rule__Procedure__Group__2__Impl : ( ( rule__Procedure__LinesAssignment_2 ) ) ;
    public final void rule__Procedure__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1070:1: ( ( ( rule__Procedure__LinesAssignment_2 ) ) )
            // InternalJConsole.g:1071:1: ( ( rule__Procedure__LinesAssignment_2 ) )
            {
            // InternalJConsole.g:1071:1: ( ( rule__Procedure__LinesAssignment_2 ) )
            // InternalJConsole.g:1072:2: ( rule__Procedure__LinesAssignment_2 )
            {
             before(grammarAccess.getProcedureAccess().getLinesAssignment_2()); 
            // InternalJConsole.g:1073:2: ( rule__Procedure__LinesAssignment_2 )
            // InternalJConsole.g:1073:3: rule__Procedure__LinesAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Procedure__LinesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getProcedureAccess().getLinesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__Group__2__Impl"


    // $ANTLR start "rule__Screen__Group__0"
    // InternalJConsole.g:1082:1: rule__Screen__Group__0 : rule__Screen__Group__0__Impl rule__Screen__Group__1 ;
    public final void rule__Screen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1086:1: ( rule__Screen__Group__0__Impl rule__Screen__Group__1 )
            // InternalJConsole.g:1087:2: rule__Screen__Group__0__Impl rule__Screen__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Screen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Screen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__0"


    // $ANTLR start "rule__Screen__Group__0__Impl"
    // InternalJConsole.g:1094:1: rule__Screen__Group__0__Impl : ( 'screen' ) ;
    public final void rule__Screen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1098:1: ( ( 'screen' ) )
            // InternalJConsole.g:1099:1: ( 'screen' )
            {
            // InternalJConsole.g:1099:1: ( 'screen' )
            // InternalJConsole.g:1100:2: 'screen'
            {
             before(grammarAccess.getScreenAccess().getScreenKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getScreenAccess().getScreenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__0__Impl"


    // $ANTLR start "rule__Screen__Group__1"
    // InternalJConsole.g:1109:1: rule__Screen__Group__1 : rule__Screen__Group__1__Impl rule__Screen__Group__2 ;
    public final void rule__Screen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1113:1: ( rule__Screen__Group__1__Impl rule__Screen__Group__2 )
            // InternalJConsole.g:1114:2: rule__Screen__Group__1__Impl rule__Screen__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Screen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Screen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__1"


    // $ANTLR start "rule__Screen__Group__1__Impl"
    // InternalJConsole.g:1121:1: rule__Screen__Group__1__Impl : ( ( rule__Screen__NameAssignment_1 ) ) ;
    public final void rule__Screen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1125:1: ( ( ( rule__Screen__NameAssignment_1 ) ) )
            // InternalJConsole.g:1126:1: ( ( rule__Screen__NameAssignment_1 ) )
            {
            // InternalJConsole.g:1126:1: ( ( rule__Screen__NameAssignment_1 ) )
            // InternalJConsole.g:1127:2: ( rule__Screen__NameAssignment_1 )
            {
             before(grammarAccess.getScreenAccess().getNameAssignment_1()); 
            // InternalJConsole.g:1128:2: ( rule__Screen__NameAssignment_1 )
            // InternalJConsole.g:1128:3: rule__Screen__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Screen__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getScreenAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__1__Impl"


    // $ANTLR start "rule__Screen__Group__2"
    // InternalJConsole.g:1136:1: rule__Screen__Group__2 : rule__Screen__Group__2__Impl rule__Screen__Group__3 ;
    public final void rule__Screen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1140:1: ( rule__Screen__Group__2__Impl rule__Screen__Group__3 )
            // InternalJConsole.g:1141:2: rule__Screen__Group__2__Impl rule__Screen__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__Screen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Screen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__2"


    // $ANTLR start "rule__Screen__Group__2__Impl"
    // InternalJConsole.g:1148:1: rule__Screen__Group__2__Impl : ( '{' ) ;
    public final void rule__Screen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1152:1: ( ( '{' ) )
            // InternalJConsole.g:1153:1: ( '{' )
            {
            // InternalJConsole.g:1153:1: ( '{' )
            // InternalJConsole.g:1154:2: '{'
            {
             before(grammarAccess.getScreenAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getScreenAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__2__Impl"


    // $ANTLR start "rule__Screen__Group__3"
    // InternalJConsole.g:1163:1: rule__Screen__Group__3 : rule__Screen__Group__3__Impl rule__Screen__Group__4 ;
    public final void rule__Screen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1167:1: ( rule__Screen__Group__3__Impl rule__Screen__Group__4 )
            // InternalJConsole.g:1168:2: rule__Screen__Group__3__Impl rule__Screen__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Screen__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Screen__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__3"


    // $ANTLR start "rule__Screen__Group__3__Impl"
    // InternalJConsole.g:1175:1: rule__Screen__Group__3__Impl : ( ( rule__Screen__InternalProcedureAssignment_3 ) ) ;
    public final void rule__Screen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1179:1: ( ( ( rule__Screen__InternalProcedureAssignment_3 ) ) )
            // InternalJConsole.g:1180:1: ( ( rule__Screen__InternalProcedureAssignment_3 ) )
            {
            // InternalJConsole.g:1180:1: ( ( rule__Screen__InternalProcedureAssignment_3 ) )
            // InternalJConsole.g:1181:2: ( rule__Screen__InternalProcedureAssignment_3 )
            {
             before(grammarAccess.getScreenAccess().getInternalProcedureAssignment_3()); 
            // InternalJConsole.g:1182:2: ( rule__Screen__InternalProcedureAssignment_3 )
            // InternalJConsole.g:1182:3: rule__Screen__InternalProcedureAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Screen__InternalProcedureAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getScreenAccess().getInternalProcedureAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__3__Impl"


    // $ANTLR start "rule__Screen__Group__4"
    // InternalJConsole.g:1190:1: rule__Screen__Group__4 : rule__Screen__Group__4__Impl rule__Screen__Group__5 ;
    public final void rule__Screen__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1194:1: ( rule__Screen__Group__4__Impl rule__Screen__Group__5 )
            // InternalJConsole.g:1195:2: rule__Screen__Group__4__Impl rule__Screen__Group__5
            {
            pushFollow(FOLLOW_13);
            rule__Screen__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Screen__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__4"


    // $ANTLR start "rule__Screen__Group__4__Impl"
    // InternalJConsole.g:1202:1: rule__Screen__Group__4__Impl : ( ( rule__Screen__TransitionSetsAssignment_4 )* ) ;
    public final void rule__Screen__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1206:1: ( ( ( rule__Screen__TransitionSetsAssignment_4 )* ) )
            // InternalJConsole.g:1207:1: ( ( rule__Screen__TransitionSetsAssignment_4 )* )
            {
            // InternalJConsole.g:1207:1: ( ( rule__Screen__TransitionSetsAssignment_4 )* )
            // InternalJConsole.g:1208:2: ( rule__Screen__TransitionSetsAssignment_4 )*
            {
             before(grammarAccess.getScreenAccess().getTransitionSetsAssignment_4()); 
            // InternalJConsole.g:1209:2: ( rule__Screen__TransitionSetsAssignment_4 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==24) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalJConsole.g:1209:3: rule__Screen__TransitionSetsAssignment_4
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Screen__TransitionSetsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getScreenAccess().getTransitionSetsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__4__Impl"


    // $ANTLR start "rule__Screen__Group__5"
    // InternalJConsole.g:1217:1: rule__Screen__Group__5 : rule__Screen__Group__5__Impl ;
    public final void rule__Screen__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1221:1: ( rule__Screen__Group__5__Impl )
            // InternalJConsole.g:1222:2: rule__Screen__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Screen__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__5"


    // $ANTLR start "rule__Screen__Group__5__Impl"
    // InternalJConsole.g:1228:1: rule__Screen__Group__5__Impl : ( '}' ) ;
    public final void rule__Screen__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1232:1: ( ( '}' ) )
            // InternalJConsole.g:1233:1: ( '}' )
            {
            // InternalJConsole.g:1233:1: ( '}' )
            // InternalJConsole.g:1234:2: '}'
            {
             before(grammarAccess.getScreenAccess().getRightCurlyBracketKeyword_5()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getScreenAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__Group__5__Impl"


    // $ANTLR start "rule__DoBlock__Group__0"
    // InternalJConsole.g:1244:1: rule__DoBlock__Group__0 : rule__DoBlock__Group__0__Impl rule__DoBlock__Group__1 ;
    public final void rule__DoBlock__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1248:1: ( rule__DoBlock__Group__0__Impl rule__DoBlock__Group__1 )
            // InternalJConsole.g:1249:2: rule__DoBlock__Group__0__Impl rule__DoBlock__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__DoBlock__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DoBlock__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DoBlock__Group__0"


    // $ANTLR start "rule__DoBlock__Group__0__Impl"
    // InternalJConsole.g:1256:1: rule__DoBlock__Group__0__Impl : ( 'do' ) ;
    public final void rule__DoBlock__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1260:1: ( ( 'do' ) )
            // InternalJConsole.g:1261:1: ( 'do' )
            {
            // InternalJConsole.g:1261:1: ( 'do' )
            // InternalJConsole.g:1262:2: 'do'
            {
             before(grammarAccess.getDoBlockAccess().getDoKeyword_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getDoBlockAccess().getDoKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DoBlock__Group__0__Impl"


    // $ANTLR start "rule__DoBlock__Group__1"
    // InternalJConsole.g:1271:1: rule__DoBlock__Group__1 : rule__DoBlock__Group__1__Impl ;
    public final void rule__DoBlock__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1275:1: ( rule__DoBlock__Group__1__Impl )
            // InternalJConsole.g:1276:2: rule__DoBlock__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DoBlock__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DoBlock__Group__1"


    // $ANTLR start "rule__DoBlock__Group__1__Impl"
    // InternalJConsole.g:1282:1: rule__DoBlock__Group__1__Impl : ( ( rule__DoBlock__LinesAssignment_1 ) ) ;
    public final void rule__DoBlock__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1286:1: ( ( ( rule__DoBlock__LinesAssignment_1 ) ) )
            // InternalJConsole.g:1287:1: ( ( rule__DoBlock__LinesAssignment_1 ) )
            {
            // InternalJConsole.g:1287:1: ( ( rule__DoBlock__LinesAssignment_1 ) )
            // InternalJConsole.g:1288:2: ( rule__DoBlock__LinesAssignment_1 )
            {
             before(grammarAccess.getDoBlockAccess().getLinesAssignment_1()); 
            // InternalJConsole.g:1289:2: ( rule__DoBlock__LinesAssignment_1 )
            // InternalJConsole.g:1289:3: rule__DoBlock__LinesAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DoBlock__LinesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDoBlockAccess().getLinesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DoBlock__Group__1__Impl"


    // $ANTLR start "rule__ProcedureWhen__Group__0"
    // InternalJConsole.g:1298:1: rule__ProcedureWhen__Group__0 : rule__ProcedureWhen__Group__0__Impl rule__ProcedureWhen__Group__1 ;
    public final void rule__ProcedureWhen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1302:1: ( rule__ProcedureWhen__Group__0__Impl rule__ProcedureWhen__Group__1 )
            // InternalJConsole.g:1303:2: rule__ProcedureWhen__Group__0__Impl rule__ProcedureWhen__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__ProcedureWhen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__0"


    // $ANTLR start "rule__ProcedureWhen__Group__0__Impl"
    // InternalJConsole.g:1310:1: rule__ProcedureWhen__Group__0__Impl : ( 'when' ) ;
    public final void rule__ProcedureWhen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1314:1: ( ( 'when' ) )
            // InternalJConsole.g:1315:1: ( 'when' )
            {
            // InternalJConsole.g:1315:1: ( 'when' )
            // InternalJConsole.g:1316:2: 'when'
            {
             before(grammarAccess.getProcedureWhenAccess().getWhenKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getProcedureWhenAccess().getWhenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__0__Impl"


    // $ANTLR start "rule__ProcedureWhen__Group__1"
    // InternalJConsole.g:1325:1: rule__ProcedureWhen__Group__1 : rule__ProcedureWhen__Group__1__Impl rule__ProcedureWhen__Group__2 ;
    public final void rule__ProcedureWhen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1329:1: ( rule__ProcedureWhen__Group__1__Impl rule__ProcedureWhen__Group__2 )
            // InternalJConsole.g:1330:2: rule__ProcedureWhen__Group__1__Impl rule__ProcedureWhen__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__ProcedureWhen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__1"


    // $ANTLR start "rule__ProcedureWhen__Group__1__Impl"
    // InternalJConsole.g:1337:1: rule__ProcedureWhen__Group__1__Impl : ( ( rule__ProcedureWhen__CommandAssignment_1 ) ) ;
    public final void rule__ProcedureWhen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1341:1: ( ( ( rule__ProcedureWhen__CommandAssignment_1 ) ) )
            // InternalJConsole.g:1342:1: ( ( rule__ProcedureWhen__CommandAssignment_1 ) )
            {
            // InternalJConsole.g:1342:1: ( ( rule__ProcedureWhen__CommandAssignment_1 ) )
            // InternalJConsole.g:1343:2: ( rule__ProcedureWhen__CommandAssignment_1 )
            {
             before(grammarAccess.getProcedureWhenAccess().getCommandAssignment_1()); 
            // InternalJConsole.g:1344:2: ( rule__ProcedureWhen__CommandAssignment_1 )
            // InternalJConsole.g:1344:3: rule__ProcedureWhen__CommandAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__CommandAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getProcedureWhenAccess().getCommandAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__1__Impl"


    // $ANTLR start "rule__ProcedureWhen__Group__2"
    // InternalJConsole.g:1352:1: rule__ProcedureWhen__Group__2 : rule__ProcedureWhen__Group__2__Impl rule__ProcedureWhen__Group__3 ;
    public final void rule__ProcedureWhen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1356:1: ( rule__ProcedureWhen__Group__2__Impl rule__ProcedureWhen__Group__3 )
            // InternalJConsole.g:1357:2: rule__ProcedureWhen__Group__2__Impl rule__ProcedureWhen__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__ProcedureWhen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__2"


    // $ANTLR start "rule__ProcedureWhen__Group__2__Impl"
    // InternalJConsole.g:1364:1: rule__ProcedureWhen__Group__2__Impl : ( 'do' ) ;
    public final void rule__ProcedureWhen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1368:1: ( ( 'do' ) )
            // InternalJConsole.g:1369:1: ( 'do' )
            {
            // InternalJConsole.g:1369:1: ( 'do' )
            // InternalJConsole.g:1370:2: 'do'
            {
             before(grammarAccess.getProcedureWhenAccess().getDoKeyword_2()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getProcedureWhenAccess().getDoKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__2__Impl"


    // $ANTLR start "rule__ProcedureWhen__Group__3"
    // InternalJConsole.g:1379:1: rule__ProcedureWhen__Group__3 : rule__ProcedureWhen__Group__3__Impl ;
    public final void rule__ProcedureWhen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1383:1: ( rule__ProcedureWhen__Group__3__Impl )
            // InternalJConsole.g:1384:2: rule__ProcedureWhen__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__3"


    // $ANTLR start "rule__ProcedureWhen__Group__3__Impl"
    // InternalJConsole.g:1390:1: rule__ProcedureWhen__Group__3__Impl : ( ( rule__ProcedureWhen__ProcedureAssignment_3 ) ) ;
    public final void rule__ProcedureWhen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1394:1: ( ( ( rule__ProcedureWhen__ProcedureAssignment_3 ) ) )
            // InternalJConsole.g:1395:1: ( ( rule__ProcedureWhen__ProcedureAssignment_3 ) )
            {
            // InternalJConsole.g:1395:1: ( ( rule__ProcedureWhen__ProcedureAssignment_3 ) )
            // InternalJConsole.g:1396:2: ( rule__ProcedureWhen__ProcedureAssignment_3 )
            {
             before(grammarAccess.getProcedureWhenAccess().getProcedureAssignment_3()); 
            // InternalJConsole.g:1397:2: ( rule__ProcedureWhen__ProcedureAssignment_3 )
            // InternalJConsole.g:1397:3: rule__ProcedureWhen__ProcedureAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ProcedureWhen__ProcedureAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getProcedureWhenAccess().getProcedureAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__Group__3__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group__0"
    // InternalJConsole.g:1406:1: rule__ScreenWhenBlock__Group__0 : rule__ScreenWhenBlock__Group__0__Impl rule__ScreenWhenBlock__Group__1 ;
    public final void rule__ScreenWhenBlock__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1410:1: ( rule__ScreenWhenBlock__Group__0__Impl rule__ScreenWhenBlock__Group__1 )
            // InternalJConsole.g:1411:2: rule__ScreenWhenBlock__Group__0__Impl rule__ScreenWhenBlock__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__ScreenWhenBlock__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__0"


    // $ANTLR start "rule__ScreenWhenBlock__Group__0__Impl"
    // InternalJConsole.g:1418:1: rule__ScreenWhenBlock__Group__0__Impl : ( 'when' ) ;
    public final void rule__ScreenWhenBlock__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1422:1: ( ( 'when' ) )
            // InternalJConsole.g:1423:1: ( 'when' )
            {
            // InternalJConsole.g:1423:1: ( 'when' )
            // InternalJConsole.g:1424:2: 'when'
            {
             before(grammarAccess.getScreenWhenBlockAccess().getWhenKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getScreenWhenBlockAccess().getWhenKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__0__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group__1"
    // InternalJConsole.g:1433:1: rule__ScreenWhenBlock__Group__1 : rule__ScreenWhenBlock__Group__1__Impl rule__ScreenWhenBlock__Group__2 ;
    public final void rule__ScreenWhenBlock__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1437:1: ( rule__ScreenWhenBlock__Group__1__Impl rule__ScreenWhenBlock__Group__2 )
            // InternalJConsole.g:1438:2: rule__ScreenWhenBlock__Group__1__Impl rule__ScreenWhenBlock__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__ScreenWhenBlock__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__1"


    // $ANTLR start "rule__ScreenWhenBlock__Group__1__Impl"
    // InternalJConsole.g:1445:1: rule__ScreenWhenBlock__Group__1__Impl : ( ( rule__ScreenWhenBlock__CommandAssignment_1 ) ) ;
    public final void rule__ScreenWhenBlock__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1449:1: ( ( ( rule__ScreenWhenBlock__CommandAssignment_1 ) ) )
            // InternalJConsole.g:1450:1: ( ( rule__ScreenWhenBlock__CommandAssignment_1 ) )
            {
            // InternalJConsole.g:1450:1: ( ( rule__ScreenWhenBlock__CommandAssignment_1 ) )
            // InternalJConsole.g:1451:2: ( rule__ScreenWhenBlock__CommandAssignment_1 )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getCommandAssignment_1()); 
            // InternalJConsole.g:1452:2: ( rule__ScreenWhenBlock__CommandAssignment_1 )
            // InternalJConsole.g:1452:3: rule__ScreenWhenBlock__CommandAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__CommandAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getScreenWhenBlockAccess().getCommandAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__1__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group__2"
    // InternalJConsole.g:1460:1: rule__ScreenWhenBlock__Group__2 : rule__ScreenWhenBlock__Group__2__Impl rule__ScreenWhenBlock__Group__3 ;
    public final void rule__ScreenWhenBlock__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1464:1: ( rule__ScreenWhenBlock__Group__2__Impl rule__ScreenWhenBlock__Group__3 )
            // InternalJConsole.g:1465:2: rule__ScreenWhenBlock__Group__2__Impl rule__ScreenWhenBlock__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__ScreenWhenBlock__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__2"


    // $ANTLR start "rule__ScreenWhenBlock__Group__2__Impl"
    // InternalJConsole.g:1472:1: rule__ScreenWhenBlock__Group__2__Impl : ( '{' ) ;
    public final void rule__ScreenWhenBlock__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1476:1: ( ( '{' ) )
            // InternalJConsole.g:1477:1: ( '{' )
            {
            // InternalJConsole.g:1477:1: ( '{' )
            // InternalJConsole.g:1478:2: '{'
            {
             before(grammarAccess.getScreenWhenBlockAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getScreenWhenBlockAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__2__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group__3"
    // InternalJConsole.g:1487:1: rule__ScreenWhenBlock__Group__3 : rule__ScreenWhenBlock__Group__3__Impl rule__ScreenWhenBlock__Group__4 ;
    public final void rule__ScreenWhenBlock__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1491:1: ( rule__ScreenWhenBlock__Group__3__Impl rule__ScreenWhenBlock__Group__4 )
            // InternalJConsole.g:1492:2: rule__ScreenWhenBlock__Group__3__Impl rule__ScreenWhenBlock__Group__4
            {
            pushFollow(FOLLOW_16);
            rule__ScreenWhenBlock__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__3"


    // $ANTLR start "rule__ScreenWhenBlock__Group__3__Impl"
    // InternalJConsole.g:1499:1: rule__ScreenWhenBlock__Group__3__Impl : ( ( rule__ScreenWhenBlock__Alternatives_3 ) ) ;
    public final void rule__ScreenWhenBlock__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1503:1: ( ( ( rule__ScreenWhenBlock__Alternatives_3 ) ) )
            // InternalJConsole.g:1504:1: ( ( rule__ScreenWhenBlock__Alternatives_3 ) )
            {
            // InternalJConsole.g:1504:1: ( ( rule__ScreenWhenBlock__Alternatives_3 ) )
            // InternalJConsole.g:1505:2: ( rule__ScreenWhenBlock__Alternatives_3 )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getAlternatives_3()); 
            // InternalJConsole.g:1506:2: ( rule__ScreenWhenBlock__Alternatives_3 )
            // InternalJConsole.g:1506:3: rule__ScreenWhenBlock__Alternatives_3
            {
            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Alternatives_3();

            state._fsp--;


            }

             after(grammarAccess.getScreenWhenBlockAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__3__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group__4"
    // InternalJConsole.g:1514:1: rule__ScreenWhenBlock__Group__4 : rule__ScreenWhenBlock__Group__4__Impl ;
    public final void rule__ScreenWhenBlock__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1518:1: ( rule__ScreenWhenBlock__Group__4__Impl )
            // InternalJConsole.g:1519:2: rule__ScreenWhenBlock__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__4"


    // $ANTLR start "rule__ScreenWhenBlock__Group__4__Impl"
    // InternalJConsole.g:1525:1: rule__ScreenWhenBlock__Group__4__Impl : ( '}' ) ;
    public final void rule__ScreenWhenBlock__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1529:1: ( ( '}' ) )
            // InternalJConsole.g:1530:1: ( '}' )
            {
            // InternalJConsole.g:1530:1: ( '}' )
            // InternalJConsole.g:1531:2: '}'
            {
             before(grammarAccess.getScreenWhenBlockAccess().getRightCurlyBracketKeyword_4()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getScreenWhenBlockAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group__4__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group_3_1__0"
    // InternalJConsole.g:1541:1: rule__ScreenWhenBlock__Group_3_1__0 : rule__ScreenWhenBlock__Group_3_1__0__Impl rule__ScreenWhenBlock__Group_3_1__1 ;
    public final void rule__ScreenWhenBlock__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1545:1: ( rule__ScreenWhenBlock__Group_3_1__0__Impl rule__ScreenWhenBlock__Group_3_1__1 )
            // InternalJConsole.g:1546:2: rule__ScreenWhenBlock__Group_3_1__0__Impl rule__ScreenWhenBlock__Group_3_1__1
            {
            pushFollow(FOLLOW_17);
            rule__ScreenWhenBlock__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group_3_1__0"


    // $ANTLR start "rule__ScreenWhenBlock__Group_3_1__0__Impl"
    // InternalJConsole.g:1553:1: rule__ScreenWhenBlock__Group_3_1__0__Impl : ( ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) ) ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* ) ) ;
    public final void rule__ScreenWhenBlock__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1557:1: ( ( ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) ) ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* ) ) )
            // InternalJConsole.g:1558:1: ( ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) ) ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* ) )
            {
            // InternalJConsole.g:1558:1: ( ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) ) ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* ) )
            // InternalJConsole.g:1559:2: ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) ) ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* )
            {
            // InternalJConsole.g:1559:2: ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 ) )
            // InternalJConsole.g:1560:3: ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionsAssignment_3_1_0()); 
            // InternalJConsole.g:1561:3: ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )
            // InternalJConsole.g:1561:4: rule__ScreenWhenBlock__TransitionsAssignment_3_1_0
            {
            pushFollow(FOLLOW_18);
            rule__ScreenWhenBlock__TransitionsAssignment_3_1_0();

            state._fsp--;


            }

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionsAssignment_3_1_0()); 

            }

            // InternalJConsole.g:1564:2: ( ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )* )
            // InternalJConsole.g:1565:3: ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )*
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionsAssignment_3_1_0()); 
            // InternalJConsole.g:1566:3: ( rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==25) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalJConsole.g:1566:4: rule__ScreenWhenBlock__TransitionsAssignment_3_1_0
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__ScreenWhenBlock__TransitionsAssignment_3_1_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionsAssignment_3_1_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group_3_1__0__Impl"


    // $ANTLR start "rule__ScreenWhenBlock__Group_3_1__1"
    // InternalJConsole.g:1575:1: rule__ScreenWhenBlock__Group_3_1__1 : rule__ScreenWhenBlock__Group_3_1__1__Impl ;
    public final void rule__ScreenWhenBlock__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1579:1: ( rule__ScreenWhenBlock__Group_3_1__1__Impl )
            // InternalJConsole.g:1580:2: rule__ScreenWhenBlock__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ScreenWhenBlock__Group_3_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group_3_1__1"


    // $ANTLR start "rule__ScreenWhenBlock__Group_3_1__1__Impl"
    // InternalJConsole.g:1586:1: rule__ScreenWhenBlock__Group_3_1__1__Impl : ( ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )? ) ;
    public final void rule__ScreenWhenBlock__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1590:1: ( ( ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )? ) )
            // InternalJConsole.g:1591:1: ( ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )? )
            {
            // InternalJConsole.g:1591:1: ( ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )? )
            // InternalJConsole.g:1592:2: ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )?
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionElseAssignment_3_1_1()); 
            // InternalJConsole.g:1593:2: ( rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==27) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalJConsole.g:1593:3: rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionElseAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__Group_3_1__1__Impl"


    // $ANTLR start "rule__IfThen__Group__0"
    // InternalJConsole.g:1602:1: rule__IfThen__Group__0 : rule__IfThen__Group__0__Impl rule__IfThen__Group__1 ;
    public final void rule__IfThen__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1606:1: ( rule__IfThen__Group__0__Impl rule__IfThen__Group__1 )
            // InternalJConsole.g:1607:2: rule__IfThen__Group__0__Impl rule__IfThen__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__IfThen__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfThen__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__0"


    // $ANTLR start "rule__IfThen__Group__0__Impl"
    // InternalJConsole.g:1614:1: rule__IfThen__Group__0__Impl : ( 'if' ) ;
    public final void rule__IfThen__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1618:1: ( ( 'if' ) )
            // InternalJConsole.g:1619:1: ( 'if' )
            {
            // InternalJConsole.g:1619:1: ( 'if' )
            // InternalJConsole.g:1620:2: 'if'
            {
             before(grammarAccess.getIfThenAccess().getIfKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getIfThenAccess().getIfKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__0__Impl"


    // $ANTLR start "rule__IfThen__Group__1"
    // InternalJConsole.g:1629:1: rule__IfThen__Group__1 : rule__IfThen__Group__1__Impl rule__IfThen__Group__2 ;
    public final void rule__IfThen__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1633:1: ( rule__IfThen__Group__1__Impl rule__IfThen__Group__2 )
            // InternalJConsole.g:1634:2: rule__IfThen__Group__1__Impl rule__IfThen__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__IfThen__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfThen__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__1"


    // $ANTLR start "rule__IfThen__Group__1__Impl"
    // InternalJConsole.g:1641:1: rule__IfThen__Group__1__Impl : ( ( rule__IfThen__ConditionAssignment_1 ) ) ;
    public final void rule__IfThen__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1645:1: ( ( ( rule__IfThen__ConditionAssignment_1 ) ) )
            // InternalJConsole.g:1646:1: ( ( rule__IfThen__ConditionAssignment_1 ) )
            {
            // InternalJConsole.g:1646:1: ( ( rule__IfThen__ConditionAssignment_1 ) )
            // InternalJConsole.g:1647:2: ( rule__IfThen__ConditionAssignment_1 )
            {
             before(grammarAccess.getIfThenAccess().getConditionAssignment_1()); 
            // InternalJConsole.g:1648:2: ( rule__IfThen__ConditionAssignment_1 )
            // InternalJConsole.g:1648:3: rule__IfThen__ConditionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__IfThen__ConditionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIfThenAccess().getConditionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__1__Impl"


    // $ANTLR start "rule__IfThen__Group__2"
    // InternalJConsole.g:1656:1: rule__IfThen__Group__2 : rule__IfThen__Group__2__Impl rule__IfThen__Group__3 ;
    public final void rule__IfThen__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1660:1: ( rule__IfThen__Group__2__Impl rule__IfThen__Group__3 )
            // InternalJConsole.g:1661:2: rule__IfThen__Group__2__Impl rule__IfThen__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__IfThen__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfThen__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__2"


    // $ANTLR start "rule__IfThen__Group__2__Impl"
    // InternalJConsole.g:1668:1: rule__IfThen__Group__2__Impl : ( 'then' ) ;
    public final void rule__IfThen__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1672:1: ( ( 'then' ) )
            // InternalJConsole.g:1673:1: ( 'then' )
            {
            // InternalJConsole.g:1673:1: ( 'then' )
            // InternalJConsole.g:1674:2: 'then'
            {
             before(grammarAccess.getIfThenAccess().getThenKeyword_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getIfThenAccess().getThenKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__2__Impl"


    // $ANTLR start "rule__IfThen__Group__3"
    // InternalJConsole.g:1683:1: rule__IfThen__Group__3 : rule__IfThen__Group__3__Impl ;
    public final void rule__IfThen__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1687:1: ( rule__IfThen__Group__3__Impl )
            // InternalJConsole.g:1688:2: rule__IfThen__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IfThen__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__3"


    // $ANTLR start "rule__IfThen__Group__3__Impl"
    // InternalJConsole.g:1694:1: rule__IfThen__Group__3__Impl : ( ( rule__IfThen__ScreenAssignment_3 ) ) ;
    public final void rule__IfThen__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1698:1: ( ( ( rule__IfThen__ScreenAssignment_3 ) ) )
            // InternalJConsole.g:1699:1: ( ( rule__IfThen__ScreenAssignment_3 ) )
            {
            // InternalJConsole.g:1699:1: ( ( rule__IfThen__ScreenAssignment_3 ) )
            // InternalJConsole.g:1700:2: ( rule__IfThen__ScreenAssignment_3 )
            {
             before(grammarAccess.getIfThenAccess().getScreenAssignment_3()); 
            // InternalJConsole.g:1701:2: ( rule__IfThen__ScreenAssignment_3 )
            // InternalJConsole.g:1701:3: rule__IfThen__ScreenAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__IfThen__ScreenAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getIfThenAccess().getScreenAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__Group__3__Impl"


    // $ANTLR start "rule__Else__Group__0"
    // InternalJConsole.g:1710:1: rule__Else__Group__0 : rule__Else__Group__0__Impl rule__Else__Group__1 ;
    public final void rule__Else__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1714:1: ( rule__Else__Group__0__Impl rule__Else__Group__1 )
            // InternalJConsole.g:1715:2: rule__Else__Group__0__Impl rule__Else__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Else__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Else__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__0"


    // $ANTLR start "rule__Else__Group__0__Impl"
    // InternalJConsole.g:1722:1: rule__Else__Group__0__Impl : ( 'else' ) ;
    public final void rule__Else__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1726:1: ( ( 'else' ) )
            // InternalJConsole.g:1727:1: ( 'else' )
            {
            // InternalJConsole.g:1727:1: ( 'else' )
            // InternalJConsole.g:1728:2: 'else'
            {
             before(grammarAccess.getElseAccess().getElseKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getElseAccess().getElseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__0__Impl"


    // $ANTLR start "rule__Else__Group__1"
    // InternalJConsole.g:1737:1: rule__Else__Group__1 : rule__Else__Group__1__Impl ;
    public final void rule__Else__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1741:1: ( rule__Else__Group__1__Impl )
            // InternalJConsole.g:1742:2: rule__Else__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Else__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__1"


    // $ANTLR start "rule__Else__Group__1__Impl"
    // InternalJConsole.g:1748:1: rule__Else__Group__1__Impl : ( ( rule__Else__ScreenAssignment_1 ) ) ;
    public final void rule__Else__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1752:1: ( ( ( rule__Else__ScreenAssignment_1 ) ) )
            // InternalJConsole.g:1753:1: ( ( rule__Else__ScreenAssignment_1 ) )
            {
            // InternalJConsole.g:1753:1: ( ( rule__Else__ScreenAssignment_1 ) )
            // InternalJConsole.g:1754:2: ( rule__Else__ScreenAssignment_1 )
            {
             before(grammarAccess.getElseAccess().getScreenAssignment_1()); 
            // InternalJConsole.g:1755:2: ( rule__Else__ScreenAssignment_1 )
            // InternalJConsole.g:1755:3: rule__Else__ScreenAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Else__ScreenAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getElseAccess().getScreenAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__Group__1__Impl"


    // $ANTLR start "rule__CLIApp__ImportsAssignment_0"
    // InternalJConsole.g:1764:1: rule__CLIApp__ImportsAssignment_0 : ( ruleImportBlock ) ;
    public final void rule__CLIApp__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1768:1: ( ( ruleImportBlock ) )
            // InternalJConsole.g:1769:2: ( ruleImportBlock )
            {
            // InternalJConsole.g:1769:2: ( ruleImportBlock )
            // InternalJConsole.g:1770:3: ruleImportBlock
            {
             before(grammarAccess.getCLIAppAccess().getImportsImportBlockParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImportBlock();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getImportsImportBlockParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__ImportsAssignment_0"


    // $ANTLR start "rule__CLIApp__VariablesAssignment_1"
    // InternalJConsole.g:1779:1: rule__CLIApp__VariablesAssignment_1 : ( ruleVariableBlock ) ;
    public final void rule__CLIApp__VariablesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1783:1: ( ( ruleVariableBlock ) )
            // InternalJConsole.g:1784:2: ( ruleVariableBlock )
            {
            // InternalJConsole.g:1784:2: ( ruleVariableBlock )
            // InternalJConsole.g:1785:3: ruleVariableBlock
            {
             before(grammarAccess.getCLIAppAccess().getVariablesVariableBlockParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleVariableBlock();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getVariablesVariableBlockParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__VariablesAssignment_1"


    // $ANTLR start "rule__CLIApp__CommandsAssignment_2_0"
    // InternalJConsole.g:1794:1: rule__CLIApp__CommandsAssignment_2_0 : ( ruleCommand ) ;
    public final void rule__CLIApp__CommandsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1798:1: ( ( ruleCommand ) )
            // InternalJConsole.g:1799:2: ( ruleCommand )
            {
            // InternalJConsole.g:1799:2: ( ruleCommand )
            // InternalJConsole.g:1800:3: ruleCommand
            {
             before(grammarAccess.getCLIAppAccess().getCommandsCommandParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleCommand();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getCommandsCommandParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__CommandsAssignment_2_0"


    // $ANTLR start "rule__CLIApp__GuardsAssignment_2_1"
    // InternalJConsole.g:1809:1: rule__CLIApp__GuardsAssignment_2_1 : ( ruleGuard ) ;
    public final void rule__CLIApp__GuardsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1813:1: ( ( ruleGuard ) )
            // InternalJConsole.g:1814:2: ( ruleGuard )
            {
            // InternalJConsole.g:1814:2: ( ruleGuard )
            // InternalJConsole.g:1815:3: ruleGuard
            {
             before(grammarAccess.getCLIAppAccess().getGuardsGuardParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGuard();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getGuardsGuardParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__GuardsAssignment_2_1"


    // $ANTLR start "rule__CLIApp__ScreensAssignment_3_0"
    // InternalJConsole.g:1824:1: rule__CLIApp__ScreensAssignment_3_0 : ( ruleScreen ) ;
    public final void rule__CLIApp__ScreensAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1828:1: ( ( ruleScreen ) )
            // InternalJConsole.g:1829:2: ( ruleScreen )
            {
            // InternalJConsole.g:1829:2: ( ruleScreen )
            // InternalJConsole.g:1830:3: ruleScreen
            {
             before(grammarAccess.getCLIAppAccess().getScreensScreenParserRuleCall_3_0_0()); 
            pushFollow(FOLLOW_2);
            ruleScreen();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getScreensScreenParserRuleCall_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__ScreensAssignment_3_0"


    // $ANTLR start "rule__CLIApp__ProceduresAssignment_3_1"
    // InternalJConsole.g:1839:1: rule__CLIApp__ProceduresAssignment_3_1 : ( ruleProcedure ) ;
    public final void rule__CLIApp__ProceduresAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1843:1: ( ( ruleProcedure ) )
            // InternalJConsole.g:1844:2: ( ruleProcedure )
            {
            // InternalJConsole.g:1844:2: ( ruleProcedure )
            // InternalJConsole.g:1845:3: ruleProcedure
            {
             before(grammarAccess.getCLIAppAccess().getProceduresProcedureParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleProcedure();

            state._fsp--;

             after(grammarAccess.getCLIAppAccess().getProceduresProcedureParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__ProceduresAssignment_3_1"


    // $ANTLR start "rule__CLIApp__InitialAssignment_4_2"
    // InternalJConsole.g:1854:1: rule__CLIApp__InitialAssignment_4_2 : ( ( RULE_ID ) ) ;
    public final void rule__CLIApp__InitialAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1858:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:1859:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:1859:2: ( ( RULE_ID ) )
            // InternalJConsole.g:1860:3: ( RULE_ID )
            {
             before(grammarAccess.getCLIAppAccess().getInitialScreenCrossReference_4_2_0()); 
            // InternalJConsole.g:1861:3: ( RULE_ID )
            // InternalJConsole.g:1862:4: RULE_ID
            {
             before(grammarAccess.getCLIAppAccess().getInitialScreenIDTerminalRuleCall_4_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCLIAppAccess().getInitialScreenIDTerminalRuleCall_4_2_0_1()); 

            }

             after(grammarAccess.getCLIAppAccess().getInitialScreenCrossReference_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CLIApp__InitialAssignment_4_2"


    // $ANTLR start "rule__ImportBlock__LinesAssignment_1"
    // InternalJConsole.g:1873:1: rule__ImportBlock__LinesAssignment_1 : ( RULE_JAVABLOCK ) ;
    public final void rule__ImportBlock__LinesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1877:1: ( ( RULE_JAVABLOCK ) )
            // InternalJConsole.g:1878:2: ( RULE_JAVABLOCK )
            {
            // InternalJConsole.g:1878:2: ( RULE_JAVABLOCK )
            // InternalJConsole.g:1879:3: RULE_JAVABLOCK
            {
             before(grammarAccess.getImportBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 
            match(input,RULE_JAVABLOCK,FOLLOW_2); 
             after(grammarAccess.getImportBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportBlock__LinesAssignment_1"


    // $ANTLR start "rule__VariableBlock__LinesAssignment_1"
    // InternalJConsole.g:1888:1: rule__VariableBlock__LinesAssignment_1 : ( RULE_JAVABLOCK ) ;
    public final void rule__VariableBlock__LinesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1892:1: ( ( RULE_JAVABLOCK ) )
            // InternalJConsole.g:1893:2: ( RULE_JAVABLOCK )
            {
            // InternalJConsole.g:1893:2: ( RULE_JAVABLOCK )
            // InternalJConsole.g:1894:3: RULE_JAVABLOCK
            {
             before(grammarAccess.getVariableBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 
            match(input,RULE_JAVABLOCK,FOLLOW_2); 
             after(grammarAccess.getVariableBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableBlock__LinesAssignment_1"


    // $ANTLR start "rule__Command__NameAssignment_1"
    // InternalJConsole.g:1903:1: rule__Command__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Command__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1907:1: ( ( RULE_ID ) )
            // InternalJConsole.g:1908:2: ( RULE_ID )
            {
            // InternalJConsole.g:1908:2: ( RULE_ID )
            // InternalJConsole.g:1909:3: RULE_ID
            {
             before(grammarAccess.getCommandAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCommandAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__NameAssignment_1"


    // $ANTLR start "rule__Command__CommandAssignment_3"
    // InternalJConsole.g:1918:1: rule__Command__CommandAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Command__CommandAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1922:1: ( ( RULE_STRING ) )
            // InternalJConsole.g:1923:2: ( RULE_STRING )
            {
            // InternalJConsole.g:1923:2: ( RULE_STRING )
            // InternalJConsole.g:1924:3: RULE_STRING
            {
             before(grammarAccess.getCommandAccess().getCommandSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getCommandAccess().getCommandSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Command__CommandAssignment_3"


    // $ANTLR start "rule__Guard__NameAssignment_1"
    // InternalJConsole.g:1933:1: rule__Guard__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Guard__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1937:1: ( ( RULE_ID ) )
            // InternalJConsole.g:1938:2: ( RULE_ID )
            {
            // InternalJConsole.g:1938:2: ( RULE_ID )
            // InternalJConsole.g:1939:3: RULE_ID
            {
             before(grammarAccess.getGuardAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getGuardAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__NameAssignment_1"


    // $ANTLR start "rule__Guard__GuardAssignment_3"
    // InternalJConsole.g:1948:1: rule__Guard__GuardAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Guard__GuardAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1952:1: ( ( RULE_STRING ) )
            // InternalJConsole.g:1953:2: ( RULE_STRING )
            {
            // InternalJConsole.g:1953:2: ( RULE_STRING )
            // InternalJConsole.g:1954:3: RULE_STRING
            {
             before(grammarAccess.getGuardAccess().getGuardSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getGuardAccess().getGuardSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Guard__GuardAssignment_3"


    // $ANTLR start "rule__Procedure__NameAssignment_1"
    // InternalJConsole.g:1963:1: rule__Procedure__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Procedure__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1967:1: ( ( RULE_ID ) )
            // InternalJConsole.g:1968:2: ( RULE_ID )
            {
            // InternalJConsole.g:1968:2: ( RULE_ID )
            // InternalJConsole.g:1969:3: RULE_ID
            {
             before(grammarAccess.getProcedureAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getProcedureAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__NameAssignment_1"


    // $ANTLR start "rule__Procedure__LinesAssignment_2"
    // InternalJConsole.g:1978:1: rule__Procedure__LinesAssignment_2 : ( RULE_JAVABLOCK ) ;
    public final void rule__Procedure__LinesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1982:1: ( ( RULE_JAVABLOCK ) )
            // InternalJConsole.g:1983:2: ( RULE_JAVABLOCK )
            {
            // InternalJConsole.g:1983:2: ( RULE_JAVABLOCK )
            // InternalJConsole.g:1984:3: RULE_JAVABLOCK
            {
             before(grammarAccess.getProcedureAccess().getLinesJAVABLOCKTerminalRuleCall_2_0()); 
            match(input,RULE_JAVABLOCK,FOLLOW_2); 
             after(grammarAccess.getProcedureAccess().getLinesJAVABLOCKTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Procedure__LinesAssignment_2"


    // $ANTLR start "rule__Screen__NameAssignment_1"
    // InternalJConsole.g:1993:1: rule__Screen__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Screen__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:1997:1: ( ( RULE_ID ) )
            // InternalJConsole.g:1998:2: ( RULE_ID )
            {
            // InternalJConsole.g:1998:2: ( RULE_ID )
            // InternalJConsole.g:1999:3: RULE_ID
            {
             before(grammarAccess.getScreenAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getScreenAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__NameAssignment_1"


    // $ANTLR start "rule__Screen__InternalProcedureAssignment_3"
    // InternalJConsole.g:2008:1: rule__Screen__InternalProcedureAssignment_3 : ( ruleDoBlock ) ;
    public final void rule__Screen__InternalProcedureAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2012:1: ( ( ruleDoBlock ) )
            // InternalJConsole.g:2013:2: ( ruleDoBlock )
            {
            // InternalJConsole.g:2013:2: ( ruleDoBlock )
            // InternalJConsole.g:2014:3: ruleDoBlock
            {
             before(grammarAccess.getScreenAccess().getInternalProcedureDoBlockParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleDoBlock();

            state._fsp--;

             after(grammarAccess.getScreenAccess().getInternalProcedureDoBlockParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__InternalProcedureAssignment_3"


    // $ANTLR start "rule__Screen__TransitionSetsAssignment_4"
    // InternalJConsole.g:2023:1: rule__Screen__TransitionSetsAssignment_4 : ( ruleWhenBlock ) ;
    public final void rule__Screen__TransitionSetsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2027:1: ( ( ruleWhenBlock ) )
            // InternalJConsole.g:2028:2: ( ruleWhenBlock )
            {
            // InternalJConsole.g:2028:2: ( ruleWhenBlock )
            // InternalJConsole.g:2029:3: ruleWhenBlock
            {
             before(grammarAccess.getScreenAccess().getTransitionSetsWhenBlockParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleWhenBlock();

            state._fsp--;

             after(grammarAccess.getScreenAccess().getTransitionSetsWhenBlockParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Screen__TransitionSetsAssignment_4"


    // $ANTLR start "rule__DoBlock__LinesAssignment_1"
    // InternalJConsole.g:2038:1: rule__DoBlock__LinesAssignment_1 : ( RULE_JAVABLOCK ) ;
    public final void rule__DoBlock__LinesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2042:1: ( ( RULE_JAVABLOCK ) )
            // InternalJConsole.g:2043:2: ( RULE_JAVABLOCK )
            {
            // InternalJConsole.g:2043:2: ( RULE_JAVABLOCK )
            // InternalJConsole.g:2044:3: RULE_JAVABLOCK
            {
             before(grammarAccess.getDoBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 
            match(input,RULE_JAVABLOCK,FOLLOW_2); 
             after(grammarAccess.getDoBlockAccess().getLinesJAVABLOCKTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DoBlock__LinesAssignment_1"


    // $ANTLR start "rule__ProcedureWhen__CommandAssignment_1"
    // InternalJConsole.g:2053:1: rule__ProcedureWhen__CommandAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ProcedureWhen__CommandAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2057:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2058:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2058:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2059:3: ( RULE_ID )
            {
             before(grammarAccess.getProcedureWhenAccess().getCommandCommandCrossReference_1_0()); 
            // InternalJConsole.g:2060:3: ( RULE_ID )
            // InternalJConsole.g:2061:4: RULE_ID
            {
             before(grammarAccess.getProcedureWhenAccess().getCommandCommandIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getProcedureWhenAccess().getCommandCommandIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getProcedureWhenAccess().getCommandCommandCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__CommandAssignment_1"


    // $ANTLR start "rule__ProcedureWhen__ProcedureAssignment_3"
    // InternalJConsole.g:2072:1: rule__ProcedureWhen__ProcedureAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__ProcedureWhen__ProcedureAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2076:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2077:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2077:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2078:3: ( RULE_ID )
            {
             before(grammarAccess.getProcedureWhenAccess().getProcedureProcedureCrossReference_3_0()); 
            // InternalJConsole.g:2079:3: ( RULE_ID )
            // InternalJConsole.g:2080:4: RULE_ID
            {
             before(grammarAccess.getProcedureWhenAccess().getProcedureProcedureIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getProcedureWhenAccess().getProcedureProcedureIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getProcedureWhenAccess().getProcedureProcedureCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ProcedureWhen__ProcedureAssignment_3"


    // $ANTLR start "rule__ScreenWhenBlock__CommandAssignment_1"
    // InternalJConsole.g:2091:1: rule__ScreenWhenBlock__CommandAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ScreenWhenBlock__CommandAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2095:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2096:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2096:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2097:3: ( RULE_ID )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getCommandCommandCrossReference_1_0()); 
            // InternalJConsole.g:2098:3: ( RULE_ID )
            // InternalJConsole.g:2099:4: RULE_ID
            {
             before(grammarAccess.getScreenWhenBlockAccess().getCommandCommandIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getScreenWhenBlockAccess().getCommandCommandIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getScreenWhenBlockAccess().getCommandCommandCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__CommandAssignment_1"


    // $ANTLR start "rule__ScreenWhenBlock__TransitionDirectAssignment_3_0"
    // InternalJConsole.g:2110:1: rule__ScreenWhenBlock__TransitionDirectAssignment_3_0 : ( ( RULE_ID ) ) ;
    public final void rule__ScreenWhenBlock__TransitionDirectAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2114:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2115:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2115:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2116:3: ( RULE_ID )
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectScreenCrossReference_3_0_0()); 
            // InternalJConsole.g:2117:3: ( RULE_ID )
            // InternalJConsole.g:2118:4: RULE_ID
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectScreenIDTerminalRuleCall_3_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectScreenIDTerminalRuleCall_3_0_0_1()); 

            }

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionDirectScreenCrossReference_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__TransitionDirectAssignment_3_0"


    // $ANTLR start "rule__ScreenWhenBlock__TransitionsAssignment_3_1_0"
    // InternalJConsole.g:2129:1: rule__ScreenWhenBlock__TransitionsAssignment_3_1_0 : ( ruleIfThen ) ;
    public final void rule__ScreenWhenBlock__TransitionsAssignment_3_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2133:1: ( ( ruleIfThen ) )
            // InternalJConsole.g:2134:2: ( ruleIfThen )
            {
            // InternalJConsole.g:2134:2: ( ruleIfThen )
            // InternalJConsole.g:2135:3: ruleIfThen
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionsIfThenParserRuleCall_3_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleIfThen();

            state._fsp--;

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionsIfThenParserRuleCall_3_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__TransitionsAssignment_3_1_0"


    // $ANTLR start "rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1"
    // InternalJConsole.g:2144:1: rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1 : ( ruleElse ) ;
    public final void rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2148:1: ( ( ruleElse ) )
            // InternalJConsole.g:2149:2: ( ruleElse )
            {
            // InternalJConsole.g:2149:2: ( ruleElse )
            // InternalJConsole.g:2150:3: ruleElse
            {
             before(grammarAccess.getScreenWhenBlockAccess().getTransitionElseElseParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleElse();

            state._fsp--;

             after(grammarAccess.getScreenWhenBlockAccess().getTransitionElseElseParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScreenWhenBlock__TransitionElseAssignment_3_1_1"


    // $ANTLR start "rule__IfThen__ConditionAssignment_1"
    // InternalJConsole.g:2159:1: rule__IfThen__ConditionAssignment_1 : ( RULE_STRING ) ;
    public final void rule__IfThen__ConditionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2163:1: ( ( RULE_STRING ) )
            // InternalJConsole.g:2164:2: ( RULE_STRING )
            {
            // InternalJConsole.g:2164:2: ( RULE_STRING )
            // InternalJConsole.g:2165:3: RULE_STRING
            {
             before(grammarAccess.getIfThenAccess().getConditionSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getIfThenAccess().getConditionSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__ConditionAssignment_1"


    // $ANTLR start "rule__IfThen__ScreenAssignment_3"
    // InternalJConsole.g:2174:1: rule__IfThen__ScreenAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__IfThen__ScreenAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2178:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2179:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2179:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2180:3: ( RULE_ID )
            {
             before(grammarAccess.getIfThenAccess().getScreenScreenCrossReference_3_0()); 
            // InternalJConsole.g:2181:3: ( RULE_ID )
            // InternalJConsole.g:2182:4: RULE_ID
            {
             before(grammarAccess.getIfThenAccess().getScreenScreenIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getIfThenAccess().getScreenScreenIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getIfThenAccess().getScreenScreenCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfThen__ScreenAssignment_3"


    // $ANTLR start "rule__Else__ScreenAssignment_1"
    // InternalJConsole.g:2193:1: rule__Else__ScreenAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Else__ScreenAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalJConsole.g:2197:1: ( ( ( RULE_ID ) ) )
            // InternalJConsole.g:2198:2: ( ( RULE_ID ) )
            {
            // InternalJConsole.g:2198:2: ( ( RULE_ID ) )
            // InternalJConsole.g:2199:3: ( RULE_ID )
            {
             before(grammarAccess.getElseAccess().getScreenScreenCrossReference_1_0()); 
            // InternalJConsole.g:2200:3: ( RULE_ID )
            // InternalJConsole.g:2201:4: RULE_ID
            {
             before(grammarAccess.getElseAccess().getScreenScreenIDTerminalRuleCall_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getElseAccess().getScreenScreenIDTerminalRuleCall_1_0_1()); 

            }

             after(grammarAccess.getElseAccess().getScreenScreenCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Else__ScreenAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000001D9000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000050002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000001400000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000002000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000012L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000004000000L});

}