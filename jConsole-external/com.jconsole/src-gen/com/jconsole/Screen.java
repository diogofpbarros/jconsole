/**
 */
package com.jconsole;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Screen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.Screen#getInternalProcedure <em>Internal Procedure</em>}</li>
 *   <li>{@link com.jconsole.Screen#getTransitionSets <em>Transition Sets</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getScreen()
 * @model
 * @generated
 */
public interface Screen extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Internal Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Procedure</em>' containment reference.
	 * @see #setInternalProcedure(Procedure)
	 * @see com.jconsole.JconsolePackage#getScreen_InternalProcedure()
	 * @model containment="true"
	 * @generated
	 */
	Procedure getInternalProcedure();

	/**
	 * Sets the value of the '{@link com.jconsole.Screen#getInternalProcedure <em>Internal Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Procedure</em>' containment reference.
	 * @see #getInternalProcedure()
	 * @generated
	 */
	void setInternalProcedure(Procedure value);

	/**
	 * Returns the value of the '<em><b>Transition Sets</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.TransitionSet}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition Sets</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getScreen_TransitionSets()
	 * @model containment="true"
	 * @generated
	 */
	EList<TransitionSet> getTransitionSets();

} // Screen
