/**
 */
package com.jconsole;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.Procedure#getLines <em>Lines</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getProcedure()
 * @model
 * @generated
 */
public interface Procedure extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Lines</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lines</em>' attribute list.
	 * @see com.jconsole.JconsolePackage#getProcedure_Lines()
	 * @model
	 * @generated
	 */
	EList<String> getLines();

} // Procedure
