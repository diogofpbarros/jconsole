/**
 */
package com.jconsole;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variables</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.Variables#getLines <em>Lines</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getVariables()
 * @model
 * @generated
 */
public interface Variables extends EObject {
	/**
	 * Returns the value of the '<em><b>Lines</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lines</em>' attribute list.
	 * @see com.jconsole.JconsolePackage#getVariables_Lines()
	 * @model
	 * @generated
	 */
	EList<String> getLines();

} // Variables
