/**
 */
package com.jconsole;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.TransitionSet#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link com.jconsole.TransitionSet#getCommand <em>Command</em>}</li>
 *   <li>{@link com.jconsole.TransitionSet#getTransitionElse <em>Transition Else</em>}</li>
 *   <li>{@link com.jconsole.TransitionSet#getTransitionDirect <em>Transition Direct</em>}</li>
 *   <li>{@link com.jconsole.TransitionSet#getProcedure <em>Procedure</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getTransitionSet()
 * @model
 * @generated
 */
public interface TransitionSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.Transition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getTransitionSet_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' reference.
	 * @see #setCommand(Command)
	 * @see com.jconsole.JconsolePackage#getTransitionSet_Command()
	 * @model required="true"
	 * @generated
	 */
	Command getCommand();

	/**
	 * Sets the value of the '{@link com.jconsole.TransitionSet#getCommand <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command</em>' reference.
	 * @see #getCommand()
	 * @generated
	 */
	void setCommand(Command value);

	/**
	 * Returns the value of the '<em><b>Transition Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition Else</em>' containment reference.
	 * @see #setTransitionElse(Transition)
	 * @see com.jconsole.JconsolePackage#getTransitionSet_TransitionElse()
	 * @model containment="true"
	 * @generated
	 */
	Transition getTransitionElse();

	/**
	 * Sets the value of the '{@link com.jconsole.TransitionSet#getTransitionElse <em>Transition Else</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition Else</em>' containment reference.
	 * @see #getTransitionElse()
	 * @generated
	 */
	void setTransitionElse(Transition value);

	/**
	 * Returns the value of the '<em><b>Transition Direct</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition Direct</em>' reference.
	 * @see #setTransitionDirect(Screen)
	 * @see com.jconsole.JconsolePackage#getTransitionSet_TransitionDirect()
	 * @model
	 * @generated
	 */
	Screen getTransitionDirect();

	/**
	 * Sets the value of the '{@link com.jconsole.TransitionSet#getTransitionDirect <em>Transition Direct</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition Direct</em>' reference.
	 * @see #getTransitionDirect()
	 * @generated
	 */
	void setTransitionDirect(Screen value);

	/**
	 * Returns the value of the '<em><b>Procedure</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure</em>' reference.
	 * @see #setProcedure(Procedure)
	 * @see com.jconsole.JconsolePackage#getTransitionSet_Procedure()
	 * @model
	 * @generated
	 */
	Procedure getProcedure();

	/**
	 * Sets the value of the '{@link com.jconsole.TransitionSet#getProcedure <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure</em>' reference.
	 * @see #getProcedure()
	 * @generated
	 */
	void setProcedure(Procedure value);

} // TransitionSet
