package com.jconsole;

public interface Pair<T, U> {

	T getFirst();

	U getSecond();

}
