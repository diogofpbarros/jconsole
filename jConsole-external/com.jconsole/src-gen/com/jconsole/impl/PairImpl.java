package com.jconsole.impl;

import com.jconsole.Pair;

public class PairImpl<T, U> implements Pair<T, U>
{
	
	private final T first;
	private final U second;
	
	public PairImpl(T t, U u) {
		first = t;
		second = u;
	}

	@Override
	public T getFirst() {
		return first;
	}

	@Override
	public U getSecond() {
		return second;
	}

}
