/**
 */
package com.jconsole.impl;

import com.jconsole.CLIApp;
import com.jconsole.Command;
import com.jconsole.Guard;
import com.jconsole.Imports;
import com.jconsole.JconsolePackage;
import com.jconsole.Procedure;
import com.jconsole.Screen;

import com.jconsole.Variables;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CLI App</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getScreens <em>Screens</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getInitial <em>Initial</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getGuards <em>Guards</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link com.jconsole.impl.CLIAppImpl#getProcedures <em>Procedures</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CLIAppImpl extends MinimalEObjectImpl.Container implements CLIApp {
	/**
	 * The cached value of the '{@link #getScreens() <em>Screens</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScreens()
	 * @generated
	 * @ordered
	 */
	protected EList<Screen> screens;

	/**
	 * The cached value of the '{@link #getInitial() <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial()
	 * @generated
	 * @ordered
	 */
	protected Screen initial;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected Variables variables;

	/**
	 * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<Command> commands;

	/**
	 * The cached value of the '{@link #getGuards() <em>Guards</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuards()
	 * @generated
	 * @ordered
	 */
	protected EList<Guard> guards;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected Imports imports;

	/**
	 * The cached value of the '{@link #getProcedures() <em>Procedures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedures()
	 * @generated
	 * @ordered
	 */
	protected EList<Procedure> procedures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CLIAppImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JconsolePackage.Literals.CLI_APP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Screen> getScreens() {
		if (screens == null) {
			screens = new EObjectContainmentEList<Screen>(Screen.class, this, JconsolePackage.CLI_APP__SCREENS);
		}
		return screens;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen getInitial() {
		if (initial != null && initial.eIsProxy()) {
			InternalEObject oldInitial = (InternalEObject) initial;
			initial = (Screen) eResolveProxy(oldInitial);
			if (initial != oldInitial) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JconsolePackage.CLI_APP__INITIAL,
							oldInitial, initial));
			}
		}
		return initial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen basicGetInitial() {
		return initial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitial(Screen newInitial) {
		Screen oldInitial = initial;
		initial = newInitial;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.CLI_APP__INITIAL, oldInitial,
					initial));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variables getVariables() {
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariables(Variables newVariables, NotificationChain msgs) {
		Variables oldVariables = variables;
		variables = newVariables;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					JconsolePackage.CLI_APP__VARIABLES, oldVariables, newVariables);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariables(Variables newVariables) {
		if (newVariables != variables) {
			NotificationChain msgs = null;
			if (variables != null)
				msgs = ((InternalEObject) variables).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.CLI_APP__VARIABLES, null, msgs);
			if (newVariables != null)
				msgs = ((InternalEObject) newVariables).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.CLI_APP__VARIABLES, null, msgs);
			msgs = basicSetVariables(newVariables, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.CLI_APP__VARIABLES, newVariables,
					newVariables));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Command> getCommands() {
		if (commands == null) {
			commands = new EObjectContainmentEList<Command>(Command.class, this, JconsolePackage.CLI_APP__COMMANDS);
		}
		return commands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Guard> getGuards() {
		if (guards == null) {
			guards = new EObjectContainmentEList<Guard>(Guard.class, this, JconsolePackage.CLI_APP__GUARDS);
		}
		return guards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Imports getImports() {
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImports(Imports newImports, NotificationChain msgs) {
		Imports oldImports = imports;
		imports = newImports;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					JconsolePackage.CLI_APP__IMPORTS, oldImports, newImports);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImports(Imports newImports) {
		if (newImports != imports) {
			NotificationChain msgs = null;
			if (imports != null)
				msgs = ((InternalEObject) imports).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.CLI_APP__IMPORTS, null, msgs);
			if (newImports != null)
				msgs = ((InternalEObject) newImports).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.CLI_APP__IMPORTS, null, msgs);
			msgs = basicSetImports(newImports, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.CLI_APP__IMPORTS, newImports,
					newImports));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Procedure> getProcedures() {
		if (procedures == null) {
			procedures = new EObjectContainmentEList<Procedure>(Procedure.class, this,
					JconsolePackage.CLI_APP__PROCEDURES);
		}
		return procedures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case JconsolePackage.CLI_APP__SCREENS:
			return ((InternalEList<?>) getScreens()).basicRemove(otherEnd, msgs);
		case JconsolePackage.CLI_APP__VARIABLES:
			return basicSetVariables(null, msgs);
		case JconsolePackage.CLI_APP__COMMANDS:
			return ((InternalEList<?>) getCommands()).basicRemove(otherEnd, msgs);
		case JconsolePackage.CLI_APP__GUARDS:
			return ((InternalEList<?>) getGuards()).basicRemove(otherEnd, msgs);
		case JconsolePackage.CLI_APP__IMPORTS:
			return basicSetImports(null, msgs);
		case JconsolePackage.CLI_APP__PROCEDURES:
			return ((InternalEList<?>) getProcedures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case JconsolePackage.CLI_APP__SCREENS:
			return getScreens();
		case JconsolePackage.CLI_APP__INITIAL:
			if (resolve)
				return getInitial();
			return basicGetInitial();
		case JconsolePackage.CLI_APP__VARIABLES:
			return getVariables();
		case JconsolePackage.CLI_APP__COMMANDS:
			return getCommands();
		case JconsolePackage.CLI_APP__GUARDS:
			return getGuards();
		case JconsolePackage.CLI_APP__IMPORTS:
			return getImports();
		case JconsolePackage.CLI_APP__PROCEDURES:
			return getProcedures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case JconsolePackage.CLI_APP__SCREENS:
			getScreens().clear();
			getScreens().addAll((Collection<? extends Screen>) newValue);
			return;
		case JconsolePackage.CLI_APP__INITIAL:
			setInitial((Screen) newValue);
			return;
		case JconsolePackage.CLI_APP__VARIABLES:
			setVariables((Variables) newValue);
			return;
		case JconsolePackage.CLI_APP__COMMANDS:
			getCommands().clear();
			getCommands().addAll((Collection<? extends Command>) newValue);
			return;
		case JconsolePackage.CLI_APP__GUARDS:
			getGuards().clear();
			getGuards().addAll((Collection<? extends Guard>) newValue);
			return;
		case JconsolePackage.CLI_APP__IMPORTS:
			setImports((Imports) newValue);
			return;
		case JconsolePackage.CLI_APP__PROCEDURES:
			getProcedures().clear();
			getProcedures().addAll((Collection<? extends Procedure>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case JconsolePackage.CLI_APP__SCREENS:
			getScreens().clear();
			return;
		case JconsolePackage.CLI_APP__INITIAL:
			setInitial((Screen) null);
			return;
		case JconsolePackage.CLI_APP__VARIABLES:
			setVariables((Variables) null);
			return;
		case JconsolePackage.CLI_APP__COMMANDS:
			getCommands().clear();
			return;
		case JconsolePackage.CLI_APP__GUARDS:
			getGuards().clear();
			return;
		case JconsolePackage.CLI_APP__IMPORTS:
			setImports((Imports) null);
			return;
		case JconsolePackage.CLI_APP__PROCEDURES:
			getProcedures().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case JconsolePackage.CLI_APP__SCREENS:
			return screens != null && !screens.isEmpty();
		case JconsolePackage.CLI_APP__INITIAL:
			return initial != null;
		case JconsolePackage.CLI_APP__VARIABLES:
			return variables != null;
		case JconsolePackage.CLI_APP__COMMANDS:
			return commands != null && !commands.isEmpty();
		case JconsolePackage.CLI_APP__GUARDS:
			return guards != null && !guards.isEmpty();
		case JconsolePackage.CLI_APP__IMPORTS:
			return imports != null;
		case JconsolePackage.CLI_APP__PROCEDURES:
			return procedures != null && !procedures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CLIAppImpl
