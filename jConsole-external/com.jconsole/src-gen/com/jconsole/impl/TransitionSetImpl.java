/**
 */
package com.jconsole.impl;

import com.jconsole.Command;
import com.jconsole.JconsolePackage;
import com.jconsole.Procedure;
import com.jconsole.Screen;
import com.jconsole.Transition;
import com.jconsole.TransitionSet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.impl.TransitionSetImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link com.jconsole.impl.TransitionSetImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link com.jconsole.impl.TransitionSetImpl#getTransitionElse <em>Transition Else</em>}</li>
 *   <li>{@link com.jconsole.impl.TransitionSetImpl#getTransitionDirect <em>Transition Direct</em>}</li>
 *   <li>{@link com.jconsole.impl.TransitionSetImpl#getProcedure <em>Procedure</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionSetImpl extends MinimalEObjectImpl.Container implements TransitionSet {
	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getCommand() <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommand()
	 * @generated
	 * @ordered
	 */
	protected Command command;

	/**
	 * The cached value of the '{@link #getTransitionElse() <em>Transition Else</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionElse()
	 * @generated
	 * @ordered
	 */
	protected Transition transitionElse;

	/**
	 * The cached value of the '{@link #getTransitionDirect() <em>Transition Direct</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionDirect()
	 * @generated
	 * @ordered
	 */
	protected Screen transitionDirect;

	/**
	 * The cached value of the '{@link #getProcedure() <em>Procedure</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedure()
	 * @generated
	 * @ordered
	 */
	protected Procedure procedure;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JconsolePackage.Literals.TRANSITION_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this,
					JconsolePackage.TRANSITION_SET__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getCommand() {
		if (command != null && command.eIsProxy()) {
			InternalEObject oldCommand = (InternalEObject) command;
			command = (Command) eResolveProxy(oldCommand);
			if (command != oldCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JconsolePackage.TRANSITION_SET__COMMAND,
							oldCommand, command));
			}
		}
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetCommand() {
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommand(Command newCommand) {
		Command oldCommand = command;
		command = newCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.TRANSITION_SET__COMMAND, oldCommand,
					command));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition getTransitionElse() {
		return transitionElse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransitionElse(Transition newTransitionElse, NotificationChain msgs) {
		Transition oldTransitionElse = transitionElse;
		transitionElse = newTransitionElse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					JconsolePackage.TRANSITION_SET__TRANSITION_ELSE, oldTransitionElse, newTransitionElse);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitionElse(Transition newTransitionElse) {
		if (newTransitionElse != transitionElse) {
			NotificationChain msgs = null;
			if (transitionElse != null)
				msgs = ((InternalEObject) transitionElse).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.TRANSITION_SET__TRANSITION_ELSE, null, msgs);
			if (newTransitionElse != null)
				msgs = ((InternalEObject) newTransitionElse).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.TRANSITION_SET__TRANSITION_ELSE, null, msgs);
			msgs = basicSetTransitionElse(newTransitionElse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.TRANSITION_SET__TRANSITION_ELSE,
					newTransitionElse, newTransitionElse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen getTransitionDirect() {
		if (transitionDirect != null && transitionDirect.eIsProxy()) {
			InternalEObject oldTransitionDirect = (InternalEObject) transitionDirect;
			transitionDirect = (Screen) eResolveProxy(oldTransitionDirect);
			if (transitionDirect != oldTransitionDirect) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT, oldTransitionDirect, transitionDirect));
			}
		}
		return transitionDirect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen basicGetTransitionDirect() {
		return transitionDirect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransitionDirect(Screen newTransitionDirect) {
		Screen oldTransitionDirect = transitionDirect;
		transitionDirect = newTransitionDirect;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT,
					oldTransitionDirect, transitionDirect));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getProcedure() {
		if (procedure != null && procedure.eIsProxy()) {
			InternalEObject oldProcedure = (InternalEObject) procedure;
			procedure = (Procedure) eResolveProxy(oldProcedure);
			if (procedure != oldProcedure) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JconsolePackage.TRANSITION_SET__PROCEDURE,
							oldProcedure, procedure));
			}
		}
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure basicGetProcedure() {
		return procedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedure(Procedure newProcedure) {
		Procedure oldProcedure = procedure;
		procedure = newProcedure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.TRANSITION_SET__PROCEDURE,
					oldProcedure, procedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case JconsolePackage.TRANSITION_SET__TRANSITIONS:
			return ((InternalEList<?>) getTransitions()).basicRemove(otherEnd, msgs);
		case JconsolePackage.TRANSITION_SET__TRANSITION_ELSE:
			return basicSetTransitionElse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case JconsolePackage.TRANSITION_SET__TRANSITIONS:
			return getTransitions();
		case JconsolePackage.TRANSITION_SET__COMMAND:
			if (resolve)
				return getCommand();
			return basicGetCommand();
		case JconsolePackage.TRANSITION_SET__TRANSITION_ELSE:
			return getTransitionElse();
		case JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT:
			if (resolve)
				return getTransitionDirect();
			return basicGetTransitionDirect();
		case JconsolePackage.TRANSITION_SET__PROCEDURE:
			if (resolve)
				return getProcedure();
			return basicGetProcedure();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case JconsolePackage.TRANSITION_SET__TRANSITIONS:
			getTransitions().clear();
			getTransitions().addAll((Collection<? extends Transition>) newValue);
			return;
		case JconsolePackage.TRANSITION_SET__COMMAND:
			setCommand((Command) newValue);
			return;
		case JconsolePackage.TRANSITION_SET__TRANSITION_ELSE:
			setTransitionElse((Transition) newValue);
			return;
		case JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT:
			setTransitionDirect((Screen) newValue);
			return;
		case JconsolePackage.TRANSITION_SET__PROCEDURE:
			setProcedure((Procedure) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case JconsolePackage.TRANSITION_SET__TRANSITIONS:
			getTransitions().clear();
			return;
		case JconsolePackage.TRANSITION_SET__COMMAND:
			setCommand((Command) null);
			return;
		case JconsolePackage.TRANSITION_SET__TRANSITION_ELSE:
			setTransitionElse((Transition) null);
			return;
		case JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT:
			setTransitionDirect((Screen) null);
			return;
		case JconsolePackage.TRANSITION_SET__PROCEDURE:
			setProcedure((Procedure) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case JconsolePackage.TRANSITION_SET__TRANSITIONS:
			return transitions != null && !transitions.isEmpty();
		case JconsolePackage.TRANSITION_SET__COMMAND:
			return command != null;
		case JconsolePackage.TRANSITION_SET__TRANSITION_ELSE:
			return transitionElse != null;
		case JconsolePackage.TRANSITION_SET__TRANSITION_DIRECT:
			return transitionDirect != null;
		case JconsolePackage.TRANSITION_SET__PROCEDURE:
			return procedure != null;
		}
		return super.eIsSet(featureID);
	}

} //TransitionSetImpl
