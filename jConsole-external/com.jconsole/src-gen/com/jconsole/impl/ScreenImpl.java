/**
 */
package com.jconsole.impl;

import com.jconsole.JconsolePackage;
import com.jconsole.Procedure;
import com.jconsole.Screen;
import com.jconsole.TransitionSet;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Screen</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.impl.ScreenImpl#getInternalProcedure <em>Internal Procedure</em>}</li>
 *   <li>{@link com.jconsole.impl.ScreenImpl#getTransitionSets <em>Transition Sets</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScreenImpl extends NamedElementImpl implements Screen {
	/**
	 * The cached value of the '{@link #getInternalProcedure() <em>Internal Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalProcedure()
	 * @generated
	 * @ordered
	 */
	protected Procedure internalProcedure;

	/**
	 * The cached value of the '{@link #getTransitionSets() <em>Transition Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitionSets()
	 * @generated
	 * @ordered
	 */
	protected EList<TransitionSet> transitionSets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScreenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JconsolePackage.Literals.SCREEN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Procedure getInternalProcedure() {
		return internalProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInternalProcedure(Procedure newInternalProcedure, NotificationChain msgs) {
		Procedure oldInternalProcedure = internalProcedure;
		internalProcedure = newInternalProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					JconsolePackage.SCREEN__INTERNAL_PROCEDURE, oldInternalProcedure, newInternalProcedure);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalProcedure(Procedure newInternalProcedure) {
		if (newInternalProcedure != internalProcedure) {
			NotificationChain msgs = null;
			if (internalProcedure != null)
				msgs = ((InternalEObject) internalProcedure).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.SCREEN__INTERNAL_PROCEDURE, null, msgs);
			if (newInternalProcedure != null)
				msgs = ((InternalEObject) newInternalProcedure).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - JconsolePackage.SCREEN__INTERNAL_PROCEDURE, null, msgs);
			msgs = basicSetInternalProcedure(newInternalProcedure, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JconsolePackage.SCREEN__INTERNAL_PROCEDURE,
					newInternalProcedure, newInternalProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransitionSet> getTransitionSets() {
		if (transitionSets == null) {
			transitionSets = new EObjectContainmentEList<TransitionSet>(TransitionSet.class, this,
					JconsolePackage.SCREEN__TRANSITION_SETS);
		}
		return transitionSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case JconsolePackage.SCREEN__INTERNAL_PROCEDURE:
			return basicSetInternalProcedure(null, msgs);
		case JconsolePackage.SCREEN__TRANSITION_SETS:
			return ((InternalEList<?>) getTransitionSets()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case JconsolePackage.SCREEN__INTERNAL_PROCEDURE:
			return getInternalProcedure();
		case JconsolePackage.SCREEN__TRANSITION_SETS:
			return getTransitionSets();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case JconsolePackage.SCREEN__INTERNAL_PROCEDURE:
			setInternalProcedure((Procedure) newValue);
			return;
		case JconsolePackage.SCREEN__TRANSITION_SETS:
			getTransitionSets().clear();
			getTransitionSets().addAll((Collection<? extends TransitionSet>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case JconsolePackage.SCREEN__INTERNAL_PROCEDURE:
			setInternalProcedure((Procedure) null);
			return;
		case JconsolePackage.SCREEN__TRANSITION_SETS:
			getTransitionSets().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case JconsolePackage.SCREEN__INTERNAL_PROCEDURE:
			return internalProcedure != null;
		case JconsolePackage.SCREEN__TRANSITION_SETS:
			return transitionSets != null && !transitionSets.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ScreenImpl
