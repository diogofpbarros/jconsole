/**
 */
package com.jconsole;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.Transition#getScreen <em>Screen</em>}</li>
 *   <li>{@link com.jconsole.Transition#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Screen</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Screen</em>' reference.
	 * @see #setScreen(Screen)
	 * @see com.jconsole.JconsolePackage#getTransition_Screen()
	 * @model required="true"
	 * @generated
	 */
	Screen getScreen();

	/**
	 * Sets the value of the '{@link com.jconsole.Transition#getScreen <em>Screen</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Screen</em>' reference.
	 * @see #getScreen()
	 * @generated
	 */
	void setScreen(Screen value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see #setCondition(String)
	 * @see com.jconsole.JconsolePackage#getTransition_Condition()
	 * @model
	 * @generated
	 */
	String getCondition();

	/**
	 * Sets the value of the '{@link com.jconsole.Transition#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' attribute.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(String value);

} // Transition
