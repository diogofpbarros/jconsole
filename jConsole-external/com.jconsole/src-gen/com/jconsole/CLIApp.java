/**
 */
package com.jconsole;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CLI App</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.jconsole.CLIApp#getScreens <em>Screens</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getInitial <em>Initial</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getVariables <em>Variables</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getCommands <em>Commands</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getGuards <em>Guards</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getImports <em>Imports</em>}</li>
 *   <li>{@link com.jconsole.CLIApp#getProcedures <em>Procedures</em>}</li>
 * </ul>
 *
 * @see com.jconsole.JconsolePackage#getCLIApp()
 * @model
 * @generated
 */
public interface CLIApp extends EObject {
	/**
	 * Returns the value of the '<em><b>Screens</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.Screen}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Screens</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getCLIApp_Screens()
	 * @model containment="true"
	 * @generated
	 */
	EList<Screen> getScreens();

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(Screen)
	 * @see com.jconsole.JconsolePackage#getCLIApp_Initial()
	 * @model
	 * @generated
	 */
	Screen getInitial();

	/**
	 * Sets the value of the '{@link com.jconsole.CLIApp#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(Screen value);

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference.
	 * @see #setVariables(Variables)
	 * @see com.jconsole.JconsolePackage#getCLIApp_Variables()
	 * @model containment="true"
	 * @generated
	 */
	Variables getVariables();

	/**
	 * Sets the value of the '{@link com.jconsole.CLIApp#getVariables <em>Variables</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variables</em>' containment reference.
	 * @see #getVariables()
	 * @generated
	 */
	void setVariables(Variables value);

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.Command}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getCLIApp_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<Command> getCommands();

	/**
	 * Returns the value of the '<em><b>Guards</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.Guard}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guards</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getCLIApp_Guards()
	 * @model containment="true"
	 * @generated
	 */
	EList<Guard> getGuards();

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference.
	 * @see #setImports(Imports)
	 * @see com.jconsole.JconsolePackage#getCLIApp_Imports()
	 * @model containment="true"
	 * @generated
	 */
	Imports getImports();

	/**
	 * Sets the value of the '{@link com.jconsole.CLIApp#getImports <em>Imports</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Imports</em>' containment reference.
	 * @see #getImports()
	 * @generated
	 */
	void setImports(Imports value);

	/**
	 * Returns the value of the '<em><b>Procedures</b></em>' containment reference list.
	 * The list contents are of type {@link com.jconsole.Procedure}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedures</em>' containment reference list.
	 * @see com.jconsole.JconsolePackage#getCLIApp_Procedures()
	 * @model containment="true"
	 * @generated
	 */
	EList<Procedure> getProcedures();

} // CLIApp
