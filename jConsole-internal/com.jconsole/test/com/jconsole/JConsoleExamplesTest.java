package com.jconsole;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import JConsolePackage.JConsole;
import jconsolebuilder.JConsoleBuilder;
import jconsolebuilder.exceptions.JConsoleException;

public class JConsoleExamplesTest 
{
	
	private static ByteArrayOutputStream out;
	
	@BeforeAll
	public static void redirectSystemOut()
	{
		// Redirect System.out
		out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
	}
	
	@Test
	public void testSingleScreenNoCommand() throws JConsoleException
	{
		
		JConsole app = JConsoleBuilder.builder()
									  .loadArgsInto(FluentAPITest.class)
									  .mainScreen("Main")
									  .screen("Main")
									  .does(() -> System.out.println("Hello, world!"))
									  .endScreen()
									  .build(); 
		
		// No input, just check if it runs and terminates
		app.run();
		
		String expected = "Hello, world!\r\n";
		String actual = out.toString();
		
		assertEquals(expected, actual);
	}
	
}
