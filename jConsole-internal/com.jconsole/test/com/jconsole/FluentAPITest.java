package com.jconsole;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import jconsolebuilder.JConsoleBuilder;
import jconsolebuilder.exceptions.CommandLeftUndefinedException;
import jconsolebuilder.exceptions.CommandSyntaxErrorException;
import jconsolebuilder.exceptions.DuplicateCommandException;
import jconsolebuilder.exceptions.DuplicateScreenException;
import jconsolebuilder.exceptions.DuplicateVarNameException;
import jconsolebuilder.exceptions.ScreenLeftUndefinedException;

public class FluentAPITest 
{

	/*
	 * ========== SCREEN TESTS ==========
	 */
	
	@Test
	public void testDuplicateScreen()
	{
		assertThrows(DuplicateScreenException.class, 
				() -> {
					JConsoleBuilder.builder()
					   .loadArgsInto(FluentAPITest.class)
					   .mainScreen("Screen")
					   .screen("Screen")
					   .does(() -> {})
					   .endScreen()
					   .screen("Screen")
					   .does(() -> {})
					   .endScreen()
					   .build();
				});
	}
	
	@Test
	public void testMainScreenUndefined()
	{
		assertThrows(ScreenLeftUndefinedException.class,
				() -> {
					JConsoleBuilder.builder()
		   			   .loadArgsInto(FluentAPITest.class)
		   			   .mainScreen("Screen")
		   			   .build();
				});
	}
	
	@Test
	public void testScreenUndefined()
	{
		assertThrows(ScreenLeftUndefinedException.class,
				() -> {
					JConsoleBuilder.builder()
		   			   .loadArgsInto(FluentAPITest.class)
		   			   .command("Command", "do")
		   			   .mainScreen("Screen")
		   			   .screen("Screen")
		   			   .does(() -> System.out.println("Hello World!"))
		   			   .onCommand("Command")
		   			   .gotoScreen("Missing")
		   			   .endScreen()
		   			   .build();
				});
	}
	
	/*
	 * ========== COMMAND TESTS ==========
	 */
	
	@Test
	public void testDuplicateCommands()
	{
		assertThrows(DuplicateCommandException.class,
				() -> {
					JConsoleBuilder.builder()
					   .loadArgsInto(FluentAPITest.class)
					   .mainScreen("Screen")
					   .screen("Screen")
					   .does(() -> {})
					   .endScreen()
					   .command("Command", "do")
					   .command("Command", "do")
					   .build();
				});
	}
	
	@Test
	public void testDuplicateVarNameInCommand()
	{
		assertThrows(DuplicateVarNameException.class,
				() -> {
					JConsoleBuilder.builder()
		   			   .loadArgsInto(FluentAPITest.class)
		   			   .mainScreen("Screen")
		   			   .screen("Screen")
		   			   .does(() -> {})
		   			   .endScreen()
		   			   .command("Command", "do <var:String> <var:String>")
		   			   .build();
				});
	}
	
	@Test
	public void testCommandSyntaxError()
	{
		assertThrows(CommandSyntaxErrorException.class,
				() -> {
					JConsoleBuilder.builder()
					   .loadArgsInto(FluentAPITest.class)
					   .mainScreen("Screen")
					   .screen("Screen")
					   .does(() -> {})
					   .endScreen()
					   .command("Command", "do <var:string continue")
					   .build();
				});
	}
	
	@Test
	public void testUndefinedCommand() throws ScreenLeftUndefinedException, DuplicateScreenException
	{
		assertThrows(CommandLeftUndefinedException.class,
				() -> {
					JConsoleBuilder.builder()
		   			   .loadArgsInto(FluentAPITest.class)
		   			   .mainScreen("Screen")
		   			   .screen("Screen")
		   			   .does(() -> System.out.println("Hello World!"))
		   			   .onCommand("Command")
		   			   .gotoScreen("Screen")
		   			   .endScreen()
		   			   .build();
				});
	}
}
