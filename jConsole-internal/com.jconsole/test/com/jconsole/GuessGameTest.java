package com.jconsole;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Random;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import JConsolePackage.JConsole;
import jconsolebuilder.JConsoleBuilder;
import jconsolebuilder.exceptions.JConsoleException;

public class GuessGameTest 
{

	private static JConsole app;
	public static int guess;
	public static int n;
	public static int attempts;
	public static int maxAttempts = 10;
	
	private static ByteArrayOutputStream out;
	
	@BeforeAll
	public static void redirectSystemOut()
	{
		// Redirect System.out
		out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
	}
	
	@BeforeAll
	public static void createApp() throws JConsoleException
	{
		// Create the app 
		// The right answer for this game is 986
		app = JConsoleBuilder.builder()
				  			 .loadArgsInto(GuessGameTest.class)
				  			 .command("Guess", "<guess:Integer>")
				  			 .mainScreen("MainMenu")
				  			 // MainMenu screen
				  			 .screen("MainMenu")
				  			 .does(() -> {
				  				 Random r = new Random(1);
				  				 n = r.nextInt(1000) + 1;
				  				 attempts = 1; 
				  				 System.out.println("Guess a number between 1 and 1000!");
				  			 })
				  			 .onCommand("Guess")
				  			 .gotoScreenIf("Win", () -> guess == n)
				  			 .elseGotoScreen("RetryGuess")
				  			 .endScreen()
				  			 // RetryGuess screen
				  			 .screen("RetryGuess")
				  			 .does(() -> {
				  				 if (n < guess)
				  					 System.out.println("Lower!");
				  				 else
				  					 System.out.println("Higher!");
				  				 attempts++;
				  			 })
				  			 .onCommand("Guess")
				  			 .gotoScreenIf("Win"       , () -> guess == n && attempts < maxAttempts)
				  			 .gotoScreenIf("RetryGuess", () -> guess != n && attempts < maxAttempts)
				  			 .gotoScreenIf("Loss"      , () -> attempts >= maxAttempts)
				  			 .endIf()
				  			 .endScreen()
				  			 // Win screen
				  			 .screen("Win")
				  			 .does(() -> System.out.println("You won!"))
				  			 .endScreen()
				  			 // Loss screen
				  			 .screen("Loss")
				  			 .does(() -> System.out.println("You lost! The answer was " + n))
				  			 .endScreen()
				  			 .build();
	}
	
	@Test
	public void normalTest()
	{
		String input = "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n" +
					   "1\n";
		
		String expected = "Guess a number between 1 and 1000!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "Higher!\r\n" + 
						  "You lost! The answer was 986\r\n";
		
		app.run(new ByteArrayInputStream(input.getBytes()));
		
		String actual = out.toString();
		
		assertEquals(expected, actual);
	}
	
}
