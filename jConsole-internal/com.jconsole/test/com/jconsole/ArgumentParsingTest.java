package com.jconsole;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import JConsolePackage.JConsole;
import jconsolebuilder.JConsoleBuilder;
import jconsolebuilder.exceptions.JConsoleException;

public class ArgumentParsingTest 
{
	
	public static boolean bool;
	public static int integer;
	public static String string;
	
	private static ByteArrayOutputStream out;
	
	@BeforeAll
	public static void redirectSystemOut()
	{
		// Redirect System.out
		out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
	}
	
	@Test
	public void testArgumentParsing() throws JConsoleException
	{
		JConsole app = JConsoleBuilder.builder()
				  .loadArgsInto(ArgumentParsingTest.class)
				  .command("Command", "parse <bool:Boolean> <integer:Integer> <string:String>")
				  .mainScreen("Main")
				  .screen("Main")
				  .does(() -> {})
				  .onCommand("Command")
				  .gotoScreen("End")
				  .endScreen()
				  .screen("End")
				  .does(() -> {})
				  .endScreen()
				  .build();
		
		String input = "parse true -1 \"abc\"";
		
		app.run(new ByteArrayInputStream(input.getBytes()));
		
		String expected = "";
		String actual = out.toString();
		
		// App has no output
		assertEquals(expected, actual);
		
		// Correct arguments parsed
		assertEquals(true, bool);
		assertEquals(-1, integer);
		assertEquals("abc", string);
	}
	
}
