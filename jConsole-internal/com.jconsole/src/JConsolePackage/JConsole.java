/**
 */
package JConsolePackage;

import java.io.InputStream;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JConsole</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link JConsolePackage.JConsole#getCliapp <em>Cliapp</em>}</li>
 * </ul>
 *
 * @see JConsolePackage.JConsolePackagePackage#getJConsole()
 * @model
 * @generated
 */
public interface JConsole extends EObject {
	/**
	 * Returns the value of the '<em><b>Cliapp</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cliapp</em>' containment reference.
	 * @see #setCliapp(CLIApp)
	 * @see JConsolePackage.JConsolePackagePackage#getJConsole_Cliapp()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CLIApp getCliapp();

	/**
	 * Sets the value of the '{@link JConsolePackage.JConsole#getCliapp <em>Cliapp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cliapp</em>' containment reference.
	 * @see #getCliapp()
	 * @generated
	 */
	void setCliapp(CLIApp value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void run();
	
	void run(InputStream is);

} // JConsole
