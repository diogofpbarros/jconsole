/**
 */
package JConsolePackage.impl;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import JConsolePackage.CLIApp;
import JConsolePackage.JConsole;
import JConsolePackage.JConsolePackagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JConsole</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link JConsolePackage.impl.JConsoleImpl#getCliapp <em>Cliapp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JConsoleImpl extends MinimalEObjectImpl.Container implements JConsole {
	/**
	 * The cached value of the '{@link #getCliapp() <em>Cliapp</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCliapp()
	 * @generated
	 * @ordered
	 */
	protected CLIApp cliapp;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JConsoleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConsolePackagePackage.Literals.JCONSOLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CLIApp getCliapp() {
		return cliapp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCliapp(CLIApp newCliapp, NotificationChain msgs) {
		CLIApp oldCliapp = cliapp;
		cliapp = newCliapp;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, JConsolePackagePackage.JCONSOLE__CLIAPP, oldCliapp, newCliapp);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCliapp(CLIApp newCliapp) {
		if (newCliapp != cliapp) {
			NotificationChain msgs = null;
			if (cliapp != null)
				msgs = ((InternalEObject)cliapp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - JConsolePackagePackage.JCONSOLE__CLIAPP, null, msgs);
			if (newCliapp != null)
				msgs = ((InternalEObject)newCliapp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - JConsolePackagePackage.JCONSOLE__CLIAPP, null, msgs);
			msgs = basicSetCliapp(newCliapp, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JConsolePackagePackage.JCONSOLE__CLIAPP, newCliapp, newCliapp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void run() {
		run(System.in);
	}
	
	@Override
	public void run(InputStream is)
	{
		Scanner sc = new Scanner(is);
		
		// TODO: should be different
		cliapp.getCurrentScreen().trigger();
		
		// TODO: this is stupid, should create a method CLIApp::ended : boolean
		boolean run = cliapp.getCurrentScreen().getTransitions() != null && !cliapp.getCurrentScreen().getTransitions().isEmpty();
		while(run)
			run = cliapp.input(sc.nextLine());
		
		sc.close();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JConsolePackagePackage.JCONSOLE__CLIAPP:
				return basicSetCliapp(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JConsolePackagePackage.JCONSOLE__CLIAPP:
				return getCliapp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JConsolePackagePackage.JCONSOLE__CLIAPP:
				setCliapp((CLIApp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.JCONSOLE__CLIAPP:
				setCliapp((CLIApp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.JCONSOLE__CLIAPP:
				return cliapp != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case JConsolePackagePackage.JCONSOLE___RUN:
				run();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //JConsoleImpl
