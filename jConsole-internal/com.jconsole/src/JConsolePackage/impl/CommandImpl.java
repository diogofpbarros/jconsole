/**
 */
package JConsolePackage.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import JConsolePackage.Command;
import JConsolePackage.JConsolePackagePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul> 
 *   <li>{@link JConsolePackage.impl.CommandImpl#getCommand <em>Command</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandImpl extends NamedElementImpl implements Command {
	
	// ========== ADDED CODE ==========

	private static final HashMap<Class<?>, Function<String, ?>> PARSERS = getParsers();

	private static final HashMap<Class<?>, Function<String, ?>> getParsers()
	{
		HashMap<Class<?>, Function<String, ?>> parsers = new HashMap<>();

		parsers.put(Integer.class, Integer::parseInt);
		parsers.put(String.class, (s) -> s);
		parsers.put(Boolean.class, Boolean::parseBoolean);

		return parsers;
	}

	protected ArrayList<Pair<String, Class<?>>> arguments = new ArrayList<>();
	protected String commandRegex;
	protected Class<?> loadInto;

	// ========== ADDED CODE ==========
	
	/**
	 * The default value of the '{@link #getCommand() <em>Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommand()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMAND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCommand() <em>Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommand()
	 * @generated
	 * @ordered
	 */
	protected String command = COMMAND_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConsolePackagePackage.Literals.COMMAND;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCommand() {
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCommand(String newCommand) {
		String oldCommand = command;
		command = newCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JConsolePackagePackage.COMMAND__COMMAND, oldCommand, command));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean matches(String input) {
		return Pattern.matches(commandRegex, input);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void parse(String input) {
		Matcher m = Pattern.compile(commandRegex).matcher(input);

		// TODO: use m.find() to catch an error
		m.find();

		int i = 1;

		String fieldName;
		Class<?> type;
		Object value;
		for (Pair<String, Class<?>> pair : arguments)
		{

			fieldName = pair.getFirst();
			type = pair.getSecond();
			
			value = PARSERS.get(type).apply(m.group(i));

			try {
				loadInto.getField(fieldName).set(null, value);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			i++;
		}
	}
	
	@Override
	public void setLoadInto(Class<?> loadInto) 
	{
		this.loadInto = loadInto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JConsolePackagePackage.COMMAND__COMMAND:
				return getCommand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JConsolePackagePackage.COMMAND__COMMAND:
				setCommand((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.COMMAND__COMMAND:
				setCommand(COMMAND_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.COMMAND__COMMAND:
				return COMMAND_EDEFAULT == null ? command != null : !COMMAND_EDEFAULT.equals(command);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case JConsolePackagePackage.COMMAND___MATCHES__STRING:
				return matches((String)arguments.get(0));
			case JConsolePackagePackage.COMMAND___PARSE__STRING:
				parse((String)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (command: ");
		result.append(command);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setArguments(ArrayList<Pair<String, Class<?>>> arguments) {
		this.arguments = arguments;
	}

	@Override
	public void setCommandRegex(String commandRegex) {
		this.commandRegex = commandRegex;
	}

} //CommandImpl
