/**
 */
package JConsolePackage.impl;

import JConsolePackage.CLIApp;
import JConsolePackage.JConsolePackagePackage;
import JConsolePackage.Screen;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CLI App</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link JConsolePackage.impl.CLIAppImpl#getScreens <em>Screens</em>}</li>
 *   <li>{@link JConsolePackage.impl.CLIAppImpl#getInitial <em>Initial</em>}</li>
 *   <li>{@link JConsolePackage.impl.CLIAppImpl#getCurrentScreen <em>Current Screen</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CLIAppImpl extends NamedElementImpl implements CLIApp {
	/**
	 * The cached value of the '{@link #getScreens() <em>Screens</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScreens()
	 * @generated
	 * @ordered
	 */
	protected EList<Screen> screens;

	/**
	 * The cached value of the '{@link #getInitial() <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial()
	 * @generated
	 * @ordered
	 */
	protected Screen initial;

	/**
	 * The cached value of the '{@link #getCurrentScreen() <em>Current Screen</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentScreen()
	 * @generated
	 * @ordered
	 */
	protected Screen currentScreen;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CLIAppImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConsolePackagePackage.Literals.CLI_APP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Screen> getScreens() {
		if (screens == null) {
			screens = new EObjectContainmentEList<Screen>(Screen.class, this, JConsolePackagePackage.CLI_APP__SCREENS);
		}
		return screens;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Screen getInitial() {
		if (initial != null && initial.eIsProxy()) {
			InternalEObject oldInitial = (InternalEObject)initial;
			initial = (Screen)eResolveProxy(oldInitial);
			if (initial != oldInitial) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JConsolePackagePackage.CLI_APP__INITIAL, oldInitial, initial));
			}
		}
		return initial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen basicGetInitial() {
		return initial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInitial(Screen newInitial) {
		Screen oldInitial = initial;
		initial = newInitial;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JConsolePackagePackage.CLI_APP__INITIAL, oldInitial, initial));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Screen getCurrentScreen() {
		if (currentScreen != null && currentScreen.eIsProxy()) {
			InternalEObject oldCurrentScreen = (InternalEObject)currentScreen;
			currentScreen = (Screen)eResolveProxy(oldCurrentScreen);
			if (currentScreen != oldCurrentScreen) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, JConsolePackagePackage.CLI_APP__CURRENT_SCREEN, oldCurrentScreen, currentScreen));
			}
		}
		return currentScreen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen basicGetCurrentScreen() {
		return currentScreen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCurrentScreen(Screen newCurrentScreen) {
		Screen oldCurrentScreen = currentScreen;
		currentScreen = newCurrentScreen;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JConsolePackagePackage.CLI_APP__CURRENT_SCREEN, oldCurrentScreen, currentScreen));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean input(String input) {
		currentScreen = currentScreen.input(input);
		
		boolean hasTransitions = currentScreen.getTransitions() != null && !currentScreen.getTransitions().isEmpty();
		
		return hasTransitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JConsolePackagePackage.CLI_APP__SCREENS:
				return ((InternalEList<?>)getScreens()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JConsolePackagePackage.CLI_APP__SCREENS:
				return getScreens();
			case JConsolePackagePackage.CLI_APP__INITIAL:
				if (resolve) return getInitial();
				return basicGetInitial();
			case JConsolePackagePackage.CLI_APP__CURRENT_SCREEN:
				if (resolve) return getCurrentScreen();
				return basicGetCurrentScreen();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JConsolePackagePackage.CLI_APP__SCREENS:
				getScreens().clear();
				getScreens().addAll((Collection<? extends Screen>)newValue);
				return;
			case JConsolePackagePackage.CLI_APP__INITIAL:
				setInitial((Screen)newValue);
				return;
			case JConsolePackagePackage.CLI_APP__CURRENT_SCREEN:
				setCurrentScreen((Screen)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.CLI_APP__SCREENS:
				getScreens().clear();
				return;
			case JConsolePackagePackage.CLI_APP__INITIAL:
				setInitial((Screen)null);
				return;
			case JConsolePackagePackage.CLI_APP__CURRENT_SCREEN:
				setCurrentScreen((Screen)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JConsolePackagePackage.CLI_APP__SCREENS:
				return screens != null && !screens.isEmpty();
			case JConsolePackagePackage.CLI_APP__INITIAL:
				return initial != null;
			case JConsolePackagePackage.CLI_APP__CURRENT_SCREEN:
				return currentScreen != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case JConsolePackagePackage.CLI_APP___INPUT__STRING:
				return input((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //CLIAppImpl
