/**
 */
package JConsolePackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JConsolePackage.JConsolePackagePackage#getCondition()
 * @model interface="true" abstract="true"
 * @generated
 */
@FunctionalInterface
public interface Condition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean test();

} // Condition
