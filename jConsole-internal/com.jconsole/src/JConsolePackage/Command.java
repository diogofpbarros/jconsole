/**
 */
package JConsolePackage;

import java.util.ArrayList;

import JConsolePackage.impl.Pair;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link JConsolePackage.Command#getCommand <em>Command</em>}</li>
 * </ul>
 *
 * @see JConsolePackage.JConsolePackagePackage#getCommand()
 * @model
 * @generated
 */
public interface Command extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' attribute.
	 * @see #setCommand(String)
	 * @see JConsolePackage.JConsolePackagePackage#getCommand_Command()
	 * @model
	 * @generated
	 */
	String getCommand();

	/**
	 * Sets the value of the '{@link JConsolePackage.Command#getCommand <em>Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command</em>' attribute.
	 * @see #getCommand()
	 * @generated
	 */
	void setCommand(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean matches(String input);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void parse(String input);

	void setLoadInto(Class<?> loadInto);

	void setArguments(ArrayList<Pair<String, Class<?>>> arguments);

	void setCommandRegex(String commandRegex);

} // Command
