/**
 */
package JConsolePackage;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CLI App</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link JConsolePackage.CLIApp#getScreens <em>Screens</em>}</li>
 *   <li>{@link JConsolePackage.CLIApp#getInitial <em>Initial</em>}</li>
 *   <li>{@link JConsolePackage.CLIApp#getCurrentScreen <em>Current Screen</em>}</li>
 * </ul>
 *
 * @see JConsolePackage.JConsolePackagePackage#getCLIApp()
 * @model
 * @generated
 */
public interface CLIApp extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Screens</b></em>' containment reference list.
	 * The list contents are of type {@link JConsolePackage.Screen}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Screens</em>' containment reference list.
	 * @see JConsolePackage.JConsolePackagePackage#getCLIApp_Screens()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Screen> getScreens();

	/**
	 * Returns the value of the '<em><b>Initial</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial</em>' reference.
	 * @see #setInitial(Screen)
	 * @see JConsolePackage.JConsolePackagePackage#getCLIApp_Initial()
	 * @model required="true"
	 * @generated
	 */
	Screen getInitial();

	/**
	 * Sets the value of the '{@link JConsolePackage.CLIApp#getInitial <em>Initial</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial</em>' reference.
	 * @see #getInitial()
	 * @generated
	 */
	void setInitial(Screen value);

	/**
	 * Returns the value of the '<em><b>Current Screen</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Screen</em>' reference.
	 * @see #setCurrentScreen(Screen)
	 * @see JConsolePackage.JConsolePackagePackage#getCLIApp_CurrentScreen()
	 * @model required="true"
	 * @generated
	 */
	Screen getCurrentScreen();

	/**
	 * Sets the value of the '{@link JConsolePackage.CLIApp#getCurrentScreen <em>Current Screen</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Screen</em>' reference.
	 * @see #getCurrentScreen()
	 * @generated
	 */
	void setCurrentScreen(Screen value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean input(String input);

} // CLIApp
