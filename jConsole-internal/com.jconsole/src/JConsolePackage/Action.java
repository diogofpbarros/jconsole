/**
 */
package JConsolePackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see JConsolePackage.JConsolePackagePackage#getAction()
 * @model interface="true" abstract="true"
 * @generated NOT
 */
@FunctionalInterface
public interface Action {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void trigger();

} // Action
