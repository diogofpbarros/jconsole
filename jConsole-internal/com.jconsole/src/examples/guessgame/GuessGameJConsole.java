package examples.guessgame;

import java.util.Random;

import JConsolePackage.JConsole;
import jconsolebuilder.JConsoleBuilder;
import jconsolebuilder.exceptions.JConsoleException;

public class GuessGameJConsole 
{
	
	public static int guess;
	public static int n;
	public static int attempts;
	public static int maxAttempts = 10;
	
	private JConsole app;
	
	public GuessGameJConsole() 
	{
		app = null;
		
		try {
			app = JConsoleBuilder.builder()
					  .loadArgsInto(GuessGameJConsole.class)
					  .command("Guess", "<guess:Integer>")
					  .mainScreen("MainMenu")
					  // MainMenu screen
			        .screen("MainMenu")
			        .does(() -> {
			      	  Random r = new Random(1);
			            n = r.nextInt(1000) + 1;
			            attempts = 1; 
			            System.out.println("Guess a number between 1 and 1000!");
			        })
			        .onCommand("Guess")
			        .gotoScreenIf("Win", () -> guess == n)
			        .elseGotoScreen("RetryGuess")
			        .endScreen()
			        // RetryGuess screen
			        .screen("RetryGuess")
			        .does(() -> {
			      	  if (n < guess)
			      		  System.out.println("Lower!");
			      	  else
			      		  System.out.println("Higher!");
			      	  attempts++;
			        })
			        .onCommand("Guess")
			        .gotoScreenIf("Win"       , () -> guess == n && attempts < maxAttempts)
			        .gotoScreenIf("RetryGuess", () -> guess != n && attempts < maxAttempts)
			        .gotoScreenIf("Loss"      , () -> attempts >= maxAttempts)
			        .endIf()
			        .endScreen()
			        // Win screen
			        .screen("Win")
			        .does(() -> System.out.println("You won!"))
			        .endScreen()
			        // Loss screen
			        .screen("Loss")
			        .does(() -> System.out.println("You lost! The answer was " + n))
			        .endScreen()
			        .build();
		} 
		catch (JConsoleException e) 
		{
			// Should not raise any errors
			e.printStackTrace();
		}
	}
	
	public void run()
	{
		app.run();
	}
	
	public static void main(String[] args) throws JConsoleException 
	{
		
		GuessGameJConsole guessgame = new GuessGameJConsole();
		
		guessgame.run();
	}
}
