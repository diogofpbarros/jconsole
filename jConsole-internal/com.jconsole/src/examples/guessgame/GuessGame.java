package examples.guessgame;

import java.util.Random;
import java.util.Scanner;

public class GuessGame 
{

	public static void main(String[] args) 
	{
		
		Random r = new Random();
		final int n = r.nextInt(1000) + 1;
		
		final int MAX_ATTEMPTS = 10;
		
		Scanner sc = new Scanner(System.in);
		
		int guess = 0;
		
		for (int i = 0; i < MAX_ATTEMPTS; i++)
		{
			System.out.println("Make a guess:");
			guess = sc.nextInt();
			
			if (guess == n)
				break;
			
			if (guess < n)
				System.out.println("Higher!");
			else
				System.out.println("Lower!");
			
			System.out.println();
		}
		
		if (guess == n)
			System.out.println("You win!");
		else
			System.out.println("You lose! The answer was: " + n);
		
		sc.close();

	}

}
