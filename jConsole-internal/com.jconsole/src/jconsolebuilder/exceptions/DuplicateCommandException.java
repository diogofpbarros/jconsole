package jconsolebuilder.exceptions;

public class DuplicateCommandException extends JConsoleException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateCommandException(String commandName)
	{
		super("Duplicate declaration of command '" + commandName + "'");
	}
	
}
