package jconsolebuilder.exceptions;

public class CommandLeftUndefinedException extends JConsoleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandLeftUndefinedException(String commandName) {
		super("Command '" + commandName + "' was left undefined upon build");
	}

}
