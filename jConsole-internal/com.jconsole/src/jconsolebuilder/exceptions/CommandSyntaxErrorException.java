package jconsolebuilder.exceptions;

public class CommandSyntaxErrorException extends JConsoleException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommandSyntaxErrorException(String commandName) {
		super("Command '" + commandName + "' is malformed");
	}

}
