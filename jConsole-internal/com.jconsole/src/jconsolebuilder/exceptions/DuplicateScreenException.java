package jconsolebuilder.exceptions;

public class DuplicateScreenException extends JConsoleException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateScreenException(String screenName)
	{
		super("Duplicate declaration of screen '" + screenName + "'");
	}
	
}
