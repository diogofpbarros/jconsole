package jconsolebuilder.exceptions;

public class JConsoleException extends Exception 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public JConsoleException(String string) {
		super(string);
	}
}
