package jconsolebuilder.exceptions;

public class DuplicateVarNameException extends JConsoleException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateVarNameException(String varName, String commandName) {
		super("Variable '" + varName + "' is used twice in the command '" + commandName + "'");
	}

}
