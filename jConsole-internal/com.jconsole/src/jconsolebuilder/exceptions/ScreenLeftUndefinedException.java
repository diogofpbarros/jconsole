package jconsolebuilder.exceptions;

public class ScreenLeftUndefinedException extends JConsoleException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ScreenLeftUndefinedException(String screenName) {
		super("Screen '" + screenName + "' was left undefined upon build");
	}

}
