package jconsolebuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import JConsolePackage.Action;
import JConsolePackage.CLIApp;
import JConsolePackage.Command;
import JConsolePackage.Condition;
import JConsolePackage.Guard;
import JConsolePackage.JConsole;
import JConsolePackage.JConsolePackageFactory;
import JConsolePackage.Screen;
import JConsolePackage.Transition;
import JConsolePackage.impl.Pair;
import jconsolebuilder.exceptions.CommandLeftUndefinedException;
import jconsolebuilder.exceptions.CommandSyntaxErrorException;
import jconsolebuilder.exceptions.DuplicateCommandException;
import jconsolebuilder.exceptions.DuplicateScreenException;
import jconsolebuilder.exceptions.DuplicateVarNameException;
import jconsolebuilder.exceptions.ScreenLeftUndefinedException;

public class JConsoleBuilder 
{

	private static JConsolePackageFactory factory = JConsolePackageFactory.eINSTANCE;
	
	private static final String VAR_REGEX = "\\<(\\w+)\\s*:\\s*(Integer|String|Boolean)\\>";
	private static final Pattern VAR_PATTERN = Pattern.compile(VAR_REGEX);
	
	private static final String COMMAND_REGEX = "^\\s*(\\w+\\s*|\\<\\w+\\s*:\\s*(?:Integer|String|Boolean)\\>\\s*)*$";
	
	public static WaitingLoadInto builder()
	{
		return new ConsoleBeingBuilt(factory.createCLIApp());
	}
	
	public static class ConsoleBeingBuilt implements WaitingLoadInto, WaitingMainScreen, WaitingScreen, WaitingDoes, WaitingTransition, WaitingGoto, WaitingCondition
	{
		
		private CLIApp appBeingBuilt;
		
		// Map of existing commands
		private HashMap<String, Command> commands = new HashMap<>();
		// Map to check if a command was already defined
		private HashMap<String, Boolean> definedCommands = new HashMap<>();
		
		// Map of existing screens
		private HashMap<String, Screen> screens = new HashMap<>();
		// Map to check if a screen was already defined
		private HashMap<String, Boolean> definedScreens = new HashMap<>();

		// Hanging objects
		private String mainScreenName;
		
		private Screen currentScreen;
		private Command currentCommand;
		private Class<?> loadInto;
		
		public ConsoleBeingBuilt(CLIApp appBeingBuilt) 
		{
			this.appBeingBuilt = appBeingBuilt;
		}
		
		public ConsoleBeingBuilt loadArgsInto(Class<?> clazz)
		{
			loadInto = clazz;
			
			return this;
		}
		
		public ConsoleBeingBuilt command(String commandName, String command) throws DuplicateCommandException, DuplicateVarNameException, CommandSyntaxErrorException 
		{
			if (commands.containsKey(commandName))
				throw new DuplicateCommandException(commandName);
			
			Command c = factory.createCommand();
			c.setName(commandName);
			c.setCommand(command);
			c.setLoadInto(loadInto);
			
			// ========== ADDED CODE ==========

			// Validate
			if(!Pattern.matches(COMMAND_REGEX, command))
				throw new CommandSyntaxErrorException(commandName);

			// Parse arguments - type and variable name

			Matcher m = VAR_PATTERN.matcher(command);
			String varname;
			Class<?> type = null;
			ArrayList<Pair<String, Class<?>>> arguments = new ArrayList<>();
			while (m.find())
			{
				varname = m.group(1);
				try {
					type = Class.forName("java.lang." + m.group(2));
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				for (Pair<String, Class<?>> pair : arguments)
					if (pair.getFirst().equals(varname))
						throw new DuplicateVarNameException(varname, commandName);
				
				arguments.add(new Pair<String, Class<?>>(varname, type));
			}
			c.setArguments(arguments);

			// Translate to a regex

			String commandRegex = "^" + 
					        	  command.replaceAll("\\<[^\\<]+?:Integer\\>", "(-?\\\\d+)")		// Replace Integer
						   		         .replaceAll("\\<[^\\<]+?:String\\>" , "\\\"(\\\\w*)\\\"") // Replace String
						   		         .replaceAll("\\<[^\\<]+?:Boolean\\>", "(true|false)") +   // Replace Boolean
						          "$";
			c.setCommandRegex(commandRegex);
			
			// ========== ADDED CODE ==========
			
			
			
			commands.put(commandName, c);
			
			return this;
		}

		public ConsoleBeingBuilt mainScreen(String screen) 
		{
			mainScreenName = screen;
			
			// Main screen was not defined yet
			definedScreens.put(screen, false);
			
			return this;
		}

		public ConsoleBeingBuilt screen(String name) throws DuplicateScreenException
		{	
			if(definedScreens.getOrDefault(name, false))
				throw new DuplicateScreenException(name);
				
			
			Screen s = getOrCreateScreen(name);
			screens.put(name, s);
			definedScreens.put(name, true);
			
			this.currentScreen = s;
			
			return this;
		}

		public ConsoleBeingBuilt does(Action action) 
		{	
			currentScreen.setAction(action);
//			System.out.println("Added action '" + action + "' to '" + currentScreen.getName() + "'");
			return this;
		}

		public ConsoleBeingBuilt onCommand(String commandName) 
		{	
			currentCommand = getOrCreateCommand(commandName);
			
			return this;
		}
		
		public ConsoleBeingBuilt gotoScreen(String screen) {
			
			Screen target = getOrCreateScreen(screen);
			
			Transition t = factory.createTransition();
			t.setSource(currentScreen);
			t.setCommand(currentCommand);
			t.setTarget(target);
			
			currentScreen.getTransitions().add(t);
			
			return this;
		}

		public ConsoleBeingBuilt gotoScreenIf(String screen, Condition condition) 
		{	
			Screen target = getOrCreateScreen(screen);
			
			Transition t = factory.createTransition();
			t.setSource(currentScreen);
			t.setCommand(currentCommand);
			t.setTarget(target);
			
			Guard g = factory.createGuard();
			g.setCondition(condition);
			t.setGuard(g);
			
			currentScreen.getTransitions().add(t);
			
			return this;
		}

		public ConsoleBeingBuilt elseGotoScreen(String screen) 
		{
			// Syntactic sugar
			return gotoScreen(screen);
		}
		
		public ConsoleBeingBuilt endIf() 
		{
			// Does nothing, only as a way to change fluent API
			return this;
		}
		
		public ConsoleBeingBuilt endScreen() 
		{
			// Does nothing, only as a way to change fluent API
			return this;
		}
		
		public JConsole build() throws ScreenLeftUndefinedException, CommandLeftUndefinedException
		{
			JConsole jc = factory.createJConsole();
			jc.setCliapp(appBeingBuilt);
			
			// Verify that no screen was left undefined
			for (Entry<String, Boolean> entry : definedScreens.entrySet())
				if (!entry.getValue())
					throw new ScreenLeftUndefinedException(entry.getKey());
			
			// Verify that no command was left undefined
			for (Entry<String, Boolean> entry : definedCommands.entrySet())
				if (!entry.getValue())
					throw new CommandLeftUndefinedException(entry.getKey());

			Screen s = screens.get(mainScreenName);
			
			appBeingBuilt.setInitial(s);
			appBeingBuilt.setCurrentScreen(s);
			
			return jc;
		}
		
		// ------------ UTIL ------------
		
		private Command getOrCreateCommand(String commandName)
		{
			// If screen already exists, return it
			if(commands.containsKey(commandName))
				return commands.get(commandName);

			// Create screen & set name
			Command c = factory.createCommand();
			c.setName(commandName);

			// Add to map
			commands.put(commandName, c);
			// Mark it as not being defined
			definedCommands.put(commandName, false);

			// Return it
			return c;
		}
		
		private Screen getOrCreateScreen(String screenName)
		{
			// If screen already exists, return it
			if(screens.containsKey(screenName))
				return screens.get(screenName);
			
			// Create screen & set name
			Screen s = factory.createScreen();
			s.setName(screenName);

			// Add to map
			screens.put(screenName, s);
			// Mark it as not being defined
			definedScreens.put(screenName, false);
			
			// Return it
			return s;
		}
		
	}
	
	// ========== Fluent API ==========
	
	public interface WaitingLoadInto
	{
		public WaitingMainScreen loadArgsInto(Class<?> clazz);
	}
	
	public interface WaitingMainScreen 
	{
		public WaitingMainScreen command(String name, String command) throws DuplicateCommandException, DuplicateVarNameException, CommandSyntaxErrorException;
		public WaitingScreen mainScreen(String screen);
	}
	
	public interface WaitingScreen
	{
		public WaitingScreen command(String name, String command) throws DuplicateCommandException, DuplicateVarNameException, CommandSyntaxErrorException;
		public WaitingDoes screen(String name) throws DuplicateScreenException;
		
		public JConsole build() throws ScreenLeftUndefinedException, CommandLeftUndefinedException;
	}
	
	public interface WaitingDoes 
	{
		public WaitingTransition does(Action action);
	}
	
	public interface WaitingTransition 
	{
		public WaitingScreen endScreen();
		public WaitingGoto onCommand(String commandName);
	}
	
	public interface WaitingGoto 
	{
		public WaitingTransition gotoScreen(String screen);
		public WaitingCondition gotoScreenIf(String screen, Condition condition);
	}
	
	public interface WaitingCondition
	{
		public WaitingTransition endIf();
		public WaitingTransition elseGotoScreen(String screen);
		public WaitingCondition gotoScreenIf(String screen, Condition condition);
	}

}
