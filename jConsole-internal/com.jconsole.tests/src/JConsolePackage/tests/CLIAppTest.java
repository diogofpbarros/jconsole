/**
 */
package JConsolePackage.tests;

import JConsolePackage.CLIApp;
import JConsolePackage.JConsolePackageFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CLI App</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link JConsolePackage.CLIApp#input(java.lang.String) <em>Input</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CLIAppTest extends NamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CLIAppTest.class);
	}

	/**
	 * Constructs a new CLI App test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CLIAppTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CLI App test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CLIApp getFixture() {
		return (CLIApp)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JConsolePackageFactory.eINSTANCE.createCLIApp());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link JConsolePackage.CLIApp#input(java.lang.String) <em>Input</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see JConsolePackage.CLIApp#input(java.lang.String)
	 * @generated
	 */
	public void testInput__String() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //CLIAppTest
