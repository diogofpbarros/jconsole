/**
 */
package JConsolePackage.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>JConsolePackage</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class JConsolePackageTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new JConsolePackageTests("JConsolePackage Tests");
		suite.addTestSuite(JConsoleTest.class);
		suite.addTestSuite(ScreenTest.class);
		suite.addTestSuite(CLIAppTest.class);
		suite.addTestSuite(TransitionTest.class);
		suite.addTestSuite(GuardTest.class);
		suite.addTestSuite(CommandTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JConsolePackageTests(String name) {
		super(name);
	}

} //JConsolePackageTests
