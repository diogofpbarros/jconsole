/**
 */
package JConsolePackage.tests;

import JConsolePackage.Action;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link JConsolePackage.Action#trigger() <em>Trigger</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class ActionTest extends TestCase {

	/**
	 * The fixture for this Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Action fixture = null;

	/**
	 * Constructs a new Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Action fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Action getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link JConsolePackage.Action#trigger() <em>Trigger</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see JConsolePackage.Action#trigger()
	 * @generated
	 */
	public void testTrigger() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ActionTest
