/**
 */
package JConsolePackage.tests;

import JConsolePackage.JConsolePackageFactory;
import JConsolePackage.Screen;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Screen</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link JConsolePackage.Screen#input(java.lang.String) <em>Input</em>}</li>
 *   <li>{@link JConsolePackage.Screen#trigger() <em>Trigger</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ScreenTest extends NamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ScreenTest.class);
	}

	/**
	 * Constructs a new Screen test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScreenTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Screen test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Screen getFixture() {
		return (Screen)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(JConsolePackageFactory.eINSTANCE.createScreen());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link JConsolePackage.Screen#input(java.lang.String) <em>Input</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see JConsolePackage.Screen#input(java.lang.String)
	 * @generated
	 */
	public void testInput__String() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link JConsolePackage.Screen#trigger() <em>Trigger</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see JConsolePackage.Screen#trigger()
	 * @generated
	 */
	public void testTrigger() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ScreenTest
