```
   _  _____                      _      
  (_)/ ____|                    | |     
   _| |     ___  _ __  ___  ___ | | ___
  | | |    / _ \| '_ \/ __|/ _ \| |/ _ \
  | | |___| (_) | | | \__ \ (_) | |  __/
  | |\_____\___/|_| |_|___/\___/|_|\___|
 _/ |                                   
|__/                                                                                                     
```

# Intro

## Motivation
- **Developing CLIs is tedious and repetitive** - same setup & structure,
  different content & behaviour
- **Parsing user input is not always trivial** - imagine parsing 4 flags, 2
  options, a date, a time and a pair of boots
- **Everyone does it differently** - the focus should be on the **what** not
  the **how**
- **Modular, modular, modular!** - modular code is easier to maintain
  and develop

## Features
- **Define commands with typing** - parsing and enforcing commands is done
  backstage
- **Screen as programming unit** - programm screens individually and
  connect them
- **Conditional guards for transitions (checked after a command call)** -
  commands are only valid when some condition(s) checks out
- **Java code integration for the CLI's backend** - integrate any Java code,
  library or API and use it in the CLI

# Use cases
This section presents some use cases of the jConsole DSL.

## 1. Guessing Game CLI
The user wants to program a command line game with the following rules:
- A random number is generated between 1 and 1000
- The player has 10 attempts to guess the number
- In each incorrect attempt, the game says ‘Higher’ or ‘Lower’
- The player wins if he guesses within those 10 attempts

To do this, the user draws a flowchart with all screens and commands
  (user input) he wants to have, to help him program:

 ![flowchart](resources/images/guessgame_flowchart.png)

From this, the user can program each screen:
  - MainMenu
  - RetryGuess
  - Won
  - Lost

The commands between each screen (in this case it's only one):
  - guess - which is an Integer

And the guards needed:
  - guess == n
  - attempts < 10

## 2. Metro CLI
The user developed an API to interact with the Metropolitan of Lisbon, and
  decided to create a CLI as a first user interface.

This new app will have the following menus:
  - A main menu to navigate between menus
  - A menu for consulting train times
  - A menu for consulting the state of a metro line

For each functionality, there is a command defined that triggers a procedure
  that uses the API and outputs information to the user.

To navigate between menus, there are commands that take the user to the
  respective screen, otherwise, the user can use the commands "back" and "exit"
  to return to the main screen or to exit the CLI, respectively.

# Syntax
This section explains and showcases the DSL's syntax and should be used as a
  beginner's guide.

The **Guessing Game CLI** example is used to show most of the syntax.

## Command language
Commands are a great part of this DSL, and as such, their format must follow
  some rules.

In a simple way, commands can be seen as simple strings that represent:
  - the format of the command
  - command arguments, and their types

For example, the command ```exit``` can be represented as the string ```"exit"```.

However, almost every CLI has variable inputs (user choice, argument, flags,
  etc.). To represent this, the programmer can define that some part of the
  command will be an argument that is parsed as a given type.

In commands, an argument input is represented as:
  ```
  <varName : varType>
  ```
Where ```varName``` is the name of the argument (that will be stored in the variable
  with the same name) and ```varType``` is the type of the argument.
  jConsole provides 3 native types for arguments:
    - Integer
    - String (surrounded by ")
    - Boolean


Visiting our **Guessing Game CLI** example, to represent that a user inputs an Integer
  we use ```<guess : Integer>```.

## Internal
JConsole's internal syntax was tweaked to have the possibility to define
  conditions/guards and actions as functional interfaces. This behaviour is
  normally not possible due to the ECore specificity, but in this particular
  case where these objects do not 'belong' to other objects or have an
  'identity', ECore properties can be ignored.

Even more, there are some exceptions that are thrown in execution time (namely,
  during app building) and help the programmer make correct use of the jConsole
  DSL.

### Domain Model
Domain model as modelled in the ECore framework:
![domain_model_internal](resources/images/domain_model_internal.png)

### Fluent API diagram
The jConsole DSL provides the programmer a fluent API as a way to help the
  programmer and verify the correct state of the CLI built at the same time.

The fluent API can be described as the following finite state machine:
![flowchart](resources/images/fluent_api.png)

### Example
The following example is located in: [GuessingGameExample](jConsole-internal/com.jconsole/src/examples/guessgame/GuessGameJConsole.java)

```java
public class GuessGameJConsole
{

  public static int guess;
  public static int n;
  public static int attempts;
  public static int maxAttempts = 10;

  public static void main(String[] args) throws JConsoleException
  {

    JConsole app = JConsoleBuilder.builder()
                                  .loadArgsInto(GuessGameJConsole.class)
                                  .command("Guess", "<guess:Integer>")
                                  .mainScreen("MainMenu")
                                  // MainMenu screen
                                  .screen("MainMenu")
                                  .does(() -> {
                                    Random r = new Random(1);
                                    n = r.nextInt(1000) + 1;
                                    attempts = 1;
                                    System.out.println("Guess a number between 1 and 1000!");
                                  })
                                  .onCommand("Guess")
                                  .gotoScreenIf("Win", () -> guess == n)
                                  .elseGotoScreen("RetryGuess")
                                  .endScreen()
                                  // RetryGuess screen
                                  .screen("RetryGuess")
                                  .does(() -> {
                                    if (n < guess)
                                      System.out.println("Lower!");
                                    else
                                      System.out.println("Higher!");
                                    attempts++;
                                  })
                                  .onCommand("Guess")
                                  .gotoScreenIf("Win"       , () -> guess == n && attempts < maxAttempts)
                                  .gotoScreenIf("RetryGuess", () -> guess != n && attempts < maxAttempts)
                                  .gotoScreenIf("Loss"      , () -> attempts >= maxAttempts)
                                  .endIf()
                                  .endScreen()
                                  // Win screen
                                  .screen("Win")
                                  .does(() -> System.out.println("You won!"))
                                  .endScreen()
                                  // Loss screen
                                  .screen("Loss")
                                  .does(() -> System.out.println("You lost! The answer was " + n))
                                  .endScreen()
                                  .build();

    app.run();
  }
}
```

## External

Before presenting the external syntax, an important keynote is that, to
  differentiate normal blocks from blocks that contain Java code, the latter
  uses double curly brackets instead of a normal set, i.e., `{{...}}` is a
  Java code block while `{...}` is a block of jConsole.

The external DSL's syntax can be separated in to 4 sections:
  - **Dependencies** - imports and variable definitions
  - **Input**        - commands and guards
  - **Output**       - screens and procedures
  - **Main**         - define main screen

In **Dependencies**, the programmer can write in Java code, both the import
  directives the app needs to run, and define all variables used in it (and by
  the commands). Import directives are written inside an imports block as
  follows:
  ```
  imports {{
    // Imports here
  }}
  ```
  and variables are declared in a similar way:
  ```
  variables {{
    // Variables here
  }}
  ```
  Keep in mind that all variables **must** be static.

---

Commands and guards are defined in the **Input** section. A command has the same
  meaning and syntax as before, but is now written as:
  ```
  command commandName = commandStr
  ```
  where `commandName` is the unique name for the command, and `commandStr`
  is the String that describes it.

Guards are used to condition transitions between screens and represent a simple
  boolean expression in Java. They are written as:
  ```
  guard guardName = guardStr
  ```
  where ```guardName``` is the guard's unique identifier and `guardStr` is
  the String containing a Java boolean expression.

---

The most important part of the app lie inside the **Output** section: screens
  and procedures. Screens can be seen as a piece of code that executes and then
  has a set of possible transitions to other screens/procedures, or if it's
  empty, it's a terminal screen. Procedures are just 'screens' that do something
  and do not transition screens, therefore 'going back' to the screen which
  called it.

Screens are defined as:
  ```
  screen screenName {
    do {{
      // Java code
    }}
  }
  ```
  where `screenName` is the screen's unique name. The `do` java block specifies
  what the screen does once the user reaches it.

Furthermore, the screen can have several transitions to other screens using
  a `when` block:
  ```
  when command {
    otherScreen
  }
  ```
  which states that when the command named ```command``` is used, the app jumps
  to another screen named `otherScreen`.

These transitions can be extended using previously defined guards in *if-then-else*
  statements, where the programmer can write a Java boolean statement using
  guards as an abstraction:
  ```
  guard lessThan5 = "i < 5"

  (...)

  when command {
    if "lessThan5 && i > 0" then anotherScreen
    else                         otherScreen
  }
  ```
  in which the guard `lessThan5` is later translated to `(i < 5) && i > 0`.

Lastly, a screen can call a procedure on a given command using a different *when*
  statement:
  ```
  when command do procedure
  ```
  where `command` is the command's identifier, and `procedure` is the procedure's
  identifier.

Defining procedures is as simple as:
  ```
  procedure procedureName {{
    // Java code
  }}
  ```
  where `procedureName` is the procedure's identifier.

---

The last section of a jConsole file is dedicated to define in which screen the
  app begins in, using:
  ```
  main is screenName
  ```
  where `screenName` is the starting screen's identifier.

---

jConsole also supports Java comments inside its syntax as follows:
  ```
  /*
   * Block comment
   */

  // Line comment
  ```

### Domain Model
The domain model suffered some modifications coming from the internal syntax,
  mostly due to the presence of code, for example, there are no actions or
  guards like before, but instead an object that keeps the correspondant code.

Domain model as modelled in the ECore framework:
![domain_model_external](resources/images/domain_model_external.png)

In detail:
  - **CLIApp** represents the app itself and keeps track of all its elements. It
    contains at most one Imports and at most one Variables, but can have as many
    Guards, Commands and Procedures as needed. CLIApp only requires that at
    least one Screen is defined, which corresponds to the initial screen
    requirement.
  - **Imports** holds the import directives in Java as plain text
  - **Variables** is very similar to Imports, but it holds variable declaration
    and instatiation intead
  - **NamedElement** is an abstract class used when a class has an identifier
  - **Guard** contains the conditional statement as a string and is a
    NamedElement
  - **Command** has the string representation of the command (used later by the
    code generation) and is a NamedElement
  - **Screen** has one internal Procedure representing what that screen does
    when reached, and a list of TransitionSets that correspond to every
    transition to every other Screen or Procedure, but if it doesn't, it means
    that the Screen is terminal. Screen is also a NamedElement
  - **Procedure** follows the same logic as Imports and Variables, it stores
    the Java code string representation
  - **TransitionSet** represents all Transitions possible when a given Command
    is issued and can have 3 different arrangements:
    - Direct Transition to a Screen without any Guards
    - A list of Transitions and maybe an 'else' Transition for a default
      behaviour
    - Procedure call
  - **Transition** has a condition in string format and a target Screen to which
    to transition if the condition evaluates to true (in generated code)

### Build & Run
jConsole provides a standalone compiler that can be executed over a .jc file
  with:
  ```
  java -jar jConsole.jar <fileName>.jc
  ```
  which generates a folder named `src-gen` that contains the generated .java
  file.

Afterwards, simply compile and run this file by doing:
  ```
  javac src-gen/<fileName>.java
  java  src-gen/<fileName>
  ```

### Code Generation
jConsole compiles a .jc file to a corresponding .java file. It uses auxiliary
  interfaces to represent each command's parser (`Command` interface) and
  create procedures with passed Java code written in Java code blocks
  (`Procedure` interface). All of these structures are kept in Maps to be
  triggered on demand by screens.

### Validators
jConsole validates that there are no duplicate commands, guards,screens, or
  procedures.

### Typing
Typing is passed to the Java compiler, because jConsole only models flow and lets
  all behaviour be programmed in Java.

### Quickfixes
To accelerate implementation of all jConsole structures, there are quickfixes
  that create missing commands, guards,screens, or procedures.
