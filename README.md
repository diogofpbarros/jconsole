```
   _  _____                      _      
  (_)/ ____|                    | |     
   _| |     ___  _ __  ___  ___ | | ___
  | | |    / _ \| '_ \/ __|/ _ \| |/ _ \
  | | |___| (_) | | | \__ \ (_) | |  __/
  | |\_____\___/|_| |_|___/\___/|_|\___|
 _/ |                                   
|__/                                                                                                     
```

# Purpose
jConsole's purpose is to simplify and accelerate software development and testing
  by having a quick and easy way of creating a console application in which one
  can integrate software freely.

# Contents
This repository is an Eclipse project that contains two nested projects:
- **jConsole** - the jConsole language package
  - **src** - DSL source code, builder and examples
  - **test** - DSL unit tests
- **sandbox** - a sandbox to test and try new ideas

# How to import to Eclipse
1. In Eclipse, Import... -> Projects from Folder or Archive
2. Select the jConsole project directory
3. Select All -> Finish 

# Versions
## Version 1.0.0
  - Command definition:
    - supported types: String, Integer, Boolean
    - supports static command substrings: emptyCommand

## Version 2.0.0 (FUTURE)
  - Command definition:
    - supports optional arguments
